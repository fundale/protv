﻿Shader "ProTV/VideoScreen"
{
    Properties
    {
        _MainTex("Standby Texture", 2D) = "black" {}
        _SoundTex("Sound-Only Texture", 2D) = "grey" {}
        _VideoTex("Video Texture (Render Texture from the TV goes here)", 2D) = "black" {}
        _Aspect("Target Aspect Ratio (0 to ignore)", Float) = 1.77777
        [Gamma] _Brightness("Brightness", Float) = 1
        [Enum(Disabled, 0, Standard, 1, Dynamic, 2)] _Mirror("Mirror Flip Mode", Float) = 1
        [Enum(None, 0, Side by Side, 1, Side By Side Swapped, 2, Over Under, 3, Over Under Swapped, 4)] _3D("Standby 3D Mode", Float) = 0
        [Enum(Half Size 3D, 2, Full Size 3D, 0)] _Wide("Standby 3D Mode Size", Float) = 2
        _Spread("Standby 3D Stereo Offset", Float) = 0
        [ToggleUI] _Force2D("Force Standby to 2D", Float) = 0
        [ToggleUI] _Clip("Clip Aspect", Float) = 0
        [ToggleUI] _Fog("Enable Fog", Float) = 1
    }
    SubShader
    {
        Tags
        {
            "Queue" = "AlphaTest+50"
        }
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // GPU Instancing support https://docs.unity3d.com/2022.3/Documentation/Manual/gpu-instancing-shader.html
            #pragma multi_compile_instancing
            #pragma multi_compile_fog
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _MainTex_TexelSize;

            sampler2D _SoundTex;
            float4 _SoundTex_ST;
            float4 _SoundTex_TexelSize;

            // Use explicit sampler state to deal with the texture resizing
            Texture2D _VideoTex;
            SamplerState sampler_VideoTex;
            float4 _VideoTex_ST;
            float4x4 _VideoData;

            float _Brightness;
            float _Mirror;
            float _Clip;

            float _3D;
            float _Wide;
            float _Spread;
            float _Force2D;
            float _Aspect;
            float _Fog;

            uniform float _VRChatMirrorMode;
            uniform float3 _VRChatMirrorCameraPos;

            float2 aspectRatio(float2 uv, const float expectedAspect, float2 res, const float2 center)
            {
                if (expectedAspect == 0) return uv; // apsect of 0 means no adjustments made
                if (abs(res.x / res.y - expectedAspect) > .001)
                {
                    float2 norm_res = float2(res.x / expectedAspect, res.y);
                    const float2 correction = lerp(
                        // width needs corrected
                        float2(norm_res.x / norm_res.y, 1),
                        // height needs corrected
                        float2(1, norm_res.y / norm_res.x),
                        // determine corrective axis
                        norm_res.x > norm_res.y
                    );
                    // apply normalized correction anchored to given center
                    uv = ((uv - center) / correction) + center;
                }
                return uv;
            }

            bool is_mirror() { return _Mirror && _VRChatMirrorMode; }

            bool isRightEye()
            {
                #ifdef USING_STEREO_MATRICES
                return unity_StereoEyeIndex == 1;
                #else
                // include stereoeyeindex here cause pico is a dumb pos
                return unity_StereoEyeIndex == 1
                    || _VRChatMirrorMode == 1 && mul(unity_WorldToCamera, float4(_VRChatMirrorCameraPos, 1)).x < 0;
                #endif
            }

            bool isDesktop()
            {
                #ifdef USING_STEREO_MATRICES
                return false;
                #else
                return _VRChatMirrorMode != 1;
                #endif
            }

            bool hasSoundTexture() { return _SoundTex_TexelSize.z > 16; }

            struct vertdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct fragdata
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
                // fog support
                UNITY_FOG_COORDS(1)
            };

            fragdata vert(vertdata v)
            {
                fragdata o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);
                UNITY_INITIALIZE_OUTPUT(fragdata, o);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                // fog support
                UNITY_TRANSFER_FOG(o, o.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag(const fragdata i) : SV_Target
            {
                // The fragment solver goes in the following order:
                // - Correct uv for respective _ST values.
                // - If the uv is detected to be rendering in a mirror, flip the x axis
                // - Handle the uv for 3D split and remap accordingly
                // - Modify the uv for any 3D offset correction required
                // - Apply any necessary aspect ratio correction having taken 3D frame splitting into account in step 2
                // - Make any point of the UV that is outside the resulting aspect ratio calculations black pixels (letterboxing)
                // - Get the target pixel for the uv
                // - Apply custom brightness adjustment
                // - Apply fog adjustment

                UNITY_SETUP_INSTANCE_ID(i);
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

                int videoWidth;
                int videoHeight;
                _VideoTex.GetDimensions(videoWidth, videoHeight);
                float2 videoDims = float2(videoWidth, videoHeight);
                const bool noVideo = videoWidth <= 16;

                // use the 3D value from the TVData object when 3D is None
                const float _3d = noVideo ? _3D : abs(_VideoData._41);
                const float wide = noVideo ? _Wide - 1 : sign(_VideoData._41);
                // because the UV starts at the bottom, to properly flip OVUN, we 'swap' the non-swapped mode
                const bool swap = _3d == 2 || _3d == 3;
                // whether the tv is stopped, playing or paused
                const float state = _VideoData._12;
                const float seek = _VideoData._22;
                // Setting force 2D to 1 makes both eyes render
                const bool force2d = noVideo ? _Force2D : int(_VideoData._11) >> 4 & 1;
                const bool isLive = int(_VideoData._11) >> 2 & 1;

                const bool rightEye = swap != isRightEye();
                const float stereoEyeRight = !force2d && rightEye ? 0.5 : 0;
                // default clip zone is 100 % of the uv space
                float4 uvClip = float4(0, 0, 1, 1);

                const bool onlySound = noVideo && hasSoundTexture() && state > 1 && (isLive || seek > 0 && seek < 1);

                if (onlySound) videoDims = _SoundTex_TexelSize.zw;
                else if (noVideo) videoDims = _MainTex_TexelSize.zw;

                float2 uv = i.uv;
                if (onlySound) uv = uv * _SoundTex_ST.xy + _SoundTex_ST.zw;
                else if (noVideo) uv = uv * _MainTex_ST.xy + _MainTex_ST.zw;
                else uv = uv * _VideoTex_ST.xy + _VideoTex_ST.zw;

                // adjust if rendering in mirror
                if (_Mirror == 1) uv.x = lerp(uv.x, 1 - uv.x, is_mirror());
                else if (_Mirror == 2) uv = lerp(uv, 1 - uv, ddy(uv) < 0);

                if (_3d == 1 || _3d == 2) // side-by-side
                {
                    // correct for SBS-Full mode
                    videoDims.x = lerp(videoDims.x, videoDims.x / 2, wide < 0);
                    // exclude 2 pixels on the center line where the eyes split to avoid some edge-case aliasing
                    const float pad = 2 / videoDims.x;
                    // clip adjustment
                    uvClip.x = stereoEyeRight;
                    uvClip.z = stereoEyeRight + 0.5;
                    // eye correction
                    uv.x = uv.x * 0.5 + stereoEyeRight;
                }
                else if (_3d == 3 || _3d == 4) // over-under
                {
                    // correct for OVUN-Full mode
                    videoDims.y = lerp(videoDims.y, videoDims.y / 2, wide < 0);
                    // exclude 2 pixels on the center line where the eyes split to avoid some edge-case aliasing
                    const float pad = 2 / videoDims.y;
                    // clip adjustment
                    uvClip.y = stereoEyeRight;
                    uvClip.w = stereoEyeRight + 0.5;
                    // eye correction
                    uv.y = uv.y * 0.5 + stereoEyeRight;
                }

                // determine the effective center of the uv
                const float2 uvCenter = (uvClip.xy + uvClip.zw) * 0.5;

                // modify the uv for the aspect ratio of the texture resolution anchored to the given center
                uv = aspectRatio(uv, _Aspect, videoDims, uvCenter);

                // letterbox any uv point that is outside the expected clip zone
                // make the span of the anti-alias fix span the size of 2 pixel of the source texture
                const float2 uvPadding = (2 / videoDims);
                // get the amount of presence that the uv has on the minimum side, use uvClip to letterbox/pillarbox the visibility
                const float2 minFactor = smoothstep(uvClip.xy, uvClip.xy + uvPadding, uv);
                // get the amount of presence that the uv has on the maximum side, use uvClip to letterbox/pillarbox the visibility
                const float2 maxFactor = smoothstep(uvClip.zw, uvClip.zw - uvPadding, uv);
                // multiply them all together. If any of the factor edges are 0, it is considered not visible
                const float visibility = maxFactor.x * maxFactor.y * minFactor.x * minFactor.y;

                if (_Clip && !visibility) discard;

                // sample the texture
                float4 tex;
                if (onlySound) tex = tex2D(_SoundTex, uv);
                else if (noVideo) tex = tex2D(_MainTex, uv);
                else tex = _VideoTex.Sample(sampler_VideoTex, uv);

                // final color output
                tex = lerp((0).xxxx, tex * _Brightness, visibility);
                // apply fog adjustment
                if (_Fog) UNITY_APPLY_FOG(i.fogCoord, tex);
                return tex;
            }
            ENDCG
        }
    }
    Fallback Off
}