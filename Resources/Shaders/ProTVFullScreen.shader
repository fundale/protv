﻿Shader "ProTV/FullScreen"
{
    Properties
    {
        _MainTex("Default Texture", 2D) = "black" {}
        _SoundTex("Sound-Only Texture", 2D) = "grey" {}
        [NoScaleOffset] _VideoTex("Video Texture (Render Texture from the TV can go here)", 2D) = "black" {}
        [Gamma] _Brightness("Brightness", Float) = 1
        [Enum(None, 0, Side by Side, 1, Side By Side Swapped, 2, Over Under, 3, Over Under Swapped, 4)] _3D("Standby 3D Mode", Float) = 0
        [Enum(Half Size 3D, 2, Full Size 3D, 0)] _Wide("Standby 3D Mode Size", Float) = 2
    }
    SubShader
    {
        Tags
        {
            "Queue"="Overlay-1"
        }

        LOD 100
        Cull Off
        ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // GPU Instancing support https://docs.unity3d.com/2019.4/Documentation/Manual/GPUInstancing.html
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _MainTex_TexelSize;

            sampler2D _SoundTex;
            float4 _SoundTex_ST;
            float4 _SoundTex_TexelSize;

            // Use explicit sampler state to deal with the texture resizing
            Texture2D _VideoTex;
            SamplerState sampler_VideoTex;
            float4 _VideoTex_ST;
            float4x4 _VideoData;

            float _3D;
            float _Wide;

            float _Brightness;

            uniform float _VRChatMirrorMode;
            uniform float3 _VRChatMirrorCameraPos;

            UNITY_INSTANCING_BUFFER_START(Props)
            // example instanced property
            // UNITY_DEFINE_INSTANCED_PROP(type, _Name)
            UNITY_INSTANCING_BUFFER_END(Props)

            float2 getScreenUV(float4 screenPos)
            {
                float2 uv = screenPos / (screenPos.w + 0.0000000001);
                return uv;
            }

            float AspectConversion()
            {
                float scr_aspect = _ScreenParams.y * (_ScreenParams.z - 1);
                float tex_rcp_aspect = _MainTex_TexelSize.y * _MainTex_TexelSize.z;
                return scr_aspect * tex_rcp_aspect;
            }

            float2 UVAspectConversion()
            {
                float coeff = AspectConversion();
                return lerp(float2(1 / coeff, 1), float2(1, coeff), coeff > 1);
            }

            float getAspectForScreen(float4 texelSize)
            {
                float scr_aspect = _ScreenParams.y * (_ScreenParams.z - 1);
                float tex_rcp_aspect = texelSize.y * texelSize.z;
                return scr_aspect * tex_rcp_aspect;
            }

            float2 aspectRatio(float2 uv, const float expectedAspect, float2 res, const float2 center)
            {
                if (expectedAspect == 0) return uv; // apsect of 0 means no adjustments made
                if (abs(res.x / res.y - expectedAspect) > .001)
                {
                    float2 norm_res = float2(res.x / expectedAspect, res.y);
                    const float2 correction = lerp(
                        // width needs corrected
                        float2(norm_res.x / norm_res.y, 1),
                        // height needs corrected
                        float2(1, norm_res.y / norm_res.x),
                        // determine corrective axis
                        norm_res.x > norm_res.y
                    );
                    // apply normalized correction anchored to given center
                    uv = ((uv - center) / correction) + center;
                }
                return uv;
            }

            bool isRightEye()
            {
                #if defined(USING_STEREO_MATRICES)
                return unity_StereoEyeIndex == 1;
                #else
                return _VRChatMirrorMode == 1 && mul(unity_WorldToCamera, float4(_VRChatMirrorCameraPos, 1)).x < 0;
                #endif
            }

            bool isDesktop()
            {
                #if defined(USING_STEREO_MATRICES)
                return false;
                #else
                return _VRChatMirrorMode != 1;
                #endif
            }

            bool hasSoundTexture() { return _SoundTex_TexelSize.z > 16; }

            struct vertdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                // SPS-I support
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct fragdata
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
                // SPS-I support
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            fragdata vert(vertdata v)
            {
                fragdata o;
                // SPS-I support
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);
                UNITY_INITIALIZE_OUTPUT(fragdata, o);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.screenPos = ComputeNonStereoScreenPos(o.vertex);
                return o;
            }

            float4 frag(const fragdata i) : SV_Target
            {
                // The fragment solver goes in the following order:
                // - If the uv is detected to be rendering in a mirror, flip the x axis
                // - Handle the uv for 3D split and remap accordingly
                // - Modify the uv for any 3D offset correction required (eg: IMAX-style 3D alignment)
                // - Apply any necessary aspect ratio correction having taken 3D frame splitting into account in step 2
                // - Make any point of the UV that is outside the resulting aspect ratio calculations black pixels (letterboxing)
                // - Apply custom brightness adjustment

                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

                // to access instanced properties, use this format
                // type val = UNITY_ACCESS_INSTANCED_PROP(Props, _Name);

                int video_width;
                int video_height;
                _VideoTex.GetDimensions(video_width, video_height);
                float2 video_dims = float2(video_width, video_height);
                const bool no_video = video_width <= 16;

                // use the 3D value from the TVData object when 3D is None
                const float _3d = no_video ? _3D : abs(_VideoData._41);
                const float wide = no_video ? _Wide - 1 : sign(_VideoData._41);
                // whether the tv is stopped, playing or paused
                const float state = _VideoData._12;
                const float seek = _VideoData._22;
                const bool isLive = int(_VideoData._11) >> 2 & 1;
                const bool only_sound = no_video && hasSoundTexture() && state > 1 && (isLive || seek > 0 && seek < 1);

                // default clip zone is 100 % of the uv space
                float4 uv_clip = float4(0, 0, 1, 1);

                float2 uv = getScreenUV(i.screenPos);
                // scale/offset math for AVPro flip correction (this is the same as the TRANSFORM_TEX macro)
                if (only_sound) uv = uv * _SoundTex_ST.xy + _SoundTex_ST.zw;
                else if (no_video) uv = uv * _MainTex_ST.xy + _MainTex_ST.zw;
                else uv = uv * _VideoTex_ST.xy + _VideoTex_ST.zw;

                if (_3d == 1 || _3d == 2) // side-by-side
                {
                    // clip adjustment
                    uv_clip.xz = float2(0, 0.5);
                    // eye correction
                    uv.x = uv.x * 0.5;
                    // correct for SBS-Full mode
                    video_dims.x = lerp(video_dims.x, video_dims.x / 2, wide < 0);
                }
                else if (_3d == 3 || _3d == 4) // over-under
                {
                    // clip adjustment
                    uv_clip.yw = float2(0, 0.5);
                    // eye correction
                    uv.y = uv.y * 0.5;
                    // correct for OVUN-Full mode
                    video_dims.y = lerp(video_dims.y, video_dims.y / 2, wide < 0);
                }

                // determine the effective center of the uv
                const float2 uv_center = (uv_clip.xy + uv_clip.zw) * 0.5;

                // modify the uv for the aspect ratio of the texture resolution anchored to the given center
                const float screen_aspect = _ScreenParams.x / _ScreenParams.y;
                if (only_sound) uv = aspectRatio(uv, screen_aspect, _SoundTex_TexelSize.zw, uv_center);
                else if (no_video) uv = aspectRatio(uv, screen_aspect, _MainTex_TexelSize.zw, uv_center);
                else uv = aspectRatio(uv, screen_aspect, video_dims, uv_center);

                // letterbox any uv point that is outside the expected clip zone
                if (any(uv <= uv_clip.xy) || any(uv >= uv_clip.zw)) return float4(0, 0, 0, 1);

                // sample the texture
                float4 tex;
                if (only_sound) tex = tex2D(_SoundTex, uv);
                else if (no_video) tex = tex2D(_MainTex, uv);
                else tex = _VideoTex.Sample(sampler_VideoTex, uv);

                // final color output
                return tex * _Brightness;
            }
            ENDCG
        }
    }
    Fallback Off
}