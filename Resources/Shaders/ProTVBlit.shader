﻿Shader "Hidden/ProTV/Blit"
{
    Properties
    {
        [HideInInspector] _MainTex ("Blit Texture", 2D) = "black" {}
        // hidden in inspector because this should only be assigned via script
        [HideInInspector] _MainTex_ST_Override ("Scale/Offset Override", Vector) = (1.0, 1.0, 0.0, 0.0)
    }
    SubShader
    {
        Pass
        {
            // required for Blit to succeed on Android
            ZTest Always
            ZWrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            // float4 _MainTex_HDR;
            float4 _MainTex_TexelSize;
            float4 _MainTex_ST_Override;

            float _AVPro;
            float _SkipGamma;

            struct vertdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct fragdata
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            fragdata vert(vertdata v)
            {
                fragdata o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag(fragdata i) : SV_Target
            {
                // because default properties are stupid while doing a Blit, allow a custom _ST override vector if the provided value is not the default one
                if (any(_MainTex_ST_Override != float4(1, 1, 0, 0))) _MainTex_ST = _MainTex_ST_Override;

                // scale/offset math for AVPro flip correction (this is the same as the TRANSFORM_TEX macro)
                i.uv = i.uv * _MainTex_ST.xy + _MainTex_ST.zw;

                float4 tex = tex2D(_MainTex, i.uv);
                // implicitly apply gamma adjustment for avpro
                // tex.rgb = DecodeHDR(tex, _MainTex_HDR);

                #ifndef UNITY_COLORSPACE_GAMMA
                // if not in gamma colorspace, handle avpro conversion
                tex.rgb = _AVPro && !_SkipGamma ? GammaToLinearSpace(tex.rgb) : tex.rgb;
                #endif

                return tex;
            }
            ENDCG
        }

        Pass
        {
            // required for Blit to succeed on Android
            ZTest Always
            ZWrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _MainTex_TexelSize;

            float _ForceAspect;
            int _3D;

            struct vertdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct fragdata
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            float2 aspectRatio(float2 uv, const float expectedAspect, float2 res, const float2 center)
            {
                if (expectedAspect == 0) return uv; // aspect 0 means no adjustments
                if (abs(res.x / res.y - expectedAspect) > .001)
                {
                    float2 normalizedRes = float2(res.x / expectedAspect, res.y);
                    const float2 correction =
                        // determine corrective axis
                        normalizedRes.x > normalizedRes.y
                            // height needs corrected
                            ? float2(1, normalizedRes.y / normalizedRes.x)
                            // width needs corrected
                            : float2(normalizedRes.x / normalizedRes.y, 1);
                    // apply normalized correction anchored to given center
                    uv = ((uv - center) / correction) + center;
                }
                return uv;
            }

            fragdata vert(vertdata v)
            {
                fragdata o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag(fragdata i) : SV_Target
            {
                float2 videoDims = _MainTex_TexelSize.zw;
                const int _3d = abs(_3D);
                const int wide = sign(_3D);
                float2 uv = i.uv;
                // default clip zone is 100 % of the uv space
                float4 uvClip = float4(0, 0, 1, 1);

                if (_3d == 1 || _3d == 2)
                {
                    // clip adjustment
                    uvClip.x = 0;
                    uvClip.z = 0.5;
                    // eye correction
                    uv.x = uv.x * 0.5;
                    // correct for SBS-Full mode
                    videoDims.x = lerp(videoDims.x, videoDims.x / 2, wide < 0);
                }
                else if (_3d == 3 || _3d == 4) // over-under
                {
                    // clip adjustment
                    uvClip.y = 0;
                    uvClip.w = 0.5;
                    // eye correction
                    uv.y = uv.y * 0.5;
                    // correct for OVUN-Full mode
                    videoDims.x = lerp(videoDims.y, videoDims.y / 2, wide < 0);
                }

                // determine the effective center of the uv
                const float2 uvCenter = (uvClip.xy + uvClip.zw) * 0.5;

                if (_ForceAspect != 0) uv = aspectRatio(uv, _ForceAspect, videoDims, uvCenter);

                // letterbox any uv point that is outside the expected clip zone
                if (any(uv <= uvClip.xy) || any(uv >= uvClip.zw)) return float4(0, 0, 0, 1);

                float4 tex = tex2D(_MainTex, uv);

                return tex;
            }
            ENDCG
        }
    }
    Fallback Off
}