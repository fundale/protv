#ifndef PROTV_CORE_INCLUDED
#define PROTV_CORE_INCLUDED
#endif

#ifndef UNITY_CG_INCLUDED
#include "UnityCG.cginc"
#endif

#ifndef UNITY_STANDARD_INPUT_INCLUDED
sampler2D _MainTex;
float4 _MainTex_ST;
#endif
float4 _MainTex_TexelSize;

sampler2D _SoundTex;
float4 _SoundTex_ST;
float4 _SoundTex_TexelSize;

float _Mirror;
float _Clip;

float _3D;
float _Wide;
float _Spread;
float _Force2D;
float _Aspect;
float _Fog;

uniform float _VRChatMirrorMode;
uniform float3 _VRChatMirrorCameraPos;

float2 TVAspectRatio(float2 uv, const float expected_aspect, const float2 res, const float2 center)
{
    if (expected_aspect == 0) return uv; // apsect of 0 means no adjustments made
    if (abs(res.x / res.y - expected_aspect) <= 0.0001) return uv; // if res is close enough, skip adjustment
    float2 norm_res = float2(res.x / expected_aspect, res.y);
    const float2 correction = lerp(
        // width needs corrected
        float2(norm_res.x / norm_res.y, 1),
        // height needs corrected
        float2(1, norm_res.y / norm_res.x),
        // determine corrective axis
        norm_res.x > norm_res.y
    );
    // apply normalized correction anchored to given center
    return ((uv - center) / correction) + center;
}

float TVAspectVisibility(const float2 uv, const float2 res, float4 uvClip)
{
    // make the span of the anti-alias fix span the size of 2 pixel of the source texture
    const float2 uvPadding = (2 / res);
    // get the amount of presence that the uv has on the minimum side, use uvClip to letterbox/pillarbox the visibility
    const float2 minFactor = smoothstep(uvClip.xy, uvClip.xy + uvPadding, uv);
    // get the amount of presence that the uv has on the maximum side, use uvClip to letterbox/pillarbox the visibility
    const float2 maxFactor = smoothstep(uvClip.zw, uvClip.zw - uvPadding, uv);
    // multiply them all together. If any of the factor edges are 0, it is considered not visible
    return maxFactor.x * maxFactor.y * minFactor.x * minFactor.y;
}

bool IsMirror() { return _Mirror && _VRChatMirrorMode; }

bool IsRightEye()
{
    #ifdef USING_STEREO_MATRICES
                return unity_StereoEyeIndex == 1;
    #else
    // include stereoeyeindex here cause pico is a dumb pos
    return unity_StereoEyeIndex == 1
        || _VRChatMirrorMode == 1 && mul(unity_WorldToCamera, float4(_VRChatMirrorCameraPos, 1)).x < 0;
    #endif
}

bool IsDesktop()
{
    #ifdef USING_STEREO_MATRICES
    return false;
    #else
    return _VRChatMirrorMode != 1;
    #endif
}

bool HasVideoTexture(float2 size) { return size.x > 16; }
bool HasSoundTexure() { return _SoundTex_TexelSize.z > 16; }

bool IsLive(float4x4 data) { return int(data._11) >> 2 & 1; }
bool IsForce2d(float4x4 data) { return int(data._11) >> 4 & 1; }

float Seek(float4x4 data) { return data._22; }
float Mode3D(float4x4 data) { return abs(data._41); }
float Wide3D(float4x4 data) { return sign(data._41); }

bool IsTVReady(float4x4 data) { return data._14; }
bool IsTVWaiting(float4x4 data) { return data._12 == 0; }
bool IsTVStopped(float4x4 data) { return data._12 == 1; }
bool IsTVPlaying(float4x4 data) { return data._12 == 2; }
bool IsTVPaused(float4x4 data) { return data._12 == 3; }
bool IsVideoActive(float4x4 data) { return data._12 > 1; }
bool IsVideoInActive(float4x4 data) { return data._12 <= 1; }
bool IsMediaPlaying(const float4x4 data) { return IsLive(data) || Seek(data) > 0 && Seek(data) < 1; }

bool Is3DSideBySide(float4x4 data)
{
    const int mode = abs(data._41);
    return mode == 1 || mode == 2;
}

bool Is3DOverUnder(float4x4 data)
{
    const int mode = abs(data._41);
    return mode == 3 || mode == 4;
}

bool Is3DSwapped(float4x4 data)
{
    const int mode = abs(data._41);
    return mode == 2 || mode == 3;
}

float2 AdjustForMirror(float2 uv)
{
    // adjust if rendering in mirror
    if (_Mirror == 1) uv.x = lerp(uv.x, 1 - uv.x, IsMirror());
    else if (_Mirror == 2)
    {
        uv.x = lerp(uv.x, 1 - uv.x, ddx(uv.x) < 0);
        uv.y = lerp(uv.y, 1 - uv.y, ddy(uv.y) < 0);
    }
    return uv;
}

float4 ProcessFragment(float2 uv, const Texture2D videoTex, const SamplerState videoSampler, const float4 videoST,
                       const float4x4 videoData)
{
    // The fragment solver goes in the following order:
    // - Correct uv for respective _ST values.
    // - If the uv is detected to be rendering in a mirror, flip the x axis
    // - Handle the uv for 3D split and remap accordingly
    // - Modify the uv for any 3D offset correction required
    // - Apply any necessary aspect ratio correction having taken 3D frame splitting into account in step 2
    // - Make any point of the UV that is outside the resulting aspect ratio calculations black pixels (letterboxing)
    // - Get the target pixel for the uv

    int videoWidth;
    int videoHeight;
    videoTex.GetDimensions(videoWidth, videoHeight);
    float2 videoDims = float2(videoWidth, videoHeight);
    const bool noVideo = !HasVideoTexture(videoDims);
    const bool only_sound = noVideo && HasSoundTexure() && IsVideoActive(videoData) && IsMediaPlaying(videoData);

    // use the 3D value from the TVData object when 3D is None
    const float _3d = noVideo ? _3D : Mode3D(videoData);
    const float wide = noVideo ? _Wide - 1 : Wide3D(videoData);
    // because the UV starts at the bottom, to properly flip OVUN, we 'swap' the non-swapped mode
    const bool swap = _3d == 2 || _3d == 3;
    // Setting force 2D to 1 makes both eyes render
    const bool force2d = noVideo ? _Force2D : IsForce2d(videoData);

    if (only_sound) videoDims = _SoundTex_TexelSize.zw;
    else if (noVideo) videoDims = _MainTex_TexelSize.zw;

    if (only_sound) uv = uv * _SoundTex_ST.xy + _SoundTex_ST.zw;
    else if (noVideo) uv = uv * _MainTex_ST.xy + _MainTex_ST.zw;
    else uv = uv * videoST.xy + videoST.zw;

    uv = AdjustForMirror(uv);

    const bool rightEye = swap != IsRightEye();
    const float stereoEyeRight = !force2d && rightEye ? 0.5 : 0;
    // default clip zone is 100 % of the uv space
    float4 uvClip = float4(0, 0, 1, 1);
    if (_3d == 1 || _3d == 2) // side-by-side
    {
        // correct for SBS-Full mode
        videoDims.x = lerp(videoDims.x, videoDims.x / 2, wide < 0);
        // exclude 2 pixels on the center line where the eyes split to avoid some edge-case aliasing
        const float pad = 2 / videoDims.x;
        // clip adjustment
        uvClip.x = stereoEyeRight;
        uvClip.z = stereoEyeRight + 0.5;
        // eye correction
        uv.x = uv.x * 0.5 + stereoEyeRight;
    }
    else if (_3d == 3 || _3d == 4) // over-under
    {
        // correct for OVUN-Full mode
        videoDims.y = lerp(videoDims.y, videoDims.y / 2, wide < 0);
        // exclude 2 pixels on the center line where the eyes split to avoid some edge-case aliasing
        const float pad = 2 / videoDims.y;
        // clip adjustment
        uvClip.y = stereoEyeRight;
        uvClip.w = stereoEyeRight + 0.5;
        // eye correction
        uv.y = uv.y * 0.5 + stereoEyeRight;
    }

    // determine the effective center of the uv
    const float2 uvCenter = (uvClip.xy + uvClip.zw) * 0.5;

    // modify the uv for the aspect ratio of the texture resolution anchored to the given center
    uv = TVAspectRatio(uv, _Aspect, videoDims, uvCenter);

    // letterbox any uv point that is outside the expected clip zone
    const float visible = TVAspectVisibility(uv, videoDims, uvClip);

    // optionally clip the border
    if (_Clip && !visible) discard;

    // sample the texture
    float4 tex;
    if (only_sound) tex = tex2D(_SoundTex, uv);
    else if (noVideo) tex = tex2D(_MainTex, uv);
    else tex = videoTex.Sample(videoSampler, uv);

    // blend between invisible and the sampled texture pixel based on the visibility.
    return lerp((0).xxxx, tex, visible);
}
