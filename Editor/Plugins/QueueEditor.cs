using ArchiTech.SDK;
using ArchiTech.SDK.Editor;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using VRC.Core;

namespace ArchiTech.ProTV.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Queue))]
    internal class QueueEditor : TVPluginEditor
    {
        protected override bool autoRenderVariables => false;

        private void OnEnable()
        {
            SetupTVReferences();
        }

        protected override void RenderChangeCheck()
        {
            DrawTVReferences();
            DrawCustomHeaderLarge("General Settings");
            using (VBox)
            {
                // output the remaining variables
                DrawVariablesByName(
                    nameof(Queue.maxEntriesPerPlayer),
                    nameof(Queue.maxQueueLength),
                    nameof(Queue.preventDuplicateVideos),
                    nameof(Queue.showUrlsInQueue),
                    nameof(Queue.loop));
            }
        }
    }
}