﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ArchiTech.SDK;
using ArchiTech.SDK.Editor;
using TMPro;
using UdonSharpEditor;
using UnityEditor;
using UnityEditor.Events;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using VRC.Core;
using VRC.SDKBase;
using VRC.Udon;

#if UNITY_2021_2_OR_NEWER
using UnityEditor.AssetImporters;

#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace ArchiTech.ProTV.Editor
{
    [CustomEditor(typeof(Playlist))]
    internal class PlaylistEditor : TVPluginEditor
    {
        public const int latestTemplateVersion = 1;
        public const string EntryIndicatorsPrefix = "?";
        public const string EntryStartIndicator = "@";
        public const string EntryAltIndicator = "^";
        public const string EntryImageIndicator = "/";
        public const string EntryTagIndicator = "#";
        public const string EntryTitleIndicator = "~";
        private Playlist script;
        private VRCUrl[] mainUrls;
        private VRCUrl[] alternateUrls;
        private string[] titles;
        private string[] descriptions;
        private string[] tags;
        private Sprite[] images;
        internal int visibleCount;
        private Vector2 scrollPos;
        private ChangeAction updateMode = ChangeAction.NOOP;
        private bool importFromFile = false;
        private TextAsset importSrc;
        private string _importSrcPath = "";
        private const int perPage = 10;
        private int currentFocus;
        private int lastFocus;
        private int entriesCount;
        private int imagesCount;
        private int targetEntry;
        private string[] detectedPlaylists = new string[0];
        private string[] detectedPlaylistNames = new string[0];

        private VRCUrl[] currentPageUrls;
        private VRCUrl[] currentPageAlts;
        private string[] currentPageTitles;
        private string[] currentPageDescriptions;
        private string[] currentPageTags;
        private Sprite[] currentPageImages;
        private int currentPageStart;
        private int currentPageEnd;
        private bool recachePage = true;

        private int rawEntryCount;

        protected override bool autoRenderVariables => false;

        private enum ChangeAction
        {
            NOOP,
            OTHER,
            MOVEUP,
            MOVEDOWN,
            ADD,
            REMOVE,
            REMOVEALL,
            UPDATESELF,
            UPDATEALL,
            UPDATEVIEW
        }

        private void OnEnable()
        {
            script = (Playlist)target;
            entriesCount = script._EDITOR_entriesCount;
            imagesCount = script._EDITOR_imagesCount;
            importSrc = script._EDITOR_importSrc;
            _importSrcPath = importSrc == null ? null : AssetDatabase.GetAssetPath(importSrc);
            if (script.storage == null) SetVariableByName(nameof(script.storage), script.GetComponentInChildren<PlaylistData>(true));
            SetupTVReferences();
        }

        protected override void InitData()
        {
            if (script.storage != null)
            {
                mainUrls = script.storage.mainUrls;
                alternateUrls = script.storage.alternateUrls;
                titles = script.storage.titles;
                descriptions = script.storage.descriptions;
                tags = script.storage.tags;
                images = script.storage.images;
            }
            else
            {
                mainUrls = script.mainUrls;
                alternateUrls = script.alternateUrls;
                titles = script.titles;
                descriptions = script.descriptions;
                tags = script.tags;
                images = script.images;
            }

            mainUrls = NormalizeArray(mainUrls, 0);
            rawEntryCount = mainUrls.Length;
            alternateUrls = NormalizeArray(alternateUrls, rawEntryCount);
            titles = NormalizeArray(titles, rawEntryCount);
            descriptions = NormalizeArray(descriptions, rawEntryCount);
            tags = NormalizeArray(tags, rawEntryCount);
            images = NormalizeArray(images, rawEntryCount);

            cacheDetectedPlaylists();
        }

        private void cacheDetectedPlaylists()
        {
            var inPackages = AssetDatabase.FindAssets("t:TextAsset", new[] { "Packages" })
                .Select(AssetDatabase.GUIDToAssetPath)
                .Where(p => p.EndsWith(".playlist")).ToArray();

            var inAssets = AssetDatabase.FindAssets("t:TextAsset", new[] { "Assets" })
                .Select(AssetDatabase.GUIDToAssetPath)
                .Where(p => p.EndsWith(".playlist")).ToArray();

            IEnumerable<string> list = new string[0].AsEnumerable();
            list = list.Append("-");
            if (inAssets.Length > 0) list = list.Append(null).Union(inAssets);
            if (inPackages.Length > 0) list = list.Append("").Union(inPackages);

            detectedPlaylists = list.ToArray();
            detectedPlaylistNames = detectedPlaylists.Select(p =>
            {
                if (p == null) return null;
                var slash = p.LastIndexOf("/", StringComparison.Ordinal);
                var dot = p.LastIndexOf(".", StringComparison.Ordinal);
                return slash == -1 ? p : p.Substring(slash + 1).Substring(0, dot - slash - 1);
            }).ToArray();
        }

        protected override void RenderChangeCheck()
        {
            showProperties();
            if (EditorApplication.isPlaying)
            {
                EditorGUILayout.HelpBox(I18n.Tr("Playlist entry editing is disabled during playmode."), MessageType.None);
            }
            else
            {
                showListControls();
                showListEntries();
            }
        }

        protected override void SaveData()
        {
            if (updateMode == ChangeAction.NOOP) return; // don't save the data if current op was none
            if (updateMode != ChangeAction.OTHER)
            {
                updateScene();
                init = false;
                recachePage = true;
            }

            if (script.storage != null)
            {
                script.storage.mainUrls = mainUrls;
                script.storage.alternateUrls = alternateUrls;
                script.storage.titles = titles;
                script.storage.descriptions = descriptions;
                script.storage.tags = tags;
                script.storage.images = images;

                script.mainUrls = null;
                script.alternateUrls = null;
                script.titles = null;
                script.descriptions = null;
                script.tags = null;
                script.images = null;
            }
            else
            {
                script.mainUrls = mainUrls;
                script.alternateUrls = alternateUrls;
                script.titles = titles;
                script.descriptions = descriptions;
                script.tags = tags;
                script.images = images;
            }

            script._EDITOR_importSrc = importSrc;
            script._EDITOR_importFromFile = importFromFile;
            script._EDITOR_entriesCount = entriesCount;
            script._EDITOR_imagesCount = imagesCount;

            if (updateMode == ChangeAction.UPDATEALL)
            {
                foreach (var playlist in ATEditorUtility.GetComponentsInScene<Playlist>())
                {
                    // other playlists are connected to this data store, update them in the scene as well.
                    if (playlist.storage == script.storage)
                    {
                        RebuildScene(playlist);
                        if (importFromFile)
                        {
                            playlist._EDITOR_importFromFile = true;
                            playlist._EDITOR_importSrc = importSrc;
                            playlist._EDITOR_entriesCount = entriesCount;
                            playlist._EDITOR_imagesCount = imagesCount;
                        }
                    }
                }
            }

            updateMode = ChangeAction.NOOP;
        }

        private static void DisplayTemplateError() => EditorGUILayout.HelpBox(I18n.Tr("Template components MUST be a descendant of the containing template object."), MessageType.Error);

        private void showProperties()
        {
            bool isChanged = false;
            isChanged |= DrawTVReferences();

            DrawCustomHeaderLarge("General Settings");
            using (VBox)
            {
                if (DrawVariablesByName(nameof(script.storage)))
                {
                    mainUrls = script.storage.mainUrls;
                    alternateUrls = script.storage.alternateUrls;
                    titles = script.storage.titles;
                    descriptions = script.storage.descriptions;
                    tags = script.storage.tags;
                    images = script.storage.images;
                    updateMode = ChangeAction.UPDATESELF;
                }

                if (script.queue != null)
                {
                    var amount = script.queuePreloadAmount;
                    amount = Math.Min(amount, script.queue.maxQueueLength);
                    amount = Math.Min(amount, mainUrls.Length);
                    if (amount != script.queuePreloadAmount)
                        SetVariableByName(nameof(script.queuePreloadAmount), amount);
                    DrawVariablesByName(nameof(script.queuePreloadAmount));
                }

                if (DrawVariablesByName(nameof(script.shuffleOnLoad)))
                    updateMode = ChangeAction.OTHER;
                if (DrawVariablesByName(nameof(script.autoplayList)))
                    updateMode = ChangeAction.OTHER;

                if (script.autoplayList)
                {
                    using (HArea)
                    {
                        Spacer(25f);
                        if (DrawVariablesByName(nameof(script.autoplayOnLoad)))
                            updateMode = ChangeAction.OTHER;
                    }

                    using (HArea)
                    {
                        Spacer(25f);
                        if (DrawVariablesByName(nameof(script.startFromRandomEntry)))
                            updateMode = ChangeAction.OTHER;
                    }

                    using (HArea)
                    {
                        Spacer(25f);
                        if (DrawVariablesByName(nameof(script.loopPlaylist)))
                            updateMode = ChangeAction.OTHER;
                    }

                    using (HArea)
                    {
                        Spacer(25f);
                        if (DrawVariablesByName(nameof(script.continueWhereLeftOff)))
                            updateMode = ChangeAction.OTHER;
                    }

                    using (HArea)
                    {
                        Spacer(25f);
                        if (DrawVariablesByName(nameof(script.prioritizeOnInteract)))
                            updateMode = ChangeAction.OTHER;
                    }

                    using (HArea)
                    {
                        Spacer(25f);
                        if (DrawVariablesByName(nameof(script.disableAutoplayOnInteract)))
                            updateMode = ChangeAction.OTHER;
                    }
                }

                using (HArea)
                {
                    if (DrawVariablesByName(nameof(script.showUrls)))
                        updateMode = ChangeAction.OTHER;
                }

                using (HArea)
                {
                    if (DrawVariablesByName(nameof(script._EDITOR_autofillAltURL)))
                    {
                        updateMode = ChangeAction.OTHER;
                        if (!script._EDITOR_autofillAltURL) SetVariableByName(nameof(script._EDITOR_autofillFormat), "$URL"); // reset to default
                    }
                }

                if (script._EDITOR_autofillAltURL)
                {
                    EditorGUILayout.HelpBox("Put $URL (uppercase is important) wherever you want the main url to be inserted. Eg: https://mydomain.tld/?url=$URL", MessageType.Info);
                    using (HArea)
                    {
                        if (DrawVariablesByName(nameof(script._EDITOR_autofillFormat)))
                            updateMode = ChangeAction.OTHER;
                    }

                    if (DrawVariablesByName(nameof(script._EDITOR_autofillEscape)))
                        updateMode = ChangeAction.OTHER;
                }
            }

            DrawCustomHeaderLarge("UI Components");
            using (VBox)
            {
                isChanged |= DrawVariablesByName(nameof(script.scrollView));

                if (isChanged) updateMode = ChangeAction.OTHER;

                EditorGUI.BeginDisabledGroup(targets.Length > 1);
                DrawVariablesByName(nameof(script.listContainer));
                bool templateChanged = DrawVariablesByName(nameof(script.template)) || script._EDITOR_templateUpgrade < latestTemplateVersion;

                if (script.template != null)
                {
                    EditorGUI.BeginChangeCheck();
                    if (templateChanged) GUI.changed = true; // enable the lack of template upgrade to trigger the change check below
                    EditorGUILayout.BeginHorizontal();
                    Spacer(15f);
                    EditorGUILayout.BeginVertical();
                    GUIContent label = null;
                    var template = script.template;

                    label = GetPropertyLabel(nameof(script.urlDisplay), showHints);
                    label.text = "└ " + label.text;
                    using (HArea)
                    {
                        EditorGUILayout.PrefixLabel(label);
                        DrawVariablesByNameWithoutLabels(nameof(script.urlDisplay), nameof(script.urlDisplayTMP));
                    }

                    if (!template.IsComponentsInChildren(script.urlDisplay, script.urlDisplayTMP))
                        DisplayTemplateError();

                    label = GetPropertyLabel(nameof(script.titleDisplay), showHints);
                    label.text = "└ " + label.text;
                    using (HArea)
                    {
                        EditorGUILayout.PrefixLabel(label);
                        DrawVariablesByNameWithoutLabels(nameof(script.titleDisplay), nameof(script.titleDisplayTMP));
                    }

                    if (!template.IsComponentsInChildren(script.titleDisplay, script.titleDisplayTMP))
                        DisplayTemplateError();

                    label = GetPropertyLabel(nameof(script.descriptionDisplay), showHints);
                    label.text = "└ " + label.text;
                    using (HArea)
                    {
                        EditorGUILayout.PrefixLabel(label);
                        DrawVariablesByNameWithoutLabels(nameof(script.descriptionDisplay), nameof(script.descriptionDisplayTMP));
                    }

                    if (!template.IsComponentsInChildren(script.descriptionDisplay, script.descriptionDisplayTMP))
                        DisplayTemplateError();

                    label = GetPropertyLabel(nameof(script.selectAction), showHints);
                    label.text = "└ " + label.text;
                    DrawVariablesByNameWithLabel(label, nameof(script.selectAction));
                    if (!template.IsComponentsInChildren(script.selectAction)) DisplayTemplateError();

                    label = GetPropertyLabel(nameof(script.loadingBar), showHints);
                    label.text = "└ " + label.text;
                    DrawVariablesByNameWithLabel(label, nameof(script.loadingBar));
                    if (!template.IsComponentsInChildren(script.loadingBar)) DisplayTemplateError();

                    label = GetPropertyLabel(nameof(script.imageDisplay), showHints);
                    label.text = "└ " + label.text;
                    DrawVariablesByNameWithLabel(label, nameof(script.imageDisplay));
                    if (!template.IsComponentsInChildren(script.imageDisplay)) DisplayTemplateError();

                    Spacer(2f);
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndHorizontal();

                    if (EditorGUI.EndChangeCheck())
                    {
                        var msg = templateChanged ? "Auto-populating the template child references" : "Update template reference paths";
                        using (new SaveObjectScope(script, msg))
                        {
                            if (templateChanged) AutopopulateTemplateFields(script);
                            UpdateTmplPaths(script);
                        }
                    }
                }

                EditorGUI.EndDisabledGroup();
            }
        }

        internal static void AutopopulateTemplateFields(Playlist script)
        {
            var template = script.template;
            if (template == null) return; // no template, no autofill
            // clear old template references that don't match the template
            if (!template.IsComponentsInChildren(script.urlDisplay)) script.urlDisplay = null;
            if (!template.IsComponentsInChildren(script.urlDisplayTMP)) script.urlDisplayTMP = null;
            if (!template.IsComponentsInChildren(script.titleDisplay)) script.titleDisplay = null;
            if (!template.IsComponentsInChildren(script.titleDisplayTMP)) script.titleDisplayTMP = null;
            if (!template.IsComponentsInChildren(script.descriptionDisplay)) script.descriptionDisplay = null;
            if (!template.IsComponentsInChildren(script.descriptionDisplayTMP)) script.descriptionDisplayTMP = null;
            if (!template.IsComponentsInChildren(script.selectAction)) script.selectAction = null;
            if (!template.IsComponentsInChildren(script.loadingBar)) script.loadingBar = null;
            if (!template.IsComponentsInChildren(script.imageDisplay)) script.imageDisplay = null;
            var t_texts = template.GetComponentsInChildren<Text>(true);
            var t_tmpTexts = template.GetComponentsInChildren<TextMeshProUGUI>(true);
            var t_buttons = template.GetComponentsInChildren<Button>(true);
            var t_images = template.GetComponentsInChildren<Image>(true);
            var t_sliders = template.GetComponentsInChildren<Slider>(true);


            foreach (var text in t_texts)
            {
                var textName = text.name.ToLower();
                if (script.urlDisplay == null && textName.Contains("url")) script.urlDisplay = text;
                if (script.titleDisplay == null && textName.Contains("title")) script.titleDisplay = text;
                if (script.descriptionDisplay == null && textName.Contains("desc")) script.descriptionDisplay = text;
            }

            foreach (var tmpText in t_tmpTexts)
            {
                var textName = tmpText.name.ToLower();
                if (script.urlDisplayTMP == null && textName.Contains("url")) script.urlDisplayTMP = tmpText;
                if (script.titleDisplayTMP == null && textName.Contains("title")) script.titleDisplayTMP = tmpText;
                if (script.descriptionDisplayTMP == null && textName.Contains("desc")) script.descriptionDisplayTMP = tmpText;
            }

            foreach (var button in t_buttons)
            {
                if (script.selectAction == null) script.selectAction = button;
            }

            foreach (var slider in t_sliders)
            {
                var sliderName = slider.name.ToLower();
                if (script.loadingBar == null && sliderName.Contains("load")) script.loadingBar = slider;
            }

            foreach (var image in t_images)
            {
                var toggleName = image.name.ToLower();
                if (script.imageDisplay == null && (toggleName.Contains("image") || toggleName.Contains("poster"))) script.imageDisplay = image;
            }

            script._EDITOR_templateUpgrade = latestTemplateVersion;
        }

        internal static void UpdateTmplPaths(Playlist script)
        {
            script.urlDisplayTmplPath = null;
            script.titleDisplayTmplPath = null;
            script.descriptionDisplayTmplPath = null;
            script.urlDisplayTMPTmplPath = null;
            script.titleDisplayTMPTmplPath = null;
            script.descriptionDisplayTMPTmplPath = null;
            script.selectActionTmplPath = null;
            script.loadingBarTmplPath = null;
            script.imageDisplayTmplPath = null;

            if (script.template == null) return; // no template, no paths

#pragma warning disable CS0618
            Transform t = script.template.transform;
            Transform st;
            if (script.urlDisplay != null)
            {
                st = script.urlDisplay.transform;
                script.urlDisplayTmplPath = st == t ? "" : st.GetHierarchyPath(t);
            }

            if (script.titleDisplay != null)
            {
                st = script.titleDisplay.transform;
                script.titleDisplayTmplPath = st == t ? "" : st.GetHierarchyPath(t);
            }

            if (script.descriptionDisplay != null)
            {
                st = script.descriptionDisplay.transform;
                script.descriptionDisplayTmplPath = st == t ? "" : st.GetHierarchyPath(t);
            }

            if (script.urlDisplayTMP != null)
            {
                st = script.urlDisplayTMP.transform;
                script.urlDisplayTMPTmplPath = st == t ? "" : st.GetHierarchyPath(t);
            }

            if (script.titleDisplayTMP != null)
            {
                st = script.titleDisplayTMP.transform;
                script.titleDisplayTMPTmplPath = st == t ? "" : st.GetHierarchyPath(t);
            }

            if (script.descriptionDisplayTMP != null)
            {
                st = script.descriptionDisplayTMP.transform;
                script.descriptionDisplayTMPTmplPath = st == t ? "" : st.GetHierarchyPath(t);
            }

            if (script.selectAction != null)
            {
                st = script.selectAction.transform;
                script.selectActionTmplPath = st == t ? "" : st.GetHierarchyPath(t);
            }

            if (script.loadingBar != null)
            {
                st = script.loadingBar.transform;
                script.loadingBarTmplPath = st == t ? "" : st.GetHierarchyPath(t);
            }

            if (script.imageDisplay != null)
            {
                st = script.imageDisplay.transform;
                script.imageDisplayTmplPath = st == t ? "" : st.GetHierarchyPath(t);
            }
#pragma warning restore CS0618
        }

        private void showListControls()
        {
            Spacer();
            EditorGUILayout.BeginHorizontal(); // 1
            EditorGUILayout.BeginVertical(); // 2
            EditorGUILayout.LabelField(I18n.Tr("Video Playlist Items"), GUILayout.Width(120f), GUILayout.ExpandWidth(true));
            if (GUILayout.Button(I18n.Tr("Update Scene"), GUILayout.MaxWidth(100f)))
                updateMode = ChangeAction.UPDATEALL;

            using (HArea)
            {
                EditorGUI.BeginDisabledGroup(importFromFile);
                if (GUILayout.Button(I18n.Tr("Save"), GUILayout.ExpandWidth(false)))
                {
                    // get where to save the file
                    string defaultName = "CustomPlaylist - " + SceneManager.GetActiveScene().name;
                    string directory = "Assets";
                    if (script._EDITOR_importSrc != null)
                    {
                        defaultName = script._EDITOR_importSrc.name;
                        directory = AssetDatabase.GetAssetPath(script._EDITOR_importSrc);
                        directory = Path.GetDirectoryName(Path.GetFullPath(directory));
                    }

                    string destination = EditorUtility.SaveFilePanel(I18n.Tr("Playlist Export"), directory, defaultName, "playlist");
                    if (!string.IsNullOrWhiteSpace(destination))
                    {
                        Debug.Log($"Saving playlist to file {destination}");
                        // write the playlist content
                        File.WriteAllText(destination, pickle(), Encoding.UTF8);
                        AssetDatabase.Refresh();
                        TextAsset t = AssetDatabase.LoadAssetAtPath<TextAsset>(ATEditorUtility.ToRelativePath(destination));
                        // if the destination cannot be loaded as an asset, it's not accessible by the project so skip assignment
                        if (t != null)
                        {
                            // load the new playlist file into the import mode
                            script._EDITOR_importFromFile = true;
                            script._EDITOR_importSrc = t;
                            updateMode = ChangeAction.OTHER;
                            cacheDetectedPlaylists();
                        }
                    }
                }

                EditorGUI.EndDisabledGroup();
                if (GUILayout.Button(I18n.Tr("Copy"), GUILayout.ExpandWidth(false)))
                    GUIUtility.systemCopyBuffer = pickle();
            }

            EditorGUILayout.EndVertical(); // end 2
            Spacer();
            EditorGUILayout.BeginVertical(); // 2
            if (importSrc != script._EDITOR_importSrc)
            {
                importSrc = script._EDITOR_importSrc;
                _importSrcPath = importSrc == null ? null : AssetDatabase.GetAssetPath(importSrc);
            }

            string detection = "";
            if (importSrc != null && script._EDITOR_importFromFile)
                detection = $" | {entriesCount} URLs ({imagesCount} Images)";
            importFromFile = EditorGUILayout.ToggleLeft(I18n.Tr("Load From Text File") + detection, script._EDITOR_importFromFile);
            if (importFromFile != script._EDITOR_importFromFile) updateMode = ChangeAction.OTHER;
            if (importFromFile)
            {
                using (HArea)
                {
                    var importSrc = this.importSrc;
                    var detectedIndex = string.IsNullOrEmpty(_importSrcPath) ? -1 : Array.IndexOf(detectedPlaylists, _importSrcPath);
                    float popupSize = 270f;
                    bool hasAnyPlaylists = detectedPlaylists.Length != 0;
                    if (detectedIndex == -1)
                    {
                        popupSize = 30f;
                        importSrc = (TextAsset)EditorGUILayout.ObjectField(importSrc, typeof(TextAsset), false, GUILayout.MaxWidth(hasAnyPlaylists ? 270f : 300f));
                    }

                    if (hasAnyPlaylists)
                    {
                        var newIndex = EditorGUILayout.Popup(GUIContent.none, detectedIndex, detectedPlaylistNames, GUILayout.MaxWidth(popupSize));
                        if (newIndex != detectedIndex) importSrc = AssetDatabase.LoadAssetAtPath<TextAsset>(detectedPlaylists[newIndex]);
                        if (detectedIndex > -1 && GUILayout.Button("?", GUILayout.Width(30))) EditorGUIUtility.PingObject(this.importSrc);
                    }

                    if (importSrc != this.importSrc)
                    {
                        this.importSrc = importSrc;
                        _importSrcPath = importSrc == null ? null : AssetDatabase.GetAssetPath(importSrc);
                        updateMode = ChangeAction.OTHER;
                        entriesCount = 0;
                        imagesCount = 0;
                    }

                    if (this.importSrc != null)
                    {
                        if (GUILayout.Button(I18n.Tr("Import"), GUILayout.ExpandWidth(false)))
                        {
                            countEntries(this.importSrc.text);
                            PlaylistData pd = script.storage;
                            if (pd == null) pd = script.GetComponentInChildren<PlaylistData>(true);
                            if (entriesCount > 100 && pd == null)
                            {
                                var go = new GameObject("Playlist Data");
                                Undo.RegisterCreatedObjectUndo(go, "Undo Playlist Data add");
                                go.transform.SetParent(script.transform);
                                pd = go.AddComponent<PlaylistData>();
                            }

                            if (pd != script.storage) SetVariableByName(nameof(script.storage), pd);
                            parseContent(this.importSrc.text);
                            updateMode = ChangeAction.UPDATEALL;
                        }
                        else if (entriesCount == 0) countEntries(this.importSrc.text);
                    }
                }
            }
            else
            {
                importSrc = script._EDITOR_importSrc;
                using (HArea)
                {
                    if (GUILayout.Button(I18n.Tr("Add Entry"), GUILayout.MaxWidth(100f)))
                        updateMode = ChangeAction.ADD;

                    using (DisabledScope(rawEntryCount == 0))
                        if (GUILayout.Button(I18n.Tr("Remove All"), GUILayout.MaxWidth(100f)))
                            updateMode = ChangeAction.REMOVEALL;
                }
            }

            var urlCount = rawEntryCount;
            var currentPage = currentFocus / perPage;
            var maxPage = urlCount / perPage;
            var oldFocus = currentFocus;
            using (HArea)
            {
                using (DisabledScope(currentPage == 0))
                    if (GUILayout.Button("<<"))
                        currentFocus -= perPage;
                using (DisabledScope(currentFocus == 0))
                    if (GUILayout.Button("<"))
                        currentFocus -= 1;
                currentFocus = EditorGUILayout.IntSlider(currentFocus, 0, urlCount - 1, GUILayout.ExpandWidth(true));
                if (currentFocus != lastFocus)
                {
                    recachePage = true;
                    lastFocus = currentFocus;
                }

                GUILayout.Label($"/ {urlCount}");

                using (DisabledScope(currentFocus == urlCount))
                    if (GUILayout.Button(">"))
                        currentFocus += 1;
                using (DisabledScope(currentPage == maxPage))
                    if (GUILayout.Button(">>"))
                        currentFocus += perPage;
            }

            if (oldFocus != currentFocus)
            {
                updateMode = ChangeAction.UPDATEVIEW;
            }

            EditorGUILayout.EndVertical(); // end 2
            EditorGUILayout.EndHorizontal(); // end 1
        }

        private static void processLines(string text, Func<string, bool> process)
        {
            int currentIndex = 0;
            int endIndex = text.Length;
            while (currentIndex < endIndex)
            {
                int nextIndex = text.IndexOf('\n', currentIndex);
                if (nextIndex == -1) nextIndex = endIndex;
                string line = text.Substring(currentIndex, nextIndex - currentIndex).Trim();
                currentIndex = nextIndex += 1;
                if (!process.Invoke(line)) break;
            }
        }

        private void countEntries(string text)
        {
            entriesCount = 0;
            imagesCount = 0;
            if (string.IsNullOrWhiteSpace(text)) return;
            text = text.Trim();
            var startInd = EntryStartIndicator;
            var imgInd = EntryImageIndicator;
            bool customIndicators = false;
            processLines(text, line =>
            {
                if (!customIndicators && line.StartsWith(EntryIndicatorsPrefix))
                {
                    customIndicators = true;
                    var indicators = parseIndicators(line);
                    startInd = indicators[0];
                    imgInd = indicators[2];
                }
                else if (line.StartsWith(startInd))
                {
                    customIndicators = true;
                    entriesCount++;
                }
                else if (line.StartsWith(imgInd))
                {
                    customIndicators = true;
                    imagesCount++;
                }

                return true;
            });
        }

        private static string[] parseIndicators(string line)
        {
            line = line.Substring(EntryIndicatorsPrefix.Length).Trim();
            var re = new Regex(line.Contains(" ") ? " +" : "");
            string[] indicators = re.Split(line).Where(s => s.Length > 0).ToArray();
            const int reqLength = 5;
            if (indicators.Length != reqLength)
            {
                var sb = new StringBuilder();
                sb.Append("Custom indicators line defined, but not enough indicators are provided.\n");
                sb.Append($"You must have {reqLength} symbols defined for the following indicators in the given order: ");
                sb.Append("Main URL, Alternate URL, Image Location, Tags, Title\n");
                sb.Append($"Eg: <color=green>? {EntryStartIndicator} {EntryAltIndicator} {EntryImageIndicator} {EntryTagIndicator} {EntryTitleIndicator}</color>\n");
                sb.Append($"Detected: [{string.Join(", ", indicators)}]");
                Debug.LogWarning(sb.ToString());
                return null;
            }

            return indicators;
        }

        private void parseContent(string text)
        {
            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            text = text.Trim();
            var mainInd = EntryStartIndicator;
            var altInd = EntryAltIndicator;
            var imageInd = EntryImageIndicator;
            var tagsInd = EntryTagIndicator;
            var titleInd = EntryTitleIndicator;

            processLines(text, line =>
            {
                // keep searching for the indicators line prefix until it's found
                if (line.StartsWith(EntryIndicatorsPrefix))
                {
                    var indicators = parseIndicators(line);

                    mainInd = indicators[0];
                    altInd = indicators[1];
                    imageInd = indicators[2];
                    tagsInd = indicators[3];
                    titleInd = indicators[4];
                    Debug.Log($"Importing with custom indicators: Main <color=lightblue>{mainInd}</color> Alt <color=lightblue>{altInd}</color> Image <color=lightblue>{imageInd}</color> Tags <color=lightblue>{tagsInd}</color> Title <color=lightblue>{titleInd}</color>");
                    return false;
                }

                // or until the default start indicator is found
                return !line.StartsWith(EntryStartIndicator);
            });

            mainUrls = new VRCUrl[entriesCount];
            alternateUrls = new VRCUrl[entriesCount];
            titles = new string[entriesCount];
            descriptions = new string[entriesCount];
            tags = new string[entriesCount];
            images = new Sprite[entriesCount];
            var count = -1;
            bool foundAlt = false;
            bool foundTagString = false;
            bool foundImage = false;
            bool foundTitle = false;
            bool foundDescription = false;
            string currentDescription = "";
            uint missingTitles = 0;

            processLines(text, line =>
            {
                if (line.StartsWith(mainInd))
                {
                    if (count > -1)
                    {
                        if (string.IsNullOrEmpty(titles[count]))
                        {
                            titles[count] = "";
                            Debug.Log($"1 Missing title at index {count}");
                            // ReSharper disable once AccessToModifiedClosure
                            missingTitles++;
                        }

                        descriptions[count] = currentDescription.Trim();
                        currentDescription = "";
                        foundAlt = false;
                        foundTagString = false;
                        foundImage = false;
                        foundTitle = false;
                        foundDescription = false;
                    }

                    count++;
                    mainUrls[count] = new VRCUrl(line.Substring(mainInd.Length).Trim());
                    if (script._EDITOR_autofillAltURL)
                    {
                        var targetUrl = mainUrls[count].Get();
                        if (script._EDITOR_autofillEscape) targetUrl = Uri.EscapeDataString(targetUrl);
                        alternateUrls[count] = new VRCUrl(script._EDITOR_autofillFormat.Replace("$URL", targetUrl));
                    }

                    return true;
                }

                if (count == -1) return true;
                if (!foundDescription && !foundAlt && line.StartsWith(altInd))
                {
                    alternateUrls[count] = new VRCUrl(line.Substring(altInd.Length));
                    foundAlt = true;
                    return true;
                }

                if (!foundDescription && !foundImage && line.StartsWith(imageInd))
                {
                    string assetFile = line.Substring(imageInd.Length);
                    images[count] = (Sprite)AssetDatabase.LoadAssetAtPath(assetFile, typeof(Sprite));
                    foundImage = true;
                    return true;
                }

                if (!foundDescription && !foundTagString && line.StartsWith(tagsInd))
                {
                    tags[count] = sanitizeTagString(line.Substring(tagsInd.Length));
                    foundTagString = true;
                    return true;
                }

                if (!foundDescription && !foundTitle && line.StartsWith(titleInd))
                {
                    titles[count] = line.Substring(titleInd.Length);
                    foundTitle = true;
                    return true;
                }

                // for the implicit title and description checks, ignore empty lines if a description has not yet been found
                if (!foundDescription && line.Length == 0) return true;

                // Title is either a line prefixed with the title indicator (defaults to ~)
                // Or the first non-prefixed line encountered
                // For example, this is valid
                //
                // @myurl
                // ^alturl
                // ~Title line 1
                // Finally it's the description of the entry because there is no prefix and the title was already declared
                // ~Because the description is already found, this is just another line of the description
                // ~even though we start it with the indicator for the title
                // with new lines and all that jazz
                if (!foundDescription && !foundTitle)
                {
                    titles[count] = line;
                    foundTitle = true;
                    // if the implicit title has been found, no other prefix lines should be considered
                    // force the remainder of the entry to be part of the description
                    foundDescription = true;
                    return true;
                }

                // any subsequent line is part of the description
                if (currentDescription.Length > 0) currentDescription += '\n';
                currentDescription += line.Trim();
                foundDescription = true;
                return true;
            });

            if (count > -1)
            {
                descriptions[count] = currentDescription.Trim();
                if (string.IsNullOrWhiteSpace(titles[count]))
                {
                    missingTitles++;
                    Debug.Log($"2 Missing title at index {count}");
                }
            }

            if (missingTitles > 0)
            {
                Debug.LogWarning($"Just a heads up, this playlist has {missingTitles} entries that don't have any titles.");
            }

            stopwatch.Stop();
            Debug.Log($"Imported {mainUrls.Length} playlist entries in {stopwatch.ElapsedMilliseconds}ms");
        }

        private string pickle()
        {
            StringBuilder s = new StringBuilder();
            s.Append(EntryIndicatorsPrefix)
                .Append(EntryStartIndicator)
                .Append(EntryAltIndicator)
                .Append(EntryImageIndicator)
                .Append(EntryTagIndicator)
                .Append(EntryTitleIndicator)
                .Append("\n\n");
            for (int i = 0; i < mainUrls.Length; i++)
            {
                var url = mainUrls[i];
                s.Append(EntryStartIndicator).Append(url?.Get() ?? string.Empty).Append("\n");

                var alt = alternateUrls[i];
                if (alt != null && !string.IsNullOrWhiteSpace(alt.Get())) s.Append(EntryAltIndicator).Append(alt?.Get() ?? string.Empty).Append("\n");

                var image = images[i];
                if (image != null) s.Append(EntryImageIndicator).Append(AssetDatabase.GetAssetPath(image.texture)).Append("\n");

                var tag = tags[i];
                if (!string.IsNullOrWhiteSpace(tag)) s.Append(EntryTagIndicator).Append(tag).Append("\n");

                var title = titles[i];
                if (!string.IsNullOrWhiteSpace(title))
                {
                    string[] titleLines = title.Split('\n');
                    foreach (string line in titleLines) s.Append(EntryTitleIndicator).Append(line).Append("\n");
                }

                var description = descriptions[i];
                if (!string.IsNullOrWhiteSpace(description)) s.AppendLine(description);

                s.AppendLine("");
            }

            return s.ToString();
        }

        private int recacheListPage()
        {
            var currentPage = currentFocus / perPage;
            var maxPage = rawEntryCount / perPage;
            currentPageStart = currentPage * perPage;
            currentPageEnd = Math.Min(rawEntryCount, currentPageStart + perPage);

            var pageLength = currentPageEnd - currentPageStart;
            currentPageUrls = new VRCUrl[pageLength];
            currentPageAlts = new VRCUrl[pageLength];
            currentPageTitles = new string[pageLength];
            currentPageDescriptions = new string[pageLength];
            currentPageTags = new string[pageLength];
            currentPageImages = new Sprite[pageLength];
            System.Array.Copy(mainUrls, currentPageStart, currentPageUrls, 0, pageLength);
            System.Array.Copy(alternateUrls, currentPageStart, currentPageAlts, 0, pageLength);
            System.Array.Copy(titles, currentPageStart, currentPageTitles, 0, pageLength);
            System.Array.Copy(descriptions, currentPageStart, currentPageDescriptions, 0, pageLength);
            System.Array.Copy(tags, currentPageStart, currentPageTags, 0, pageLength);
            System.Array.Copy(images, currentPageStart, currentPageImages, 0, pageLength);
            recachePage = false;
            return currentPageStart;
        }

        private void showListEntries()
        {
            if (recachePage) recacheListPage();
            var height = Mathf.Min(330f, perPage * 55f) + 15f; // cap size at 330 + 15 for spacing for the horizontal scroll bar
            Spacer();
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Height(height)); // 1
            EditorGUI.BeginDisabledGroup(importFromFile); // 2
            for (var pageIndex = 0; pageIndex < currentPageUrls.Length; pageIndex++)
            {
                int rawIndex = currentPageStart + pageIndex;
                EditorGUILayout.BeginHorizontal(); // 3
                EditorGUILayout.BeginVertical(); // 4
                bool mainUrlUpdated = false;
                // URL field management
                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.LabelField($"{rawIndex}) PC Url", GUILayout.MaxWidth(100f), GUILayout.ExpandWidth(false));
                    var oldUrl = currentPageUrls[pageIndex] ?? VRCUrl.Empty;
                    var url = new VRCUrl(EditorGUILayout.TextField(oldUrl.Get(), GUILayout.ExpandWidth(true)));
                    if (url.Get() != oldUrl.Get())
                    {
                        updateMode = ChangeAction.UPDATESELF;
                        mainUrls[rawIndex] = url;
                        mainUrlUpdated = true;
                    }
                }

                // ALT field management
                using (HArea)
                {
                    EditorGUILayout.LabelField($"     Android Url", GUILayout.MaxWidth(100f), GUILayout.ExpandWidth(false));
                    var oldAlt = currentPageAlts[pageIndex] ?? VRCUrl.Empty;
                    var alt = new VRCUrl(EditorGUILayout.TextField(oldAlt.Get(), GUILayout.ExpandWidth(true)));
                    if (mainUrlUpdated && script._EDITOR_autofillAltURL)
                    {
                        var targetUrl = mainUrls[rawIndex].Get();
                        if (script._EDITOR_autofillEscape) targetUrl = Uri.EscapeDataString(targetUrl);
                        alt = new VRCUrl(script._EDITOR_autofillFormat.Replace("$URL", targetUrl));
                    }

                    if (alt.Get() != oldAlt.Get())
                    {
                        updateMode = ChangeAction.UPDATESELF;
                        alternateUrls[rawIndex] = alt;
                    }
                }

                // TITLE field management
                using (HArea)
                {
                    EditorGUILayout.LabelField("     Title", GUILayout.MaxWidth(100f), GUILayout.ExpandWidth(false));
                    var title = EditorGUILayout.TextArea(currentPageTitles[pageIndex], GUILayout.Width(250f), GUILayout.ExpandWidth(true));
                    if (title != currentPageTitles[pageIndex])
                    {
                        updateMode = ChangeAction.UPDATESELF;
                        titles[rawIndex] = title.Trim();
                    }
                }

                // TODO make this a less stupid layout, make it look nicer
                // DESCRIPTION field management
                using (HArea)
                {
                    EditorGUILayout.LabelField("     Description", GUILayout.MaxWidth(100f), GUILayout.ExpandWidth(false));
                    var description = EditorGUILayout.TextArea(currentPageDescriptions[pageIndex], GUILayout.Width(250f), GUILayout.ExpandWidth(true));
                    if (description != currentPageDescriptions[pageIndex])
                    {
                        updateMode = ChangeAction.UPDATESELF;
                        descriptions[rawIndex] = description.Trim();
                    }
                }

                // TAGS field management
                using (HArea)
                {
                    EditorGUILayout.LabelField("     Tags", GUILayout.MaxWidth(100f), GUILayout.ExpandWidth(false));
                    var tagString = EditorGUILayout.TextArea(currentPageTags[pageIndex], GUILayout.Width(250f), GUILayout.ExpandWidth(true));
                    if (tagString != currentPageTags[pageIndex])
                    {
                        updateMode = ChangeAction.UPDATESELF;
                        tags[rawIndex] = sanitizeTagString(tagString);
                    }
                }

                EditorGUILayout.EndVertical(); // end 4
                var image = (Sprite)EditorGUILayout.ObjectField(currentPageImages[pageIndex], typeof(Sprite), false, GUILayout.Height(75), GUILayout.Width(60));
                if (image != currentPageImages[pageIndex])
                {
                    updateMode = ChangeAction.UPDATESELF;
                    images[rawIndex] = image;
                }

                if (!importFromFile)
                {
                    // Playlist entry actions
                    EditorGUILayout.BeginVertical(); // 4
                    if (GUILayout.Button(I18n.Tr("Remove")))
                    {
                        // Cannot modify urls list within loop else index error occurs
                        targetEntry = rawIndex;
                        updateMode = ChangeAction.REMOVE;
                    }

                    // Playlist entry ordering
                    using (HArea)
                    {
                        EditorGUI.BeginDisabledGroup(rawIndex == 0);
                        if (GUILayout.Button(I18n.Tr("Up")))
                        {
                            targetEntry = rawIndex;
                            updateMode = ChangeAction.MOVEUP;
                        }

                        EditorGUI.EndDisabledGroup();
                        EditorGUI.BeginDisabledGroup(rawIndex + 1 == rawEntryCount);
                        if (GUILayout.Button(I18n.Tr("Down")))
                        {
                            targetEntry = rawIndex;
                            updateMode = ChangeAction.MOVEDOWN;
                        }

                        EditorGUI.EndDisabledGroup();
                    }

                    EditorGUILayout.EndVertical(); // end 4
                }

                EditorGUILayout.EndHorizontal(); // end 3
                GUILayout.Space(3f);
            }

            EditorGUI.EndDisabledGroup(); // end 2
            EditorGUILayout.EndScrollView(); // end 1
        }

        private string sanitizeTagString(string tagString)
        {
            // sanitize the tags to reduce the number of externs required for udon processing
            var tagList = tagString.Split(',');
            for (int k = 0; k < tagList.Length; k++)
            {
                var tag = tagList[k];
                tag = tag.ToLower();
                if (tag.Contains(":"))
                {
                    int idx = tag.IndexOf(':');
                    if (idx > -1)
                    {
                        var tGroup = tag.Substring(0, idx).Trim();
                        var tValue = tag.Substring(idx + 1).Trim();
                        tag = tGroup + ':' + tValue;
                    }
                    else tag = tag.Trim();

                    tagList[k] = tag;
                }
                else tagList[k] = tag.Trim();
            }

            return string.Join(",", tagList);
        }

        #region Scene Updates

        private void updateScene()
        {
            if (script.scrollView == null || script.scrollView.viewport == null)
            {
                Debug.LogError("ScrollRect or associated viewport is null. Ensure they are connected in the inspector.");
                return;
            }

            switch (updateMode)
            {
                case ChangeAction.ADD:
                    addItems();
                    break;
                case ChangeAction.MOVEUP:
                    moveItems(targetEntry, targetEntry - 1);
                    break;
                case ChangeAction.MOVEDOWN:
                    moveItems(targetEntry, targetEntry + 1);
                    break;
                case ChangeAction.REMOVE:
                    removeItems(targetEntry);
                    break;
                case ChangeAction.REMOVEALL:
                    removeAll();
                    break;
            }

            targetEntry = -1;
            switch (updateMode)
            {
                case ChangeAction.UPDATEVIEW:
                case ChangeAction.UPDATESELF:
                    UpdateContents(script, currentFocus);
                    break;
                case ChangeAction.UPDATEALL:
                    RebuildScene(script, currentFocus);
                    break;
                default:
                    RebuildScene(script, currentFocus);
                    break;
            }
        }

        private void addItems()
        {
            var newIndex = mainUrls.Length;
            Debug.Log($"Adding playlist item. New size {newIndex + 1}");
            mainUrls = AddArrayItem(mainUrls);
            alternateUrls = AddArrayItem(alternateUrls);
            tags = AddArrayItem(tags);
            titles = AddArrayItem(titles);
            descriptions = AddArrayItem(descriptions);
            images = AddArrayItem(images);
            // Make sure the urls default to an empty instead of null
            mainUrls[newIndex] = VRCUrl.Empty;
            alternateUrls[newIndex] = VRCUrl.Empty;
        }

        private void removeItems(int index)
        {
            Debug.Log($"Removing playlist item {index}: {titles[index]}");
            mainUrls = RemoveArrayItem(mainUrls, index);
            alternateUrls = RemoveArrayItem(alternateUrls, index);
            tags = RemoveArrayItem(tags, index);
            titles = RemoveArrayItem(titles, index);
            descriptions = RemoveArrayItem(descriptions, index);
            images = RemoveArrayItem(images, index);
        }

        private void moveItems(int from, int to)
        {
            // no change needed
            if (from == to) return;
            Debug.Log($"Moving playlist item {from} -> {to}");

            mainUrls = MoveArrayItem(mainUrls, from, to);
            alternateUrls = MoveArrayItem(alternateUrls, from, to);
            tags = MoveArrayItem(tags, from, to);
            titles = MoveArrayItem(titles, from, to);
            descriptions = MoveArrayItem(descriptions, from, to);
            images = MoveArrayItem(images, from, to);
        }

        private void removeAll()
        {
            Debug.Log($"Removing all {mainUrls.Length} playlist items");
            mainUrls = new VRCUrl[0];
            alternateUrls = new VRCUrl[0];
            tags = new string[0];
            titles = new string[0];
            descriptions = new string[0];
            images = new Sprite[0];
        }

        public static void RebuildScene(Playlist playlist, int offset = 0)
        {
            if (playlist == null) return;
            if (playlist.scrollView == null || playlist.listContainer == null || playlist.template == null)
            {
#pragma warning disable CS0618
                UnityEngine.Debug.LogError($"Playlist {playlist.transform.GetHierarchyPath()} is missing required UI components. Cannot rebuild playlist in scene.");
#pragma warning restore CS0618
                return;
            }

            // determine how many entries can be shown within the physical space of the viewport
            var visible = calculateVisibleEntries(playlist);
            // destroy and rebuild the list of entries for the visibleCount
            rebuildEntries(playlist, visible);
            // re-organize the layout to the viewport's size
            recalculateLayout(playlist);
            // update the internal content of each entry with in the range of visibleOffset -> visibleOffset + visibleCount and certain constraints
            UpdateContents(playlist, offset);
            // ensure the attached scrollbar has the necessary event listener attached
            attachScrollbarEvent(playlist);
        }

        private static int calculateVisibleEntries(Playlist playlist)
        {
            // calculate the x/y entry counts
            Rect max = playlist.scrollView.viewport.rect;
            Rect item = ((RectTransform)playlist.template.transform).rect;
            var horizontalCount = Mathf.FloorToInt(max.width / item.width);
            var verticalCount = Mathf.FloorToInt(max.height / item.height) + 1; // allows Y overflow for better visual flow
            var mainUrls = playlist.storage == null ? playlist.mainUrls : playlist.storage.mainUrls;
            return Mathf.Min(mainUrls.Length, horizontalCount * verticalCount);
        }

        private static void rebuildEntries(Playlist playlist, int visible)
        {
            // clear existing entries
            while (playlist.listContainer.childCount > 0) DestroyImmediate(playlist.listContainer.GetChild(0).gameObject);
            // rebuild entries list
            for (int i = 0; i < visible; i++) createEntry(playlist);
        }

        private static void createEntry(Playlist playlist)
        {
            // create scene entry
            GameObject entry = Instantiate(playlist.template, playlist.listContainer, false);
            entry.name = $"Entry ({playlist.listContainer.childCount})";
            entry.transform.SetAsLastSibling();

            var behavior = UdonSharpEditorUtility.GetBackingUdonBehaviour(playlist);
            var button = entry.GetComponentInChildren<Button>();

            if (playlist.selectAction == null)
            {
                // trigger isn't present, put one on the template root
                button = entry.AddComponent<Button>();
                button.transition = Selectable.Transition.None;
                button.navigation = new Navigation { mode = Navigation.Mode.None };
            }

            // clear old listners
            while (button.onClick.GetPersistentEventCount() > 0)
                UnityEventTools.RemovePersistentListener(button.onClick, 0);

            // set UI event sequence for the button
            UnityAction<bool> interactable = System.Delegate.CreateDelegate(typeof(UnityAction<bool>), button, "set_interactable") as UnityAction<bool>;
            UnityEventTools.AddBoolPersistentListener(button.onClick, interactable, false);
            UnityEventTools.AddStringPersistentListener(button.onClick, behavior.SendCustomEvent, nameof(playlist.SwitchEntry));
            UnityEventTools.AddBoolPersistentListener(button.onClick, interactable, true);
            entry.SetActive(true);
        }

        private static void recalculateLayout(Playlist playlist)
        {
            // ensure the content box fills exactly 100% of the viewport.
            playlist.listContainer.SetParent(playlist.scrollView.viewport);
            playlist.listContainer.anchorMin = new Vector2(0, 0);
            playlist.listContainer.anchorMax = new Vector2(1, 1);
            playlist.listContainer.sizeDelta = new Vector2(0, 0);
            var max = playlist.listContainer.rect;
            float maxWidth = max.width;
            float maxHeight = max.height;
            int col = 0;
            int row = 0;
            // template always assumes the anchor PIVOT is located at X=0.0 and Y=1.0 (aka upper left corner)
            // TODO enforce this assumption
            // TODO Take the left-right margins into account for spacing
            // should be able to make the assumption that all entries are the same structure (thus width/height) as template
            Rect tmpl = ((RectTransform)playlist.template.transform).rect;
            float entryHeight = tmpl.height;
            float entryWidth = tmpl.width;
            float listHeight = entryHeight;
            bool firstEntry = true;
            for (int i = 0; i < playlist.listContainer.childCount; i++)
            {
                RectTransform entry = (RectTransform)playlist.listContainer.GetChild(i);
                // expect fill in left to right.
                var X = entryWidth * col;
                // detect if a new row is needed, first row will be row 0 implicitly
                if (firstEntry) firstEntry = false;
                else if (X + entryWidth > maxWidth)
                {
                    // reset the horizontal data
                    col = 0;
                    X = 0f;
                    // horizontal exceeds the shape of the container, shift to the next row
                    row++;
                }

                // calculate the target row
                var Y = entryHeight * row;
                entry.anchoredPosition = new Vector2(X, -Y);
                col++; // target next column
            }

            playlist.scrollView.CalculateLayoutInputVertical();
        }

        private static int calculateVisibleOffset(Playlist playlist, int rawOffset)
        {
            var mainUrls = playlist.storage == null ? playlist.mainUrls : playlist.storage.mainUrls;
            Rect max = playlist.scrollView.viewport.rect;
            Rect item = ((RectTransform)playlist.template.transform).rect;
            var horizontalCount = Mathf.FloorToInt(max.width / item.width);
            if (horizontalCount == 0) horizontalCount = 1;
            var verticalCount = Mathf.FloorToInt(max.height / item.height);
            // limit offset to the url max minus the last "page", account for the "extra" overflow row as well.
            var maxRow = (mainUrls.Length - 1) / horizontalCount + 1;
            var contentHeight = maxRow * item.height;
            // clamp the min/max row to the view area boundries
            maxRow = Mathf.Min(maxRow, maxRow - verticalCount);
            if (maxRow == 0) maxRow = 1;

            var maxOffset = maxRow * horizontalCount;
            var currentRow = rawOffset / horizontalCount; // int DIV causes stepped values
            var steppedOffset = currentRow * horizontalCount;
            // currentOffset will be smaller than maxOffset when the scroll limit has not yet been reached
            var targetOffset = Mathf.Min(steppedOffset, maxOffset);

            // update the scrollview content proxy's height
            float scrollHeight = Mathf.Max(contentHeight, max.height + item.height / 2);
            playlist.scrollView.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, scrollHeight);
            if (playlist.scrollView.verticalScrollbar != null)
                playlist.scrollView.verticalScrollbar.value = 1f - (float)rawOffset / (maxOffset);

            return Mathf.Max(0, targetOffset);
        }

        public static void UpdateContents(Playlist playlist, int focus)
        {
            int playlistIndex = calculateVisibleOffset(playlist, focus);
            var hasStorage = playlist.storage != null;
            var mainUrls = hasStorage ? playlist.storage.mainUrls : playlist.mainUrls;
            int numOfUrls = mainUrls.Length;
            for (int i = 0; i < playlist.listContainer.childCount; i++)
            {
                if (playlistIndex >= numOfUrls)
                {
                    // urls have exceeded count, hide the remaining entries
                    playlist.listContainer.GetChild(i).gameObject.SetActive(false);
                    continue;
                }

                var entry = playlist.listContainer.GetChild(i);
                entry.gameObject.SetActive(true);
                var titles = hasStorage ? playlist.storage.titles : playlist.titles;
                var descriptions = hasStorage ? playlist.storage.descriptions : playlist.descriptions;
                var images = hasStorage ? playlist.storage.images : playlist.images;

                var currentUrl = mainUrls[playlistIndex].Get();
                var currentTitle = titles[playlistIndex];
                var currentDescription = descriptions[playlistIndex];
                var currentImage = images[playlistIndex];

                if (currentTitle.StartsWith("~")) currentTitle = currentTitle.Substring(1);

                Transform t;

                if (playlist.urlDisplay != null)
                {
                    t = entry;
                    if (playlist.urlDisplayTmplPath != "") t = entry.Find(playlist.urlDisplayTmplPath);
                    var component = t.GetComponent<Text>();
                    component.text = currentUrl;
                    EditorUtility.SetDirty(component);
                }

                if (playlist.urlDisplayTMP != null)
                {
                    t = entry;
                    if (playlist.urlDisplayTMPTmplPath != "") t = entry.Find(playlist.urlDisplayTMPTmplPath);
                    var component = t.GetComponent<TextMeshProUGUI>();
                    component.text = currentUrl;
                    EditorUtility.SetDirty(component);
                }

                if (playlist.titleDisplay != null)
                {
                    t = entry;
                    if (playlist.titleDisplayTmplPath != "") t = entry.Find(playlist.titleDisplayTmplPath);
                    var component = t.GetComponent<Text>();
                    component.text = currentTitle;
                    EditorUtility.SetDirty(component);
                }

                if (playlist.titleDisplayTMP != null)
                {
                    t = entry;
                    if (playlist.titleDisplayTMPTmplPath != "") t = entry.Find(playlist.titleDisplayTMPTmplPath);
                    var component = t.GetComponent<TextMeshProUGUI>();
                    component.text = currentTitle;
                    EditorUtility.SetDirty(component);
                }

                if (playlist.descriptionDisplay != null)
                {
                    t = entry;
                    if (playlist.descriptionDisplayTmplPath != "") t = entry.Find(playlist.descriptionDisplayTmplPath);
                    var component = t.GetComponent<Text>();
                    component.text = currentDescription;
                    EditorUtility.SetDirty(component);
                }

                if (playlist.descriptionDisplayTMP != null)
                {
                    t = entry;
                    if (playlist.descriptionDisplayTMPTmplPath != "") t = entry.Find(playlist.descriptionDisplayTMPTmplPath);
                    var component = t.GetComponent<TextMeshProUGUI>();
                    component.text = currentDescription;
                    EditorUtility.SetDirty(component);
                }

                if (playlist.imageDisplay != null)
                {
                    t = entry;
                    if (playlist.imageDisplayTmplPath != "") t = entry.Find(playlist.imageDisplayTmplPath);
                    var component = t.GetComponent<Image>();
                    component.sprite = currentImage;
                    EditorUtility.SetDirty(component);
                }

                if (playlist.loadingBar != null)
                {
                    t = entry;
                    if (playlist.loadingBarTmplPath != "") t = entry.Find(playlist.loadingBarTmplPath);
                    var component = t.GetComponent<Slider>();
                    component.SetValueWithoutNotify(0f);
                    EditorUtility.SetDirty(component);
                }

                playlistIndex++;
            }
        }

        private static void attachScrollbarEvent(Playlist playlist)
        {
            ATEditorUtility.EnsureSelectableActionEvent(playlist.scrollView.verticalScrollbar, playlist.scrollView.verticalScrollbar.onValueChanged, playlist.UpdateView);
        }

        #endregion
    }

    [ScriptedImporter(1, "playlist")]
    public class PlaylistImporter : ScriptedImporter
    {
        public override void OnImportAsset(AssetImportContext ctx)
        {
            TextAsset subAsset = new TextAsset(File.ReadAllText(ctx.assetPath));
            ctx.AddObjectToAsset("text", subAsset);
            ctx.SetMainObject(subAsset);
        }
    }
}