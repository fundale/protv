using ArchiTech.SDK;
using ArchiTech.SDK.Editor;
using UnityEditor;

namespace ArchiTech.ProTV.Editor
{
    public abstract class TVPluginUIEditor : ATBehaviourEditor
    {
        protected static void DisplayTemplateError() => EditorGUILayout.HelpBox(I18n.Tr("Template components MUST be a descendant of the containing template object."), MessageType.Error);
    }
}