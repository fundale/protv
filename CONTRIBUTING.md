# Contribution

To contribute to this project, you must adhere to the general existing structure of the project.  
All PRs should be made against their respective dev branches. You can PR a release branch if no corresponding dev branch exists.

For any non-core features, such as special integrations with third-party tooling or custom skins/themes, 
please make the PR against the [ArchiTech.ProTV.Extras](https://gitlab.com/techanon/protv-extras) community contributions package instead.

---

A huge thanks to all the folks who contributed and/or assisted with testing on this project.
If you wish to help contribute to making this asset the best in the meta-verse, join the discord and let us know what you think: https://discord.gg/87nJmdDkBB

## Personal Thank Yous
- Literally Smith (testing, asset promotion, feature ideas, early adopter)
- Reava_ (for helping out with ironing out a bunch of stability issues with livestreams and helping out supporting other users)
- Air In (for pushing me forward with improving quest support even further)
- NaugieMon (for being such good sport with the tedious quest testing and helping with various graphics)
- Niyah (various bug reports)
- Vowgan (inspiration for getting into VRChat creation in the first place)
- Kitty Toes & Doxis (for pushing the playlist plugin to its limits)
- Inertial (for helping conceptualize a better structure for playlist performance improvements)
- KevBal (for helping chase down that stupid nested canvas issue, thanks unity)
- Beal (for creating the original 2.0 promo video pro-bono. Follow him on twitch.tv/bealthebuilder)
- EvanTheFloydian (for doing such a stellar job on the 2.3 promo video)
- iigo (video screen shader code help)
- OctoFloofy (reporting many issues and assistance with testing/debugging)
- JokerIsPunk (various bug reports)
- Varenon (much help with VPM setup and understanding, also vudon logger is useful)
- NestorBoy (massive help with shaders and render texture usage)
- BeastChild (extensive testing and feedback under production use-cases)
- Enverex (massive amount of testing and feedback during 3.x pre-release development)
- error.mdl & d4rkpl4y3r (for helping solve all the 3D shader issues including the mirror one)

## Asset Inclusions
- ChimCham (commissioned custom TV model)
- NaugieMon (artist/compositor for logos, retro icons and brand graphics)
- Shyaong (artist for neon icons)
- StudiousGluteous (contributed 3d logo)
- Merlin/Texelsaur (video screen aspect ratio shader code)
- Silent (parallax shader for cool looking posters)