﻿using System;
using ArchiTech.SDK;
using JetBrains.Annotations;
using TMPro;
using UdonSharp;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using VRC.SDKBase;
using VRC.Udon.Common.Enums;
using VRC.Udon.Common.Interfaces;
using Random = UnityEngine.Random;

// ReSharper disable ConvertToAutoPropertyWithPrivateSetter

namespace ArchiTech.ProTV
{
    internal enum PlaylistImportMode
    {
        NONE,
        FILE,
        URL
    }

    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    [DefaultExecutionOrder(-1)]
    public partial class Playlist : TVPlugin
    {
        public Queue queue;

        [SerializeField,
         I18nInspectorName("Playlist ScrollView")
        ]
        internal ScrollRect scrollView;

        [SerializeField, FormerlySerializedAs("content"),
         I18nInspectorName("Playlist Item Container")
        ]
        internal RectTransform listContainer;

        [SerializeField,
         I18nInspectorName("Playlist Item Template")
        ]
        internal GameObject template;

        [SerializeField,
         I18nTooltip("Text display component for the relevant entry URL. MUST be a child of the Template object. Supports both UI and TMP.")
        ]
        internal Text urlDisplay;

        [SerializeField] internal TextMeshProUGUI urlDisplayTMP;

        [SerializeField,
         I18nTooltip("Text display component for the relevant entry Title. MUST be a child of the Template object. Supports both UI and TMP.")
        ]
        internal Text titleDisplay;

        [SerializeField] internal TextMeshProUGUI titleDisplayTMP;

        [SerializeField,
         I18nTooltip("Text display component for the relevant entry Description. MUST be a child of the Template object. Supports both UI and TMP.")
        ]
        internal Text descriptionDisplay;

        [SerializeField] internal TextMeshProUGUI descriptionDisplayTMP;

        [SerializeField,
         I18nTooltip("Interaction component for relevant entry activation. MUST be a child of the Template object.")
        ]
        internal Button selectAction;

        [SerializeField,
         I18nTooltip("Image display component for the relevant entry image. MUST be a child of the Template object. Supports both UI and TMP.")
        ]
        internal Image imageDisplay;

        [SerializeField,
         I18nTooltip("Visual component for relevant entry loading progress. MUST be a child of the Template object.")
        ]
        internal Slider loadingBar;

        [SerializeField, HideInInspector] internal string urlDisplayTmplPath;
        [SerializeField, HideInInspector] internal string urlDisplayTMPTmplPath;
        [SerializeField, HideInInspector] internal string titleDisplayTmplPath;
        [SerializeField, HideInInspector] internal string titleDisplayTMPTmplPath;
        [SerializeField, HideInInspector] internal string descriptionDisplayTmplPath;
        [SerializeField, HideInInspector] internal string descriptionDisplayTMPTmplPath;
        [SerializeField, HideInInspector] internal string selectActionTmplPath;
        [SerializeField, HideInInspector] internal string loadingBarTmplPath;
        [SerializeField, HideInInspector] internal string imageDisplayTmplPath;

        [I18nInspectorName("Shuffle Playlist on Load")]
        public bool shuffleOnLoad = false;

        [I18nInspectorName("Preload Queue Amount"), Min(0)]
        public int queuePreloadAmount = 0;

        [I18nInspectorName("Autoplay?")] public bool autoplayList = false;

        [I18nInspectorName("Prioritize on Interact")]
        public bool prioritizeOnInteract = true;

        [I18nInspectorName("Autoplay On Load")]
        public bool autoplayOnLoad = true;

        [I18nInspectorName("Loop Playlist")] public bool loopPlaylist = true;

        [I18nInspectorName("Start From Random Entry")]
        public bool startFromRandomEntry = false;

        [I18nInspectorName("Continue From Last Known Entry")]
        public bool continueWhereLeftOff = true;

        [I18nInspectorName("Disable Autoplay on Interact")]
        public bool disableAutoplayOnInteract;

        [I18nInspectorName("Show Urls in Playlist?")]
        public bool showUrls;

        [FormerlySerializedAs("pcUrls"), FormerlySerializedAs("urls")]
        public VRCUrl[] mainUrls;

        [FormerlySerializedAs("questUrls"), FormerlySerializedAs("alts")]
        public VRCUrl[] alternateUrls;


        public string[] titles;
        public string[] descriptions;
        public string[] tags;
        public Sprite[] images;

        [SerializeField,
         I18nInspectorName("Playlist Storage"), I18nTooltip("If present, the playlist will store the entries data on the specified component instead of itself. This helps with editor performance when dealing with very large playlists.")
        ]
        internal PlaylistData storage;

        [NonSerialized] public int viewOffset = -1;
        [NonSerialized] public int IN_INDEX = -1;

        public override sbyte Priority => -10;

        // entry caches
        private Transform[] entryRefs;
        private Text[] urlDisplayRefs;
        private Text[] titleDisplayRefs;
        private Text[] descriptionDisplayRefs;
        private TextMeshProUGUI[] urlDisplayTMPRefs;
        private TextMeshProUGUI[] titleDisplayTMPRefs;
        private TextMeshProUGUI[] descriptionDisplayTMPRefs;
        private Button[] selectActionRefs;
        private Slider[] loadingBarRefs;
        private Image[] imageDisplayRefs;

        // A 1 to 1 array corresponding to each original entry specifying whether the element should be filtered (aka hidden) in the views.
        private bool[] hidden;

        // an array of the same size as the urls that stores corresponding references to the indexes within the urls array.
        // the order of this array is what gets modified when the playlist gets sorted.
        private int[] sortView = new int[0];

        // an array of a variable size (length of sortView or less) that represents the list of url indexes that are visible for rendering in the current view
        // this array also contains values which correspond to the indexes of the URL list, which may be non-sequential based on the sortView order
        private int[] filteredView = new int[0];
        private int filteredViewCount = 0;

        // an array that represents the visible render shown in the scene based on the filteredView array
        // unlike the previous two, this array's contents corresponds to indexes of the filteredView array
        // eg: to get the actual URL based on a particular entry of the current view, you'd access it via urls[filteredView[currentView[index]]]
        private int[] currentView = new int[0];
        private int currentSortViewIndex = -1;
        private bool isLoading = false;
        private Slider loading;
        private float loadingBarDamp;
        private float loadingPercent;
        private bool hasLoading;
        private bool hasQueue;
        private bool loadAutoplay;
        private PlaylistRPC rpc;

        private bool hasRPC;
        private bool hasUrlDisplay;
        private bool hasTitleDisplay;
        private bool hasDescriptionDisplay;
        private bool hasUrlDisplayTMP;
        private bool hasTitleDisplayTMP;
        private bool hasDescriptionDisplayTMP;
        private bool hasSelectAction;
        private bool hasLoadingBar;
        private bool hasImageDisplay;

        // These are not used during runtime, only for storing persistent data used by the custom inspector
        [SerializeField] internal TextAsset _EDITOR_importSrc;

        [SerializeField, FormerlySerializedAs("_EDITOR_manualToImport")]
        internal bool _EDITOR_importFromFile;

        [SerializeField] internal int _EDITOR_entriesCount;
        [SerializeField] internal int _EDITOR_imagesCount;
        [SerializeField] internal int _EDITOR_templateUpgrade;

        [SerializeField,
         I18nInspectorName("Import From")
        ]
        internal string _EDITOR_importUrl;

        [SerializeField,
         I18nInspectorName("Autofill Alternate Urls")
        ]
        internal bool _EDITOR_autofillAltURL;

        [SerializeField,
         I18nInspectorName("Autofill URL Escape")
        ]
        internal bool _EDITOR_autofillEscape;

        [SerializeField,
         I18nInspectorName("Autofill Format")
        ]
        internal string _EDITOR_autofillFormat = "$URL";

        // Getter Helpers
        public bool[] Hidden => hidden;
        public int[] SortView => sortView;
        public int[] FilteredView => filteredView;
        private int nextSortViewIndex => wrap(currentSortViewIndex + 1);
        private int prevSortViewIndex => wrap(currentSortViewIndex - 1);

        public int CurrentEntryIndex => currentSortViewIndex == -1 ? -1 : sortView[currentSortViewIndex];
        public int NextEntryIndex => currentSortViewIndex == -1 ? -1 : sortView[nextSortViewIndex];
        public int PrevEntryIndex => currentSortViewIndex == -1 ? -1 : sortView[prevSortViewIndex];

        [PublicAPI] public VRCUrl CurrentEntryMainUrl => currentSortViewIndex == -1 ? VRCUrl.Empty : mainUrls[sortView[currentSortViewIndex]];

        [PublicAPI] public VRCUrl CurrentEntryAlternateUrl => currentSortViewIndex == -1 ? VRCUrl.Empty : alternateUrls[sortView[currentSortViewIndex]];

        [PublicAPI] public string CurrentEntryTags => currentSortViewIndex == -1 ? string.Empty : tags[sortView[currentSortViewIndex]];

        [PublicAPI] public string CurrentEntryTitle => currentSortViewIndex == -1 ? string.Empty : titles[sortView[currentSortViewIndex]];

        [PublicAPI] public string CurrentEntryDescription => currentSortViewIndex == -1 ? string.Empty : descriptions[sortView[currentSortViewIndex]];

        [PublicAPI] public Sprite CurrentEntryImage => currentSortViewIndex == -1 ? null : images[sortView[currentSortViewIndex]];


        public override void Start()
        {
            if (init) return;
            SetLogPrefixColor("#ff8811");
            base.Start();
            if (storage != null)
            {
                mainUrls = storage.mainUrls;
                alternateUrls = storage.alternateUrls;
                titles = storage.titles;
                descriptions = storage.descriptions;
                tags = storage.tags;
                images = storage.images;
            }

            cacheEntryRefs();
            var len = mainUrls.Length;
            hidden = new bool[len];
            sortView = new int[len];
            filteredView = new int[len];
            ResetSortView();
            if (shuffleOnLoad) shuffle(sortView, 3);
            cacheFilteredView();
            hasQueue = queue != null;

            if (template != null) template.SetActive(false);
            if (titles.Length != len) Warn($"Titles count ({titles.Length}) doesn't match Urls count ({len}).");
            if (len == 0) Warn("No entries in the playlist.");
            else
            {
                if (autoplayList)
                {
                    if (startFromRandomEntry)
                        currentSortViewIndex = Mathf.FloorToInt(Random.Range(0f, 1f) * (sortView.Length - 1));
                    loadAutoplay = autoplayOnLoad && hasLocalPlayer && localPlayer.isMaster;
                }

                SeekView(sortViewIndexToFilteredViewIndex(currentSortViewIndex));
            }

            rpc = GetComponentInChildren<PlaylistRPC>(true);
            hasRPC = rpc != null;
            if (hasRPC) rpc.gameObject.SetActive(true);
        }

        public void UpdateLoadingBar()
        {
            if (isLoading)
            {
                SendCustomEventDelayedFrames(nameof(UpdateLoadingBar), 1, EventTiming.LateUpdate);
                if (hasLoading) loadingPercent = loading.value;
                if (loadingPercent > 0.95f) return;
                float dampSpeed = loadingPercent > 0.8f ? 0.4f : 0.3f;
                loadingPercent = Mathf.SmoothDamp(loadingPercent, 1f, ref loadingBarDamp, dampSpeed);
                if (hasLoading) loading.value = loadingPercent;
            }
        }

        [PublicAPI]
        public void FillQueue()
        {
            if (hasQueue) FillQueue(queue.AvailableSize);
        }

        [PublicAPI]
        public void FillQueue(int amount)
        {
            if (hasQueue && amount > 0)
            {
                queue.Start(); // ensure queue has been initialized
                while (amount > 0 && !queue.IsFull)
                {
                    currentSortViewIndex = nextSortViewIndex;
                    var nextIndex = sortView[currentSortViewIndex];
                    queue._AddEntry(mainUrls[nextIndex], alternateUrls[nextIndex], titles[nextIndex]);
                    amount--;
                }
            }
        }


        // === TV EVENTS ===

        #region TV Events

        public override void _TvReady()
        {
            // shortcircut the method if the user cannot play media or there has been a URL already loaded in the past.
            if (!IsTVOwner || !tv.CanPlayMedia || !tv.WaitingForMedia) return;
            if (hasQueue && queuePreloadAmount > 0) FillQueue(queuePreloadAmount);
            else if (loadAutoplay)
            {
                int nextIndex = sortView[nextSortViewIndex];
                if (hasQueue)
                {
                    queue.Start(); // ensure queue has been initialized
                    // if something else has not yet prepared an entry for the queue, go ahead
                    if (queue._CheckEntry(0, false))
                    {
                        if (hasRPC && prioritizeOnInteract) rpc.SendCustomNetworkEvent(NetworkEventTarget.All, nameof(PlaylistRPC.ALL_PRIORITIZE));
                        queue._AddEntry(mainUrls[nextIndex], alternateUrls[nextIndex], titles[nextIndex]);
                    }
                }
                else if (hasTV)
                {
                    // if something else has not yet assigned the autoplay, go ahead
                    var inurl = tv.IN_MAINURL;
                    if (inurl == null || string.IsNullOrWhiteSpace(inurl.Get()))
                    {
                        if (hasRPC && prioritizeOnInteract) rpc.SendCustomNetworkEvent(NetworkEventTarget.All, nameof(PlaylistRPC.ALL_PRIORITIZE));
                        tv._ChangeMedia(mainUrls[nextIndex], alternateUrls[nextIndex], titles[nextIndex]);
                    }
                }
            }
        }

        public override void _TvMediaEnd()
        {
            bool willPlay = autoplayList && tv.IsOwner;
            if (IsTraceEnabled) Trace($"Autoplay with Owner: {willPlay}");
            if (willPlay)
            {
                willPlay = !tv.loading && !tv.waiting; // check general TV state
                if (IsTraceEnabled) Trace($"General TV State: {willPlay}");
            }

            if (willPlay)
            {
                willPlay = loopPlaylist || currentSortViewIndex != mainUrls.Length - 1; // check if playlist can trigger the next entry 
                if (IsTraceEnabled) Trace($"Next Entry Available: {willPlay}");
            }

            if (willPlay)
            {
                willPlay = !hasQueue || queue.WillBeEmpty; // if queue is attached, make sure it's completely empty before queueing
                if (IsTraceEnabled) Trace($"Playable if Queue: {willPlay}");
            }

            if (willPlay)
            {
                // when the playlist should not continue at the last known entry,
                // and the active media is not part of the playlist,
                // reset the playlist to the start of the list,
                // otherwise switch to the next entry in the list.
                if (!continueWhereLeftOff && currentSortViewIndex == -1)
                    SwitchEntry(0);
                else SwitchEntry(nextSortViewIndex);
            }
        }

        public override void _TvMediaChange()
        {
            // Examine the logic of findActualSortViewIndex to make sure it's finding things correctly.
            if (autoplayList)
            {
                var index = findActualSortViewIndex();
                if (index > -1) currentSortViewIndex = index;
                else if (!continueWhereLeftOff) currentSortViewIndex = -1;
            }
            else currentSortViewIndex = findActualSortViewIndex();

            if (IsTraceEnabled) Trace($"Media Change: new sort view index {currentSortViewIndex}");
            retargetActive();
        }

        public override void _TvVideoPlayerError()
        {
            if (!autoplayList) return;
            if (!tv.IsOwner && !tv.ownerDisabled) return; // when the owner not the local user but is avaialable, ignore the event.
            if (tv.errorState != TVErrorState.FAILED) return; // only proceed if the tv signals that an error has actually occurred.
            if (tv.LoadingMedia) return; // skip if another plugin has already switched to another entry.
            if (loopPlaylist || currentSortViewIndex != mainUrls.Length - 1)
            {
                var nextIndex = nextSortViewIndex;
                if (IsInfoEnabled) Info($"Error detected. Switching to next entry {nextIndex}.");
                SwitchEntry(nextIndex);
            }
        }

        public override void _TvLoading()
        {
            isLoading = true;
            loadingPercent = 0f;
            if (hasLoading) loading.value = 0f;
            UpdateLoadingBar();
        }

        public override void _TvLoadingEnd()
        {
            isLoading = false;
            loadingPercent = 1f;
            if (hasLoading) loading.value = 1f;
        }

        public override void _TvLoadingAbort()
        {
            isLoading = false;
            loadingPercent = 0f;
            if (hasLoading) loading.value = 0f;
        }

        #endregion

        #region UI EVents

        public void Next()
        {
            SwitchEntry(nextSortViewIndex);
        }

        public void Previous()
        {
            SwitchEntry(prevSortViewIndex);
        }

        public void UpdateView()
        {
            if (!init) return;
            int filteredViewIndex = 0;
            // must have scrollbar and scrollbar must be visible
            if (scrollView.verticalScrollbar != null && scrollView.content.rect.height > scrollView.viewport.rect.height)
            {
                var percent = (1f - scrollView.verticalScrollbar.value);
                Rect max = scrollView.viewport.rect;
                Rect item = ((RectTransform)template.transform).rect;
                var horizontalCount = Mathf.FloorToInt(max.width / item.width);
                if (horizontalCount == 0) horizontalCount = 1;
                var verticalCount = Mathf.FloorToInt(max.height / item.height);
                filteredViewIndex = Mathf.FloorToInt(percent * (filteredViewCount - verticalCount * horizontalCount + horizontalCount));
            }

            if (updateView(filteredViewIndex, false)) retargetActive();
        }

        public void Shuffle()
        {
            Info("Randomizing sort");
            shuffle(sortView, 3);
            cacheFilteredView(); // must recache the filtered view after a shuffle to update to the new sortView order
            SeekView(0);
        }

        public void ResetSort()
        {
            Info("Resetting sort to default");
            ResetSortView();
            cacheFilteredView(); // must recache the filtered view after a shuffle to update to the new sortView order
            SeekView(0);
        }

        public void AutoPlay()
        {
            if (autoplayList) return; // already autoplay, skip
            if (IsDebugEnabled) Debug($"Playlist autoplay enabled via _AutoPlay.");
            if (startFromRandomEntry) currentSortViewIndex = Mathf.FloorToInt(Random.Range(0f, 1f) * sortView.Length - 1);
            if (tv.stateOwner != TVPlayState.PLAYING && !tv.loading && !tv.waiting) SwitchEntry(currentSortViewIndex);
            autoplayList = true;
        }

        public void ManualPlay()
        {
            if (IsDebugEnabled) Debug($"Playlist autoplay disabled via _ManualPlay.");
            autoplayList = false;
        }

        public void ToggleAutoPlay()
        {
            if (autoplayList) ManualPlay();
            else AutoPlay();
        }

        /// <summary>
        /// Convenience proxy event. Check the overload method in <see cref="SwitchEntry(int)"/>.<br/>
        /// Compatible with UdonGraph/CyanTriggers when used with <see cref="IN_INDEX"/> variable.<br/>
        /// Compatible with UIEvents via Template object usage.
        /// </summary>
        /// <seealso cref="SwitchEntry(int)"/>
        [PublicAPI]
        public void SwitchEntry()
        {
            if (!init) return;
            if (IN_INDEX == -1) IN_INDEX = getDetectedEntry(selectActionRefs);
            if (IN_INDEX == -1) return; // no valid index available
            SwitchEntry(IN_INDEX);
            IN_INDEX = -1;
        }

        public void SwitchToRandomEntry() => SwitchToRandomEntry(true);

        public void SwitchToRandomUnfilteredEntry() => SwitchToRandomEntry(false);

        public void Prioritize()
        {
            Start();
            if (!hasTV) return;
            if (!hasQueue && isLoading) return; // wait until the current video loading finishes/fails
            if (hasRPC && prioritizeOnInteract) rpc.SendCustomNetworkEvent(NetworkEventTarget.All, nameof(PlaylistRPC.ALL_PRIORITIZE));
        }

        #endregion

        // === Public Helper Methods

        public void SwitchToRandomEntry(bool filtered)
        {
            int max = filtered ? filteredViewCount : sortView.Length;
            int index = Random.Range(0, max);
            if (filtered) index = filteredViewIndexToSortViewIndex(index);
            SwitchEntry(index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortViewIndex">the index of the entry to switch to based on the sorted list</param>
        [PublicAPI]
        public void SwitchEntry(int sortViewIndex)
        {
            Start();
            if (!hasTV) return;
            if (!hasQueue && isLoading) return; // wait until the current video loading finishes/fails
            if (sortViewIndex >= sortView.Length) Error($"Playlist Item {sortViewIndex} doesn't exist.");
            else if (sortViewIndex > -1)
            {
                if (hasRPC && prioritizeOnInteract) rpc.SendCustomNetworkEvent(NetworkEventTarget.All, nameof(PlaylistRPC.ALL_PRIORITIZE));
                currentSortViewIndex = sortViewIndex;
                if (IsInfoEnabled) Info($"Switching to playlist item {sortViewIndex}");
                int index = sortView[currentSortViewIndex];
                var title = titles[index];
                if (title.StartsWith("~")) title = title.Substring(1);
                if (hasQueue) queue._AddEntry(mainUrls[index], alternateUrls[index], title);
                else tv._ChangeMedia(mainUrls[index], alternateUrls[index], title);
            }
        }

        public void SeekView(int filteredViewIndex = -1)
        {
            if (!init) return;
            filteredViewIndex = Mathf.Clamp(filteredViewIndex, 0, filteredViewCount - 1);
            if (scrollView.verticalScrollbar != null)
            {
                float value = Mathf.Clamp01(1f - ((float)filteredViewIndex) / filteredViewCount);
                scrollView.verticalScrollbar.SetValueWithoutNotify(value);
            }

            if (updateView(filteredViewIndex, true)) retargetActive();
        }

        public void UpdateFilter(bool[] hide)
        {
            if (hide.Length != mainUrls.Length)
            {
                if (IsDebugEnabled) Info("Filter array must be the same size as the list of urls in the playlist");
                return;
            }

            hidden = hide;
            cacheFilteredView();
            // since the filtered array changed size, recalculate the total entries height
            Rect max = scrollView.viewport.rect;
            Rect item = ((RectTransform)template.transform).rect;
            var horizontalCount = Mathf.FloorToInt(max.width / item.width);
            if (horizontalCount == 0) horizontalCount = 1;
            // limit offset to the url max minus the last "page", account for the "extra" overflow row as well.
            var maxRow = (filteredViewCount - 1) / horizontalCount + 1;
            var contentHeight = maxRow * item.height;

            scrollView.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contentHeight);
            SeekView(0);
        }

        public void UpdateSort()
        {
            cacheFilteredView();
            SeekView(0);
        }

        public void ChangeAutoPlay(bool active)
        {
            if (active) AutoPlay();
            else ManualPlay();
        }

        // === Helper Methods ===

        private static void shuffle(int[] view, int cycles)
        {
            for (int j = 0; j < cycles; j++)
                VRC.SDKBase.Utilities.ShuffleArray(view);
        }

        // prepare the sortView with the default index mapping
        public void ResetSortView()
        {
            for (int i = 0; i < sortView.Length; i++) sortView[i] = i;
        }

        private void cacheFilteredView()
        {
            var count = 0;
            // repopulate the filteredView with visible items
            foreach (var index in sortView)
                if (!hidden[index])
                {
                    filteredView[count] = index;
                    count++;
                }

            // cache the visible items count
            filteredViewCount = count;
            // remove the remainder of the filteredView entries
            for (; count < sortView.Length; count++)
                filteredView[count] = -1;
        }

        private void cacheEntryRefs()
        {
            hasUrlDisplay = urlDisplay != null;
            hasTitleDisplay = titleDisplay != null;
            hasDescriptionDisplay = descriptionDisplay != null;
            hasUrlDisplayTMP = urlDisplayTMP != null;
            hasTitleDisplayTMP = titleDisplayTMP != null;
            hasDescriptionDisplayTMP = descriptionDisplayTMP != null;
            hasSelectAction = selectAction != null;
            hasLoadingBar = loadingBar != null;
            hasImageDisplay = imageDisplay != null;

            int cacheSize = listContainer.childCount;
            entryRefs = new RectTransform[cacheSize];
            urlDisplayRefs = new Text[cacheSize];
            titleDisplayRefs = new Text[cacheSize];
            descriptionDisplayRefs = new Text[cacheSize];
            urlDisplayTMPRefs = new TextMeshProUGUI[cacheSize];
            titleDisplayTMPRefs = new TextMeshProUGUI[cacheSize];
            descriptionDisplayTMPRefs = new TextMeshProUGUI[cacheSize];
            selectActionRefs = new Button[cacheSize];
            loadingBarRefs = new Slider[cacheSize];
            imageDisplayRefs = new Image[cacheSize];

            for (int i = 0; i < cacheSize; i++)
            {
                Transform entry = listContainer.GetChild(i);
                Transform t;
                entryRefs[i] = entry;

                if (hasUrlDisplay)
                {
                    t = entry;
                    if (urlDisplayTmplPath != EMPTYSTR) t = entry.Find(urlDisplayTmplPath);
                    urlDisplayRefs[i] = t.GetComponent<Text>();
                }

                if (hasTitleDisplay)
                {
                    t = entry;
                    if (titleDisplayTmplPath != EMPTYSTR) t = entry.Find(titleDisplayTmplPath);
                    titleDisplayRefs[i] = t.GetComponent<Text>();
                }

                if (hasDescriptionDisplay)
                {
                    t = entry;
                    if (descriptionDisplayTmplPath != EMPTYSTR) t = entry.Find(descriptionDisplayTmplPath);
                    descriptionDisplayRefs[i] = t.GetComponent<Text>();
                }

                if (hasUrlDisplayTMP)
                {
                    t = entry;
                    if (urlDisplayTMPTmplPath != EMPTYSTR) t = entry.Find(urlDisplayTMPTmplPath);
                    urlDisplayTMPRefs[i] = t.GetComponent<TextMeshProUGUI>();
                }

                if (hasTitleDisplayTMP)
                {
                    t = entry;
                    if (titleDisplayTMPTmplPath != EMPTYSTR) t = entry.Find(titleDisplayTMPTmplPath);
                    titleDisplayTMPRefs[i] = t.GetComponent<TextMeshProUGUI>();
                }

                if (hasDescriptionDisplayTMP)
                {
                    t = entry;
                    if (descriptionDisplayTMPTmplPath != EMPTYSTR) t = entry.Find(descriptionDisplayTMPTmplPath);
                    descriptionDisplayTMPRefs[i] = t.GetComponent<TextMeshProUGUI>();
                }

                if (hasSelectAction)
                {
                    t = entry;
                    if (selectActionTmplPath != EMPTYSTR) t = entry.Find(selectActionTmplPath);
                    selectActionRefs[i] = t.GetComponent<Button>();
                }

                if (hasLoadingBar)
                {
                    t = entry;
                    if (loadingBarTmplPath != EMPTYSTR) t = entry.Find(loadingBarTmplPath);
                    loadingBarRefs[i] = t.GetComponent<Slider>();
                }

                if (hasImageDisplay)
                {
                    t = entry;
                    if (imageDisplayTmplPath != EMPTYSTR) t = entry.Find(imageDisplayTmplPath);
                    imageDisplayRefs[i] = t.GetComponent<Image>();
                }
            }
        }

        private bool updateView(int filteredViewIndex, bool forceUpdate)
        {
            // modifies the scope of the view, cache the offset for later use
            var newViewOffset = calculateFilteredViewOffset(filteredViewIndex);
            bool viewChanged = newViewOffset != viewOffset;
            if (viewChanged || forceUpdate)
            {
                viewOffset = newViewOffset;
                updateCurrentView(viewOffset);
            }

            return viewChanged;
        }

        // Takes in the current index and calculates a rounded value based on the horizontal count
        // This ensures that elements don't incidentally shift horizontally and only vertically while scrolling
        private int calculateFilteredViewOffset(int filteredViewIndex)
        {
            Rect max = scrollView.viewport.rect;
            Rect item = ((RectTransform)template.transform).rect;
            var horizontalCount = Mathf.FloorToInt(max.width / item.width);
            if (horizontalCount == 0) horizontalCount = 1;
            var verticalCount = Mathf.FloorToInt(max.height / item.height);
            // limit offset to the url max minus the last "row", account for the "extra" overflow row as well.
            var maxRawRow = filteredViewCount / horizontalCount + 1;
            // clamp the min/max row to the view area boundries
            var maxRow = Mathf.Min(maxRawRow, maxRawRow - verticalCount);
            if (maxRow == 0) maxRow = 1;

            var maxOffset = maxRow * horizontalCount;
            var currentRow = filteredViewIndex / horizontalCount; // int DIV causes stepped values, good
            var currentOffset = currentRow * horizontalCount;
            // currentOffset will be smaller than maxOffset when the scroll limit has not yet been reached
            var targetOffset = Mathf.Min(currentOffset, maxOffset);
            return Mathf.Max(0, targetOffset);
        }

        private void updateCurrentView(int filteredViewIndex)
        {
            currentView = new int[listContainer.childCount];
            if (IsDebugEnabled) Debug($"Updating view to index {filteredViewIndex}");
            string _log = "None";
            for (int i = 0; i < listContainer.childCount; i++)
            {
                if (filteredViewIndex >= filteredViewCount)
                {
                    // urls have exceeded count, hide the remaining entries
                    listContainer.GetChild(i).gameObject.SetActive(false);
                    currentView[i] = -1;
                    continue;
                }

                if (IsTraceEnabled)
                {
                    if (i == 0) _log = $"Visible Indexes:\n{filteredView[filteredViewIndex]}";
                    _log += $", {filteredView[filteredViewIndex]}";
                }

                var entry = listContainer.GetChild(i);
                entry.gameObject.SetActive(true);
                // update entry contents
                var index = filteredView[filteredViewIndex];
                var main = mainUrls[index].Get();
                var alt = alternateUrls[index].Get();
                var rawTitle = titles[index];
                var desc = descriptions[index];

                bool hasOnlyTitle = string.IsNullOrWhiteSpace(main) && string.IsNullOrWhiteSpace(alt);
                var title = rawTitle.StartsWith("~") ? rawTitle.Substring(1) : rawTitle;

                if (hasUrlDisplay && showUrls) urlDisplayRefs[i].text = main;
                if (hasTitleDisplay) titleDisplayRefs[i].text = title;
                if (hasDescriptionDisplay) descriptionDisplayRefs[i].text = desc;
                if (hasUrlDisplayTMP && showUrls) urlDisplayTMPRefs[i].text = main;
                if (hasTitleDisplayTMP) titleDisplayTMPRefs[i].text = title;
                if (hasDescriptionDisplayTMP) descriptionDisplayTMPRefs[i].text = desc;
                // disable the select button action if no urls are available. Effectively treats the entry as a "section header" entry.
                // though if you have shuffle on load enabled, the "section header" concept becomes basically useless. Take note.
                if (hasSelectAction) selectActionRefs[i].interactable = !hasOnlyTitle || rawTitle.StartsWith("~");
                if (hasImageDisplay)
                {
                    var image = imageDisplayRefs[i];
                    var imageEntry = images[index];
                    image.sprite = imageEntry;
                    image.gameObject.SetActive(imageEntry != null);
                }

                currentView[i] = filteredViewIndex;
                filteredViewIndex++;
            }

            if (IsTraceEnabled) Trace(_log);
        }

        private void retargetActive()
        {
            // if autoplay is disabled, try to see if the current media matches one on the playlist, if so, indicate loading
            if (hasLoading) loading.value = 0f;
            int found = findCurrentViewIndex();
            // cache the found index's Slider component, otherwise null
            if (found > -1)
            {
                if (hasLoadingBar)
                {
                    loading = loadingBarRefs[found];
                    hasLoading = loading != null;
                    loading.value = loadingPercent;
                }

                if (IsTraceEnabled) Trace($"Media index {found} found");
            }
            else
            {
                if (IsTraceEnabled) Trace($"Media index not within view");
                loading = null;
                hasLoading = false;
            }
        }

        private int getDetectedEntry(Selectable[] referencesArray)
        {
            Debug("Auto-detecting selected index via interaction");
            for (int i = 0; i < referencesArray.Length; i++)
            {
                var @ref = referencesArray[i];
                if (@ref == null) continue;
                if (!@ref.enabled)
                {
                    int index = Array.IndexOf(sortView, currentViewIndexToRawIndex(i));
                    if (IsDebugEnabled) Debug($"Detected view index {index}.");
                    return index;
                }
            }

            Debug("Index not able to be auto-detected");
            return -1;
        }

        private int findCurrentViewIndex()
        {
            if (!hasTV) return -1;
            var url = tv.urlMain.Get();
            // if the current index is playing on the TV and not hidden, 
            //  return either it's position in the current view, or -1 if it's not visible in the current view
            if (currentSortViewIndex > -1)
            {
                var rawIndex = sortView[currentSortViewIndex];
                if (mainUrls[rawIndex].Get() == url)
                    if (!hidden[rawIndex])
                        return Array.IndexOf(currentView, Array.IndexOf(filteredView, rawIndex));
            }

            // then if the current index IS hidden or IS NOT playing on the TV, 
            // attempt a fuzzy search to find another index that matches that URL
            // do not need to check for hidden here as current view already has that taken into account
            for (int i = 0; i < currentView.Length; i++)
            {
                var listIndex = currentViewIndexToRawIndex(i);
                if (listIndex > -1 && mainUrls[listIndex].Get() == url) return i;
            }

            return -1;
        }

        private int findSortViewIndex()
        {
            var len = sortView.Length;
            var url = tv.urlMain.Get();
            for (int i = 0; i < len; i++)
            {
                var rawIndex = sortView[i];
                if (mainUrls[rawIndex].Get() == url)
                    return i;
            }

            return -1;
        }

        // take a given index (typically derived from the button a player clicks on in the UI)
        // and reverse the values through the arrays to get the original list index.
        private int currentViewIndexToRawIndex(int currentViewIndex)
        {
            if (currentViewIndex == -1) return -1;
            if (currentViewIndex >= currentView.Length) return -1;
            int filteredViewIndex = currentView[currentViewIndex];
            if (filteredViewIndex == -1) return -1;
            if (filteredViewIndex >= filteredViewCount) return -1;
            return filteredView[filteredViewIndex];
        }

        private int filteredViewIndexToSortViewIndex(int filteredViewIndex)
        {
            if (filteredViewIndex == -1) return -1;
            return Array.IndexOf(sortView, filteredView[filteredViewIndex]);
        }

        private int sortViewIndexToFilteredViewIndex(int sortViewIndex)
        {
            if (sortViewIndex == -1) return -1;
            return Array.IndexOf(filteredView, sortView[sortViewIndex]);
        }

        private int currentViewIndexToNextSortViewIndex(int currentViewIndex)
        {
            if (currentViewIndex == -1) return -1;
            if (currentViewIndex >= currentView.Length) return -1;
            int filteredViewIndex = currentView[currentViewIndex];
            if (filteredViewIndex == -1) return -1;
            int sortViewIndex = Array.IndexOf(sortView, filteredView[filteredViewIndex], nextSortViewIndex);
            if (sortViewIndex == -1) sortViewIndex = Array.IndexOf(sortView, filteredView[filteredViewIndex]);
            return sortViewIndex;
        }

        private int findActualSortViewIndex()
        {
            var url = tv.urlMain;
            // check for exact instances (only really applies to the person who selected the entry)
            int rawIndex = Array.IndexOf(mainUrls, url);
            if (rawIndex == -1)
            {
                // other wise scan the playlist for the first matching URL string
                var len = mainUrls.Length;
                var str = url.Get();
                // Note: This loop can become performance heavy in Udon1 with extremely large playlists.
                for (int i = 0; i < len; i++)
                {
                    if (mainUrls[i].Get() == str)
                    {
                        rawIndex = i;
                        break;
                    }
                }

                if (rawIndex == -1) return -1;
            }

            return Array.IndexOf(sortView, rawIndex);
        }

        private int wrap(int value)
        {
            if (value < 0) value = sortView.Length + value; // adds a negative
            else if (value >= sortView.Length) value -= sortView.Length; // subtracts the full length
            return value;
        }
    }
}