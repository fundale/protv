using System;
using ArchiTech.SDK;
using UdonSharp;
using UnityEngine;
using UnityEngine.Serialization;
using VRC.SDKBase;

namespace ArchiTech.ProTV
{
    [AddComponentMenu("")]
    [Obsolete("Use TVManagedWhitelist instead")]
    public class TVUsernameWhitelist : TVManagedWhitelist { }
}