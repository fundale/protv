﻿using System;
using JetBrains.Annotations;
using UdonSharp;
using UnityEngine;
using VRC.SDK3.Components.Video;
using VRC.SDK3.Video.Components.Base;
using VRC.SDKBase;
using VRC.Udon.Common.Interfaces;
using ArchiTech.SDK;
using VRC.Udon.Common.Enums;

// ReSharper disable ConvertIfStatementToConditionalTernaryExpression

namespace ArchiTech.ProTV
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
    [DefaultExecutionOrder(-9999)] // needs to initialize before anything else if possible
    public partial class TVManager : ATEventHandler
    {
        public override void Start()
        {
            if (init) return;
            SetLogPrefixColor("#00ff00");
            base.Start();
            if (syncData == null) syncData = GetComponentInChildren<TVManagerData>(true);
            syncData.Logger = Logger;
            Log(ATLogLevel.ALWAYS, $"Starting TV (v{versionNumber})");
            if (repeatingRetryDelay < 5f) repeatingRetryDelay = 5f;
            if (videoManagers == null || videoManagers.Length == 0)
                videoManagers = GetComponentsInChildren<VPManager>(true);
            if (videoManagers.Length == 0)
            {
                Error("No video managers available. Make sure any desired video managers are a child of the TV, otherwise the TV will not work.");
                return;
            }

            // Quest always default to the alternate URL
#pragma warning disable CS0162
            // ReSharper disable once HeuristicUnreachableCode
            if (isAndroid) useAlternateUrl = preferAlternateUrlForQuest;
#pragma warning restore CS0162
            // load initial video player
            videoPlayer = defaultVideoManager;
            // make the object active at start, then when the TV is ready, hide all but the active one.
            foreach (VPManager m in videoManagers) m.gameObject.SetActive(true);
            prevManager = activeManager = nextManager = videoManagers[videoPlayer];
            volume = defaultVolume;
            nextManager.ChangeVolume(volume);
            audio3d = !startWith2DAudio;
            nextManager.ChangeAudioMode(audio3d);
            if (startWithVideoDisabled) disableVideo = true;

            RequestSync();
            initSecurity();
            // make the script wait a bit before trying to fetch the video data for the first time.
            // for each uniqe video player in the world, modify the wait time by some value if an autoplay is assigned.
            // This is to mitigate the ratelimiting that occurs if multiple TVs try to autoplay at the same time.
            // the autoplayStartOffset is calculated at build time automatically.
            triggerRefresh(1f + autoplayStartOffset);
            // implicitly have minimum 1 second as the buffer delay to guarentee that 
            // the syncTime continuous sync will be able to transmit prior to playing the media
            // This prevents non-owners from trying to sync too early to the wrong part of the media
            if (bufferDelayAfterLoad < 1f) bufferDelayAfterLoad = 1f;
            if (automaticResyncInterval == 0f) automaticResyncInterval = Mathf.Infinity;
            autoSyncWait = Time.timeSinceLevelLoad + automaticResyncInterval;
            syncEnforceWait = waitUntil + syncEnforcementTimeLimit;
            aspectRatio = targetAspectRatio;
            if (startHidden) manuallyHidden = true;
            if (startDisabled) gameObject.SetActive(false);
            // trigger the delayed ready up
            // if deserialization does not happen in time (ie: user is master or the owner has the object disabled)
            // force the local player to run the ready up logic anyways
            // give it plenty of breathing room before forcing internal ready up so the world can load in
            // master forces quickly, non-master waits extra time
            SendCustomEventDelayedSeconds(nameof(_InternalReadyUp), 1f + autoplayStartOffset);
        }

        private void OnEnable()
        {
            Start();
            // trigger the update loops
            SendCustomEventDelayedFrames(nameof(_InternalTimeSync), 1);
            SendCustomEventDelayedFrames(nameof(_InternalUpdate), 1);
            SendCustomEventDelayedFrames(nameof(_InternalLateUpdate), 1, EventTiming.LateUpdate);
            // if the TV was disabled before Update check ran, the TV was set off by default from some external method, like a toggle or likewise.
            // If that's the case, the startDisabled flag is redundant so simply unset the flag.
            if (startDisabled) startDisabled = false;
            if (!VRC.SDKBase.Utilities.IsValid(activeManager) || !VRC.SDKBase.Utilities.IsValid(activeManager.videoPlayer)) return;

            if (IsOwner)
            {
                if (reloadCache > 0f)
                {
                    var diff = Time.timeSinceLevelLoad - reloadStart;
                    jumpToTime = reloadCache + diff;
                    if (jumpToTime > endTime) jumpToTime = endTime;
                    if (!stopMediaWhenDisabled) activeManager.videoPlayer.SetTime(jumpToTime);
                }

                SendCustomNetworkEvent(NetworkEventTarget.All, nameof(ALL_OwnerEnabled));
            }
            else
            {
                if (!ownerDisabled && isReady)
                {
                    if (IsDebugEnabled) Debug("Requesting data from owner.");
                    SendCustomNetworkEvent(NetworkEventTarget.Owner, nameof(OWNER_RequestSyncData));
                }
            }

            disabled = false;
            if (videoTextureWasEnabled) _EnableVideoTexture();
            _Play();
        }

        private void OnDisable()
        {
            if (!VRC.SDKBase.Utilities.IsValid(activeManager) || !VRC.SDKBase.Utilities.IsValid(activeManager.videoPlayer)) return;

            // In order to prevent a loop glitch due to owner not updating syncTime when the object is disabled
            // send a command as owner to everyone to signal that the owner is disabled.
            if (IsOwner)
            {
                if (!isLive)
                {
                    reloadCache = currentTime;
                    reloadStart = Time.timeSinceLevelLoad;
                }

                SendCustomNetworkEvent(NetworkEventTarget.All, nameof(ALL_OwnerDisabled));
            }

            disabled = true;
            videoTextureWasEnabled = !disableVideo;
            _DisableVideoTexture();
            if (stopMediaWhenDisabled) stop(true);
            else pause();
        }

        public override void OnPlayerLeft(VRCPlayerApi p)
        {
            if (!VRC.SDKBase.Utilities.IsValid(p)) hasLocalPlayer = false;
        }

        public void _InternalReadyUp()
        {
            if (isReady) return;
            if (forceReadyUp || IsOwner)
            {
                if (IsTraceEnabled) Trace("Deserialization not received in time, forcing ready up");
                internalReadyUp();
            }
            else
            {
                // if not the owner, try to force the owner to resync the current data
                if (IsTraceEnabled) Trace("Deserialization not received in time, requesting sync data.");
                SendCustomNetworkEvent(NetworkEventTarget.Owner, nameof(OWNER_RequestSyncData));
                // trigger another delayed force attempt in case deserialization STILL doesn't happen
                // this is usually because the owner has the tv object disabled.
                forceReadyUp = true;
                SendCustomEventDelayedSeconds(nameof(_InternalReadyUp), 5f);
            }
        }

        /// <summary>
        /// Handles preparing the internal data after the initial wait period is complete.
        /// Triggers refresh, handles autoplay and notifies plugins the TV is ready to be used.
        /// Gets called automatically for the instance master.
        /// Gets called via OnDeserialization for everyone else.
        /// </summary>
        /// <seealso cref="_PostDeserialization"/>
        private void internalReadyUp()
        {
            if (isReady) return;
            isReady = true;
            // with the TV now ready, disable all but the default video manager
            foreach (VPManager m in videoManagers)
                if (m != nextManager)
                    m.gameObject.SetActive(false);

            // authorization checks depend on the auth plugin,
            // so let the auth plugin run its setup before checking for any authorization checks
            if (IsTraceEnabled) Trace("Auth check for ReadyUp");
            if (hasAuthPlugin) authPlugin._TvReady();

            if (CanPlayMedia && AutoOwnershipAvailable)
            {
                if (IsTraceEnabled) Trace($"Taking ownership via _InternalReadyUp (state {stateOwner} time {syncTime})");
                takeOwnership();
                SendCustomNetworkEvent(NetworkEventTarget.All, nameof(ALL_OwnerEnabled));
                if ((int)stateOwner > (int)TVPlayState.STOPPED)
                {
                    // if there is media already active when ownership is taken,
                    // setup the data for a continuity sync reload
                    if (IsTraceEnabled) Trace($"Setting jump time to EPSILON. old jumptime {jumpToTime}");
                    jumpToTime = EPSILON;
                    reloadCache = syncTime;
                    reloadStart = Time.timeSinceLevelLoad;
                    triggerRefresh(0f);
                }
            }

            if (urlRevision == 0 && (!syncToOwner || IsOwner))
            {
                if (autoplayMainUrl == null) autoplayMainUrl = EMPTYURL;
                if (autoplayAlternateUrl == null) autoplayAlternateUrl = EMPTYURL;
                IN_MAINURL = autoplayMainUrl;
                IN_ALTURL = autoplayAlternateUrl;
                IN_TITLE = autoplayTitle;
            }

            _ChangeInteractions(interactionState);
            Info("TV is now ready");
            SendManagedEvent(nameof(TVPlugin._TvReady));
        }

        public void _InternalLateUpdate()
        {
            if (!gameObject.activeInHierarchy) return;
            // trigger the next update loop
            SendCustomEventDelayedFrames(nameof(_InternalLateUpdate), updateIntervalFrames, EventTiming.LateUpdate);
            // clear the auth cache for the current frame
            localAuthCacheUser = -1;
            authCacheUser = -1;
            superAuthCacheUser = -1;
            // tv has not been fully init'd yet, skip current cycle.
            if (!isReady) return;
            VPManager manager = ActiveManager;
            // when video player is switching (as denoted by the epsilon jump time), use the prevManager reference.
            if (state == TVPlayState.PLAYING)
            {
                updateShaderData();
                manager.Blit();
            }
            // when TV is not playing, run the blit once then wait until it plays again.
            // this enables updating the blit data only as needed
            // this will also run a single time once the tv is ready.
            else if (!nonPlayBlit)
            {
                nonPlayBlit = true;
                updateShaderData();
                if (IsTraceEnabled) Trace($"Latest VideoData:\n{shaderVideoData.ToString()}");
                manager.Blit();
            }
        }

        public void _InternalUpdate()
        {
            if (!gameObject.activeInHierarchy) return;
            // trigger the next update loop
            SendCustomEventDelayedFrames(nameof(_InternalUpdate), updateIntervalFrames);
            // has not yet been initialized or is playmode without cyanemu
            if (!init) return;
            if (!hasLocalPlayer) return;
            if (buffering) return; // shortcut the loop while media is being prepared.

            var time = Time.timeSinceLevelLoad;
            // wait until the timeout has cleard
            if (time < waitUntil) return;
            if (!isReady) return; // wait until either deserialization triggers ready state or the force ready timeout expires
            if (waiting)
            {
                Info("Refresh media via delay");
                waiting = false;
                // For some reason when rate limiting happens, the auto reload causes loading to be enabled unexpectely
                // This might cause unexpected edgecases at some point. Keep a close eye on media change issues related to loading states.
                loading = false;
                _RefreshMedia();
                return;
            }

            VPManager manager = ActiveManager;
            if (!VRC.SDKBase.Utilities.IsValid(manager) || manager == null) return; // manager has been destroyed/unavailable, exiting world or application
            BaseVRCVideoPlayer vp = manager.videoPlayer;
            if (!VRC.SDKBase.Utilities.IsValid(vp) || vp == null) return; // video player has been destroyed, exiting world or application

            if (mediaEnded) { } // media has ended, nothing to skip and time doesn't need to be updated
            else if (syncTime == INF)
            {
                // skip the current media
                endMedia(vp);
                return;
            }
            // wait until manual loop has cleared before checking for the currentTime
            else if (!manualLoop)
            {
                // update time and cache the mediaEnd check flag
                currentTime = Mathf.Clamp(vp.GetTime() - seekOffset, startTime, endTime);
                // do not enable media end if the local player has not loaded a video previously or if the video is live media
                if (state != TVPlayState.WAITING && !isLive) mediaEnded = currentTime + 0.1f >= endTime;
            }

            if (errorState == TVErrorState.FAILED) return; // blocking error has occurred 

            if (vp.IsPlaying && time > autoSyncWait)
            {
                // every so often trigger an automatic resync
                enforceSyncTime = true;
                autoSyncWait = time + automaticResyncInterval;
            }

            if (enableReloadKeybind && !LoadingMedia)
            {
                if (Input.GetKeyDown(KeyCode.F5) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)) 
                    triggerRefresh(autoplayStartOffset);
            }

            mediaSyncCheck(time, vp);
            // live media does not do time checks for ending.
            if (isLive)
            {
                liveMediaReloadCheck(time);
                return;
            }

            mediaEndCheck(vp);
        }

        private void mediaSyncCheck(float time, BaseVRCVideoPlayer vp)
        {
            if (!syncToOwner || isLive)
            {
                // if TV is local or if livestream detected, do sync check logic and skip the rest
                // skip the enforcement when currentTime is infinity (a rare circumstance)
                if (enforceSyncTime && time >= syncEnforceWait && currentTime != INF)
                {
                    // single time update for local-only mode.
                    // Also helps fix audio/video desync/drift in most cases.
                    enforceSyncTime = false;
                    if (IsInfoEnabled) Info($"Sync enforcement requested for live or local. Updating to {currentTime}");
                    if (isLive) currentTime += 10f;
                    else currentTime += 0.05f; // anything smaller than this won't be registered by AVPro for some reason...
                    vp.SetTime(Mathf.Max(0, currentTime + seekOffset));
                }

                return;
            }

            // This handles updating the sync time from owner to others.
            if (IsOwner)
            {
                // check for loading timeout and trigger PlayerError if exceeded
                // Skip check if a delayed refresh is active
                if (loading && !waiting && maxAllowedLoadingTime > 0 && time >= loadingWait)
                {
                    Warn($"Loading timeout of {maxAllowedLoadingTime}s reached.");
                    _OnVideoPlayerError(VideoError.PlayerError);
                }

                if (enforceSyncTime && time >= syncEnforceWait)
                {
                    // single time update for owner. 
                    // Also helps fix audio/video desync/drift in most cases.
                    enforceSyncTime = false;
                    if (IsInfoEnabled) Info($"Sync enforcement requested for owner. Updating to {currentTime}");
                    currentTime += 0.05f; // anything smaller than this won't be registered by AVPro for some reason...
                    vp.SetTime(Mathf.Clamp(currentTime + seekOffset, startTime, endTime));
                }

                syncTime = currentTime;
            }
            else if (loading) // skip other time checks if TV is loading a video
            {
                // check for loading timeout and trigger PlayerError if exceeded
                // Skip check if a delayed refresh is active
                if (!waiting && maxAllowedLoadingTime > 0 && time >= loadingWait)
                {
                    Warn($"Loading timeout of {maxAllowedLoadingTime}s reached.");
                    _OnVideoPlayerError(VideoError.PlayerError);
                }
            }
            else if ((int)state > (int)TVPlayState.STOPPED && (int)errorStateOwner <= (int)TVErrorState.BLOCKED)
            {
                var compSyncTime = syncTime + lagComp;
                if (compSyncTime > endTime) compSyncTime = endTime;
                float syncDelta = Mathf.Abs(currentTime - compSyncTime);
                if (enforceSyncTime || state == TVPlayState.PLAYING && syncDelta > playDriftThreshold && !ownerDisabled)
                {
                    // sync time enforcement check should ONLY be for when the video is playing
                    // Also helps fix audio/video desync/drift in most cases.
                    // delay enforcing sync until the owner's sync time is greater than the start time
                    // syncTime being less than startTime is an artifact of combining auto-ownership with a sliced video
                    if (time >= syncEnforceWait && syncTime >= startTime)
                    {
                        if (IsTraceEnabled) Trace($"Sync info:\nsyncTime {syncTime} + lag {lagComp} = comp {compSyncTime}\nseekOffset {seekOffset} currentTime {currentTime}");
                        currentTime = compSyncTime;
                        if (IsInfoEnabled) Info($"Sync enforcement. Updating to {currentTime}");
                        enforceSyncTime = false;
                        vp.SetTime(Mathf.Clamp(currentTime + seekOffset, startTime, endTime));
                        SendManagedVariable(nameof(TVPlugin.OUT_SEEK), currentTime);
                        SendManagedEvent(nameof(TVPlugin._TvSeekChange));
                    }
                }
                // video sync enforcement will always occur for paused mode as the user expects the video to not be active, so we can skip forward as needed.
                else if (syncDelta > pauseDriftThreshold)
                {
                    currentTime = compSyncTime;
                    if (IsDebugEnabled) Debug($"Paused drift threshold exceeded. Updating to {currentTime}");
                    vp.SetTime(Mathf.Clamp(currentTime + seekOffset, startTime, endTime));
                    SendManagedVariable(nameof(TVPlugin.OUT_SEEK), currentTime);
                    SendManagedEvent(nameof(TVPlugin._TvSeekChange));
                }
            }
        }

        private void mediaEndCheck(BaseVRCVideoPlayer vp)
        {
            // loop/media end check
            if (mediaEnded)
            {
                bool shouldLoop = loop > 0;
                if (IsOwner && shouldLoop)
                {
                    // owner when loop is active
                    vp.SetTime(startTime);
                    mediaEnded = false;
                    syncTime = currentTime = startTime;
                    SendManagedEvent(nameof(TVPlugin._TvMediaLoop));
                    if (loop < int.MaxValue)
                    {
                        loop--;
                        if (loop == 0) SendManagedEvent(nameof(TVPlugin._TvDisableLoop));
                    }
                }
                else if (state == TVPlayState.PLAYING && endTime > 0f)
                {
                    if (shouldLoop)
                    {
                        if (syncToOwner && syncTime >= currentTime)
                        {
                            if (IsDebugEnabled) Debug("Sync is enabled but sync time hasn't been passed, skip");
                        }
                        // sync is enabled but sync time hasn't been passed, skip
                        else
                        {
                            // non-owner when owner has loop (causing the sync time to start over)
                            vp.SetTime(startTime);
                            mediaEnded = false;
                            // update current time to start time so this only executes once, prevents accidental spam
                            currentTime = startTime;
                            SendManagedEvent(nameof(TVPlugin._TvMediaLoop));
                            if (loop < int.MaxValue)
                            {
                                loop--;
                                if (loop == 0) SendManagedEvent(nameof(TVPlugin._TvDisableLoop));
                            }
                        }
                    }
                    else if (!manualLoop)
                    {
                        endMedia(vp);
                    }
                }
            }
            else if (manualLoop) manualLoop = false;
        }

        private void liveMediaReloadCheck(float time)
        {
            if (liveMediaAutoReloadInterval > 0 && time > liveReloadTimestamp)
            {
                liveReloadTimestamp = time + liveMediaAutoReloadInterval * 60f;
                triggerRefresh(0f);
            }
        }

        private void endMedia(BaseVRCVideoPlayer vp)
        {
            if (IsDebugEnabled) Debug("Ending media playback.");
            var cTime = currentTime;
            // force times to tne end for media end detection and to prevent accidental skips from INF check
            currentTime = syncTime = endTime;
            // once media has finished, any reload info should be zeroed
            jumpToTime = 0f;
            reloadCache = 0f;
            SendManagedEvent(nameof(TVPlugin._TvMediaEnd));
            // if no plugins triggered a new URL or refresh, force end the actual media
            if (mediaEnded || !waiting)
            {
                if (IsTraceEnabled) Trace($"Media full ending: actual {mediaEnded} or not waiting {!waiting}");
                vp.Pause();
                vp.SetTime(endTime);
                state = TVPlayState.PAUSED;
                if (IsOwner)
                {
                    syncState = stateOwner = state;
                    RequestSync();
                }

                mediaEnded = true;
                nonPlayBlit = false; // force blit once more just to be sure
            }
            // if the ending is not a real ending, restore the original timestamp
            else currentTime = syncTime = cTime;
        }

        public void _InternalTimeSync()
        {
            if (!gameObject.activeInHierarchy) return;
            SendCustomEventDelayedSeconds(nameof(_InternalTimeSync), 0.2f);
            if (IsOwner && state == TVPlayState.PLAYING) RequestSerialization();
        }

        public override void OnPreSerialization()
        {
            lagCompSync = Networking.GetServerTimeInMilliseconds();
            syncTime = currentTime;
        }

        public override void OnDeserialization()
        {
            lagComp = (Networking.GetServerTimeInMilliseconds() - lagCompSync) * 0.001f;
        }

        private void RequestSync()
        {
            if (!init) return;
            if (IsOwner)
            {
                RequestSerialization();
                syncData._RequestData();
            }
        }

        /// <summary>
        /// Any changes to the owner's synced data will be monitored by this method and update the local player data as needed.
        /// </summary>
        public void _PostDeserialization()
        {
            if (!syncToOwner) return;
            ownerDisabled = false; // ensure that owner is marked enabled since the deserialization doesn't happen when owner is disabled.
            if (!isReady && firstDeserialization)
            {
                // if this is the first time deserialization happens,
                // delay to allow any other first-time deserialization scripts to finish
                // before triggering the ready state
                firstDeserialization = false;
                SendCustomEventDelayedFrames(nameof(_PostDeserialization), 2);
                return;
            }

            // grab the delta states
            bool _deltaVP = syncVideoManagerSelection && videoPlayer != syncVideoPlayer;
            bool _deltaVolume = syncVolumeControl && volume != syncVolume;
            bool _deltaAudioMode = syncAudioMode && audio3d != syncAudio3d;
            bool _deltaPlaybackSpeed = playbackSpeed != syncPlaybackSpeed;
            bool _deltaLocked = locked != syncLocked;
            bool _deltaErrorState = errorStateOwner != syncErrorState;
            bool _deltaTitle = title != syncTitle;
            bool _deltaAddedBy = addedBy != syncAddedBy;
            bool _deltaUrlRevision = urlRevision != syncUrlRevision;
            bool _deltaState = stateOwner != syncState;
            bool _deltaLoop = loop != syncLoop;

            var oldVolume = volume;
            var oldVideoPlayer = videoPlayer;
            var oldAudio3d = audio3d;
            var oldLoop = loop;

            // update the valid deltas + log
            string _changes = "Deserialization Changes";
            if (_deltaVP)
            {
                if (IsDebugEnabled) _changes += $"\nVideo Player swap {videoPlayer} -> {syncVideoPlayer}";
                videoPlayer = syncVideoPlayer;
            }

            if (_deltaVolume)
            {
                if (IsDebugEnabled) _changes += $"\nVolume update {volume} -> {syncVolume}";
                volume = syncVolume;
            }

            if (_deltaAudioMode)
            {
                if (IsDebugEnabled) _changes += $"\nAudioMode3D update {audio3d} -> {syncAudio3d}";
                audio3d = syncAudio3d;
            }

            if (_deltaLoop)
            {
                if (IsDebugEnabled) _changes += $"\nLoop update {loop} -> {syncLoop}";
                loop = syncLoop;
            }

            if (_deltaPlaybackSpeed)
            {
                if (IsDebugEnabled) _changes += $"\nPlayback Speed update {playbackSpeed} -> {syncPlaybackSpeed}";
                playbackSpeed = syncPlaybackSpeed;
            }

            if (_deltaLocked)
            {
                if (IsDebugEnabled) _changes += $"\nLock change {locked} -> {syncLocked}";
                lockedBySuper = syncLocked && _IsSuperAuthorized(Owner);
                locked = syncLocked || disallowUnauthorizedUsers && !_IsAuthorized();
            }

            if (_deltaErrorState)
            {
                if (IsDebugEnabled) _changes += $"\nOwner error state change {errorStateOwner} -> {syncErrorState}";
                errorStateOwner = syncErrorState;
                _deltaTitle = false;
                _deltaAddedBy = false;
                _deltaUrlRevision = false;
                _deltaState = false;
            }
            else // if owner has an error skip syncing the following data
            {
                if (_deltaTitle)
                {
                    if (IsDebugEnabled) _changes += $"\nTitle change \"{title}\" -> \"{syncTitle}\"";
                    title = syncTitle;
                }

                if (_deltaAddedBy)
                {
                    if (IsDebugEnabled) _changes += $"\nAdded by change {addedBy} -> {syncAddedBy}";
                    addedBy = syncAddedBy;
                }

                if (_deltaUrlRevision)
                {
                    if (IsDebugEnabled) _changes += $"\nURL change {urlRevision} -> {syncUrlRevision}";
                    urlRevision = syncUrlRevision;
                }

                if (_deltaState)
                {
                    if (IsDebugEnabled) _changes += $"\nState change {stateOwner} -> {syncState}";
                    stateOwner = syncState;
                }
            }

            if (IsDebugEnabled) Debug(_changes);

            // prepare the initialization if needed
            if (!isReady)
            {
                if (IsTraceEnabled) Trace("Ready up via Deserialization");
                internalReadyUp(); // deserialization happened before the force ready up timout ended. This is good.
            }

            // Run actions based on detected deltas
            if (_deltaVP)
            {
                // restore old value to pass the conditional in the change method.
                videoPlayer = oldVideoPlayer;
                changeVideoPlayer(syncVideoPlayer);
            }

            if (_deltaVolume)
            {
                // restore old value to pass the conditional in the change method.
                volume = oldVolume;
                changeVolume(syncVolume);
            }

            if (_deltaAudioMode)
            {
                // restore old value to pass the conditional in the change method.
                audio3d = oldAudio3d;
                changeAudioMode(syncAudio3d);
            }

            if (_deltaLoop)
            {
                // restore old value to pass the conditional in the change method.
                loop = oldLoop;
                changeLoop(syncLoop);
            }

            if (_deltaPlaybackSpeed) changePlaybackSpeed(playbackSpeed);

            if (_deltaLocked) SendManagedEvent(locked ? nameof(TVPlugin._TvLock) : nameof(TVPlugin._TvUnLock));

            if (_deltaErrorState)
            {
                if ((int)errorStateOwner >= (int)TVErrorState.BLOCKED)
                    Warn("Current TV Owner has an error. Media will not sync until the owner no longer has an error.");
            }

            if (_deltaTitle)
            {
                SendManagedVariable(nameof(TVPlugin.OUT_TITLE), title);
                SendManagedEvent(nameof(TVPlugin._TvTitleChange));
            }

            // ReSharper disable once RedundantCheckBeforeAssignment
            if (_deltaAddedBy) { }

            if (_deltaUrlRevision)
            {
                // if the owner is stopped, do NOT force a refresh.
                if ((int)syncState > (int)TVPlayState.STOPPED)
                {
                    triggerRefresh(0f);
                    triggerSync(0.2f);
                    return;
                }
            }

            // late join first deserialization, wait until initial wait time is complete
            // a lot has changed since this was added. Unsure if it's needed anymore
            // TODO: review necessity of this waiting conditional
            if (waiting)
            {
                if (IsTraceEnabled) Trace("Refresh wait detected during deserialization.");
                return;
            }

            if (_deltaState)
            {
                // if loading, skip state change actions as those actions will be triggered once loading is done.
                if (loading)
                {
                    if (IsTraceEnabled) Trace("Media is currently loading. Deferring state change action.");
                    return;
                }

                switch (stateOwner)
                {
                    // always enforce stopping
                    case TVPlayState.STOPPED:
                        // when local mode is waiting, the initial video load is required.
                        if (state == TVPlayState.WAITING) play();
                        else stop(syncLoading);
                        break;
                    // allow the local player to be paused if owner is playing
                    case TVPlayState.PLAYING:
                        if (!locallyPaused) play();
                        break;
                    // pause for the local player
                    case TVPlayState.PAUSED:
                        // the owner should not be able to trigger the locallyPaused
                        // flag, so use the internal pause method instead of the public
                        // _Pause event.
                        pause();
                        break;
                }
            }

            // Give the syncTime enough time to catch up before running the time sync logic
            // This mitigates certain issues when switching videos
            if (!manualLoop && stateOwner != TVPlayState.WAITING) triggerSync(0.3f);
        }


        // === VPManager events ===

        public void _OnVideoPlayerEnd()
        {
            // only set end state if not loading and not live media.
            // Some live media (like RTSPT) will trigger this event before the media even starts, so don't flag end if it's live media.
            if (!loading && !isLive)
            {
                if (IsTraceEnabled) Trace("Media ended via built-in OnVideoEnd.");
                mediaEnded = true;
            }
        }

        public void _OnVideoPlayerError(VideoError error)
        {
            errorState = TVErrorState.FAILED;
            Error($"Video Error: {error}");
            if (error == VideoError.RateLimited)
            {
                Warn("Refresh via rate limit error, retrying in 5 seconds...");
                errorState = TVErrorState.RETRY;
                triggerRefresh(6f); // 5+1 seconds just to avoid any race condition issues with the global rate limit
            }
            else if (error == VideoError.PlayerError || error == VideoError.InvalidURL)
            {
                if (error == VideoError.InvalidURL)
                {
                    if (isLive) Warn("Stream is either offline or the URL is incorrect.");
                    else Warn("Unable to load. Media maybe unavailable, protected, region-locked or the URL is wrong.");
                }
                else //if (error == VideoError.PlayerError)
                {
                    if (isLive) Warn("Livestream has stopped.");
                    else Warn("Unexpected error with the media playback.");
                }

                if (retryCount > 0)
                {
                    // the first retry should be very short.
                    float retryDelay = 6f;
                    // any subsequent retries (meaning 2 or more times the url failed to load) use the repeating delay value
                    if (errorState == TVErrorState.RETRY) retryDelay = repeatingRetryDelay;
                    else errorState = TVErrorState.RETRY;
                    // do not decrement retry count if count is "effectively infinite"
                    if (retryCount < int.MaxValue)
                    {
                        retryCount--;
                        if (IsInfoEnabled) Info($"{retryCount} retries remaining.");
                    }

                    state = TVPlayState.PAUSED;
                    // if video is ended, but subsequent video is failing, force another blit op just to make sure things are rendered correctly
                    nonPlayBlit = false;
                    // if flag is enabled, flip flop the useAlternateUrl flag once.
                    // if that again fails, flip flop once more and then don't flip any more until success or a new URL is input
                    if (retryUsingAlternateUrl)
                        if (retryingWithAlt && retriedWithAlt) { }
                        else
                        {
                            useAlternateUrl = !useAlternateUrl;
                            if (retryingWithAlt) retriedWithAlt = true;
                            else retryingWithAlt = true;
                        }

                    triggerRefresh(retryDelay);
                }

                setLoadingState(false);
            }
            else setLoadingState(false);

            // if error does not trigger a reload of some kind, restore the url data of the previous url
            RequestSync();
            if (!waiting)
            {
                parseUrl(url.Get(), out urlProtocol, out urlDomain, out urlParamKeys, out urlParamValues);
                mediaEnded = true;
            }

            SendManagedVariable(nameof(TVPlugin.OUT_ERROR), error);
            SendManagedEvent(nameof(TVPlugin._TvVideoPlayerError));
            if (!waiting) mediaEnded = false;
        }

        public void _OnVideoPlayerPlay()
        {
            triggerSync(0.3f);
        }


        // Once the active manager detects the player has finished loading, get video information and log
        public void _OnVideoPlayerReady()
        {
            if (!loading)
            {
                // if this is called again and loading is done, just rerun the media start actions to continue.
                // generally caused by some UnityVideo bullshit when swapping back and forth between UnityVideo and another video player option.
                jumpToTime = lastJumpToTime;
                startMedia();
                return;
            }

            if (!buffering)
            {
                // general media info
                var urlStr = url.Get();
                // video has successfully loaded, make sure the active manager is updated to the target manager
                activeManager = nextManager;
                mediaLength = activeManager.videoPlayer.GetDuration();
                isLive = mediaLength == INF || mediaLength == 0f;
                mediaHash = urlStr.GetHashCode();
                mediaEnded = false;
            }

            if (isLive)
            {
                // livestreams should just start immediately
                liveReloadTimestamp = Time.timeSinceLevelLoad + liveMediaAutoReloadInterval * 60f;
                prepareMedia();
                startMedia();
            }
            // non-owner buffering will continue to wait for owner loading to finish or owner to be disabled, or owner to have a failed load
            // Owner always ends the buffer after the configured delay
            else if (buffering && (IsOwner || !syncLoading || ownerDisabled || errorStateOwner == TVErrorState.FAILED))
            {
                if (IsInfoEnabled) Info("Buffering complete.");
                buffering = false;
                startMedia();
            }
            else if (bufferDelayAfterLoad > 0)
            {
                // timeout is exceeded while the buffer flag is unset. Buffering has started, call delayed event
                if (!buffering)
                {
                    if (IsInfoEnabled) Info($"Allowing video to buffer for {bufferDelayAfterLoad} seconds.");
                    prepareMedia();
                }

                SendCustomEventDelayedSeconds(nameof(_OnVideoPlayerReady), buffering ? 1f : bufferDelayAfterLoad);
                buffering = true;
            }
            else
            {
                // no buffering, start immediately
                prepareMedia();
                startMedia();
            }
        }

        private void prepareMedia()
        {
            cacheMediaReadyInfo();
            if (activeManager.isVisible) { }
            else if (manuallyHidden) { }
            else activeManager.Show();

            bool vpSwap = prevManager != activeManager;
            if (vpSwap)
            {
                if (prevManager != null)
                {
                    if (IsDebugEnabled) Debug($"Hiding previous manager {prevManager.gameObject.name}");
                    activeManager.UpdateState();
                    prevManager.Stop();
                    prevManager.ChangePlaybackSpeed(1f);
                    prevManager.gameObject.SetActive(false);
                }

                prevManager = activeManager;
            }

            // only do epsilon jump when media is actively loaded
            if (jumpToTime == EPSILON && (int)state > (int)TVPlayState.STOPPED)
            {
                // If jumptime is still epsilon, a non-switching reload occurred. Calculate new jump time, including buffer delay.
                float diff = Time.timeSinceLevelLoad - reloadStart;
                jumpToTime = reloadCache + diff + bufferDelayAfterLoad;
                if (jumpToTime > endTime) jumpToTime = endTime;
                if (IsTraceEnabled) Trace($"Jump to time is Epsilon, Jumping to {jumpToTime} from start {reloadStart} and cache {reloadCache}");
            }
            else jumpToTime = startTime;

            if (IsInfoEnabled)
            {
                var added = string.IsNullOrEmpty(addedBy) ? Owner.displayName : addedBy;
                Info($"[{activeManager.gameObject.name}] ({added}) Now Playing: {url}");
            }

            activeManager.ChangeMute(mute || manuallyHidden);

            if (endTime < startTime)
            {
                if (IsDebugEnabled) Debug($"endTime {endTime} precedes startTime {startTime}. Updating.");
                startTime = 0f; // invalid start time given, zero-out
            }

            if (currentTime + 0.1f >= endTime)
            {
                if (IsDebugEnabled) Debug($"last playing time {currentTime} exceeds the new media end time {endTime}. Updating.");
                jumpToTime = startTime;
            }

            if (jumpToTime < startTime)
            {
                if (IsDebugEnabled) Debug($"jumpToTime {jumpToTime} precedes startTime {startTime}. Updating.");
                jumpToTime = startTime;
            }

            locallyPaused = false;
            // clear the retry flags on successful video load
            retrying = false;
            retryingWithAlt = false;
            retriedWithAlt = false;
            errorState = TVErrorState.NONE;
            if (IsOwner) errorStateOwner = TVErrorState.NONE;
        }

        private void startMedia()
        {
            if (IsInfoEnabled && jumpToTime > 0f) Info($"Jumping [{activeManager.gameObject.name}] to timestamp: {jumpToTime}");
            activeManager.videoPlayer.SetTime(jumpToTime);
            lastJumpToTime = jumpToTime;
            jumpToTime = 0f;

            setLoadingState(false);
            if (IsOwner)
            {
                stateOwner = syncState = playVideoAfterLoad || isLive ? TVPlayState.PLAYING : TVPlayState.PAUSED;
                RequestSync();
            }

            var checkState = syncState;
            if (isLive || lastJumpToTime > 0f) checkState = TVPlayState.PLAYING;
            SendManagedVariable(nameof(TVPlugin.OUT_URL), url);
            SendManagedEvent(nameof(TVPlugin._TvMediaReady));
            if (checkState == TVPlayState.PAUSED || locallyPaused)
            {
                if (IsDebugEnabled) Debug($"Media starting paused. (local pause {locallyPaused}");
                state = TVPlayState.PAUSED;
                SendManagedEvent(nameof(TVPlugin._TvPause));
            }
            else
            {
                if (IsDebugEnabled) Debug("Media starting playing.");
                state = TVPlayState.PLAYING;
                activeManager.videoPlayer.Play();
                SendManagedEvent(nameof(TVPlugin._TvPlay));
            }
        }

        private void cacheMediaReadyInfo()
        {
            // grab parameters
            float value = 0f;
            int check = 0;
            string param = null;
            if (isLive)
            {
                startTime = 0f;
                endTime = INF;
                videoDuration = INF;
                loop = 0;
                // always have at least 1 retry for any live content
                if (retryCount == 0) retryCount = 1;
            }
            else
            {
                // check for start param
                param = getUrlParam("start", EMPTYSTR);
                if (float.TryParse(param, out value)) startTime = value;
                else startTime = 0f;

                // check for end param
                param = getUrlParam("end", EMPTYSTR);
                if (float.TryParse(param, out value)) endTime = value;
                else endTime = mediaLength;
                videoDuration = endTime - startTime;

                // if loop is present without value, default to -1
                check = 0;
                param = getUrlParam("loop", "-1");
                bool parsed = int.TryParse(param, out check);
                // if loop is not explicitly provided but duration is upto 30 seconds, implicilty loop it once
                // generally helpful for really short meme clips in-case someone takes too long to load the video the first time
                if (!parsed && videoDuration <= 30f) check = 1;
                bool oldState = loop != 0;
                bool newState = check != 0;
                loop = check;
                if (loop < 0) loop = int.MaxValue;
                if (oldState != newState) SendManagedEvent(loop > 0 ? nameof(TVPlugin._TvEnableLoop) : nameof(TVPlugin._TvDisableLoop));

                // check for t or start params, only update jumpToTime if start or t succeeds
                // only parse if another jumpToTime value has not been set.
                if (jumpToTime <= startTime)
                {
                    param = getUrlParam("t", EMPTYSTR);
                    if (float.TryParse(param, out value)) jumpToTime = value;
                }
            }


            // check for end param
            param = getUrlParam("aspect", EMPTYSTR);
            if (param.Contains(":"))
            {
                var pair = param.Split(new[] { ':' }, 2, StringSplitOptions.RemoveEmptyEntries);
                if (int.TryParse(pair[0], out int aspectWidth) && int.TryParse(pair[1], out int aspectHeight))
                    aspectRatio = (float)aspectWidth / aspectHeight;
                else aspectRatio = targetAspectRatio;
            }
            else if (float.TryParse(param, out value)) aspectRatio = value;
            else aspectRatio = targetAspectRatio;

            param = getUrlParam("3D", "1");
            var lastWidth = width3dFull;
            var lastMode = mode3d;
            if (int.TryParse(param, out check))
            {
                width3dFull = check < 0;
                check = Math.Abs(check);
                mode3d = check > 4 ? TV3DMode.NONE : (TV3DMode)check;
            }
            else
            {
                width3dFull = false;
                mode3d = TV3DMode.NONE;
            }

            if (mode3d != lastMode)
            {
                SendManagedVariable(nameof(TVPlugin.OUT_MODE), mode3d);
                SendManagedEvent(nameof(TVPlugin._Tv3DModeChange));
            }

            if (width3dFull != lastWidth) SendManagedEvent(width3dFull ? nameof(TVPlugin._Tv3DWidthFull) : nameof(TVPlugin._Tv3DWidthHalf));

            param = getUrlParam("spread", "0");
            if (float.TryParse(param, out value)) spread3d = value;
            else spread3d = 0;

            if (IsDebugEnabled) Debug("Params set after video is ready");
            if (IsTraceEnabled) Trace($"Start info loaded: start={startTime}, end={endTime}, t={jumpToTime}, loop={loop}, 3D={mode3d}, 3D[Full]={width3dFull}");
        }

        private void cacheMediaChangeInfo()
        {
            string param = EMPTYSTR;
            // if retry is present without value, default to -1
            _TryGetUrlParam("retry", "-1", out param);
            int value;
            if (int.TryParse(param, out value)) retryCount = value;
            if (retryCount < 0) retryCount = int.MaxValue;

            isLive = _HasUrlParam("live") || System.Array.IndexOf(liveProtocols, urlProtocol) > -1;

            if (IsTraceEnabled) Trace($"Change info loaded: retry={retryCount}, live={isLive}");
        }

        // === Public events to control the TV from user interfaces ===

        /// <summary>
        /// The nexus event/method which drives the logic for handling how the TV deals with changes to the active media.
        /// </summary>
        [PublicAPI]
        public void _RefreshMedia()
        {
            if (!init) return;

            if (loading)
            {
                if (IsWarnEnabled) Warn("Cannot change to another media while loading.");
                return; // disallow refreshing media while TV is loading another video
            }

            // disallow non-owners from changing media while the TV is running a managed or targeted events.
            if (runningEvents && !IsOwner) return;

            if (!CanPlayMedia)
            {
                // if TV is locked without being privileged, force unset any requested URLs
                // This converts the command into a simple video refresh
                if (IsWarnEnabled) Warn("TV is locked. Cannot change media for un-privileged users.");
                IN_MAINURL = EMPTYURL;
                IN_ALTURL = EMPTYURL;
                IN_TITLE = EMPTYSTR;
            }
            else if (AutoOwnershipAvailable)
            {
                if (IsTraceEnabled) Trace($"Taking ownership due to auto-ownership");
                takeOwnership();
            }

            // compare input URL and previous URL
            if (IN_MAINURL == null) IN_MAINURL = EMPTYURL;
            if (IN_ALTURL == null) IN_ALTURL = EMPTYURL;
            if (IN_TITLE == null) IN_TITLE = EMPTYSTR;
            string urlMainStr = IN_MAINURL.Get();
            string urlAltStr = IN_ALTURL.Get();
            bool hasMainUrl = urlMainStr != EMPTYSTR;
            bool hasAltUrl = urlAltStr != EMPTYSTR;
            bool hasTitle = IN_TITLE != EMPTYSTR;
            bool newMainUrl = hasMainUrl && urlMainStr != urlMain.Get();
            bool newAltUrl = hasAltUrl && urlAltStr != urlAlt.Get();
            bool newTitle = hasTitle && IN_TITLE != title;
            bool updateTitle = false;
            bool hasUrl = hasMainUrl || hasAltUrl;
            bool newUrl = newMainUrl || newAltUrl;

            if (hasUrl)
            {
                if (newUrl && IsDebugEnabled) Debug("New URL(s) detected.");

                if (!_CheckDomainWhitelist(urlMainStr, urlAltStr))
                {
                    errorState = TVErrorState.BLOCKED;
                    _OnVideoPlayerError(VideoError.AccessDenied);
                    // deny access and exit logic
                    IN_MAINURL = EMPTYURL;
                    IN_ALTURL = EMPTYURL;
                    IN_TITLE = EMPTYSTR;
                    return;
                }

                // when new URLs are detected, grab ownership to handle the sync data
                takeOwnership();
                RequestSync();
                // update relavent URL data
                urlMain = syncUrlMain = IN_MAINURL;
                urlAlt = syncUrlAlt = IN_ALTURL;
                syncTitle = IN_TITLE;
                updateTitle = title != syncTitle;
                title = syncTitle;
                addedBy = localPlayer.displayName;
                urlRevision++;
                syncUrlRevision = urlRevision;
                // reset the alternate URL flag back to default
                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                useAlternateUrl = preferAlternateUrlForQuest ? isAndroid : false;
                if (state == TVPlayState.WAITING)
                    state = syncState = stateOwner = TVPlayState.STOPPED;
                errorState = TVErrorState.NONE;
                // new URL, reset the retry flags
                retryingWithAlt = false;
                retriedWithAlt = false;
            }
            else if (newTitle)
            {
                if (IsDebugEnabled) Debug("Explicit title change detected.");
                // just the title is changing, skip reloading and just notify about the title change
                takeOwnership();
                RequestSync();
                title = syncTitle = IN_TITLE;
                IN_TITLE = EMPTYSTR;
                SendManagedVariable(nameof(TVPlugin.OUT_TITLE), title);
                SendManagedEvent(nameof(TVPlugin._TvTitleChange));
                return;
            }
            else
            {
                if (IsDebugEnabled) Debug("No URL change. Running generic reload.");
                // nothing is changing, thus a reload is taking place, pull from the synced variables
                urlMain = syncUrlMain;
                urlAlt = syncUrlAlt;
                title = syncTitle;
                addedBy = syncAddedBy;
            }

            IN_MAINURL = EMPTYURL;
            IN_ALTURL = EMPTYURL;
            IN_TITLE = EMPTYSTR;

            urlMainStr = urlMain.Get();
            urlAltStr = urlAlt.Get();

            // sanity checks
            if (urlMainStr == null)
            {
                urlMain = EMPTYURL;
                urlMainStr = EMPTYSTR;
            }

            if (urlAltStr == null)
            {
                urlAlt = EMPTYURL;
                urlAltStr = EMPTYSTR;
            }

            // graceful fallback checks
            if (urlAltStr == EMPTYSTR)
            {
                urlAlt = urlMain;
                urlAltStr = urlMainStr;
            }

            if (urlMainStr == EMPTYSTR)
            {
                urlMain = urlAlt;
                urlMainStr = urlAltStr;
            }

            if (urlMainStr == EMPTYSTR)
            {
                if (IsDebugEnabled) Debug("No URLs present. Skip.");
                return;
            }

            url = useAlternateUrl ? urlAlt : urlMain;
            if (stopMediaWhenHidden && manuallyHidden) return; // skip URL loading if media is force hidden
            string urlStr = url.Get();
            bool newMedia = urlStr.GetHashCode() != mediaHash;
            if (IsInfoEnabled) Info($"[{nextManager.gameObject.name}] loading URL: {urlStr}");

            // when the media is not actually changing links and it's currently loaded, run timestamp continuity
            if (!newMedia && (int)state > (int)TVPlayState.STOPPED)
            {
                reloadStart = Time.timeSinceLevelLoad;
                // if epsilon is set prior to this point, there is a video swap going on. Use previous manager time instead.
                reloadCache = syncTime;
                // skip timestamp continuity if the video is a retry attempt after an error
                jumpToTime = retrying || forceRestartMedia ? 0f : EPSILON;
                if (IsTraceEnabled) Trace($"Refresh, jump time: {jumpToTime} | retry {retrying} || force {forceRestartMedia}");
                forceRestartMedia = false;
            }

            // if alternate URL is provided without a main URL and the user isn't assigned to use the alternate url, skip.
            if (IsTraceEnabled) Trace($"useAlt {useAlternateUrl} hasAlt {hasAltUrl} hasMain {hasMainUrl}");
            bool urlChange = !(!hasMainUrl && hasAltUrl && !useAlternateUrl);
            if (urlChange)
            {
                parseUrl(urlStr, out urlProtocol, out urlDomain, out urlParamKeys, out urlParamValues);

                // only cache once per url
                if (newMedia && errorState != TVErrorState.RETRY)
                {
                    retryCount = defaultRetryCount;
                    if (retryUsingAlternateUrl)
                        if (retryCount == 0)
                            if (urlMainStr != urlAltStr)
                                retryCount = 1;
                    cacheMediaChangeInfo();
                }

                loading = true;
                waiting = false; // halt any queued refreshes
                if (errorState == TVErrorState.BLOCKED) errorState = TVErrorState.NONE;
                nextManager.videoPlayer.LoadURL(url);
                // rate limit stuff
                nextUrlAttemptAllowed = Time.timeSinceLevelLoad + 6f;
            }

            if (urlChange)
            {
                SendManagedVariable(nameof(TVPlugin.OUT_URL), url);
                SendManagedEvent(nameof(TVPlugin._TvMediaChange));
            }

            if (updateTitle)
            {
                SendManagedVariable(nameof(TVPlugin.OUT_TITLE), title);
                SendManagedEvent(nameof(TVPlugin._TvTitleChange));
            }

            if (urlChange && (errorState != TVErrorState.FAILED || waiting))
            {
                setLoadingState(true);
            }
        }

        public Texture _GetVideoTexture() => customTexture;

        // === Networked methods ===

        public void OWNER_RequestSyncData() => RequestSync();

        public void ALL_QuickReSync()
        {
            if (syncToOwner && !IsOwner)
            {
                if (IsTraceEnabled) Trace($"Resync triggered by network. Waiting 0.2f seconds");
                triggerSync(0.3f);
            }
        }

        public void ALL_ManualReSync()
        {
            if (syncToOwner)
            {
                if (IsTraceEnabled) Trace($"Resync triggered by network. Waiting {syncEnforcementTimeLimit} seconds");
                triggerSync(syncEnforcementTimeLimit);
            }
        }

        public void ALL_OwnerEnabled()
        {
            Debug("Enabling owner via Network call");
            ownerDisabled = false;
        }

        public void ALL_OwnerDisabled()
        {
            Debug("Disabling owner via Network call");
            ownerDisabled = true;
        }
    }
}