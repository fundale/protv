using System;
using ArchiTech.SDK;
using UdonSharp;
using UnityEngine;

namespace ArchiTech.ProTV
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    public abstract class TVPluginUI : ATBehaviour
    {
        [NonSerialized] public bool OUT_STATE;
        [NonSerialized] public int OUT_MODE = -1;
        [NonSerialized] public int OUT_VALUE = 0;
        [NonSerialized] public string OUT_TEXT;

        public abstract void UpdateUI();
    }
}