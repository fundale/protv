using ArchiTech.SDK;
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon.Common;

namespace ArchiTech.ProTV
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
    [DefaultExecutionOrder(-9998)] // needs to initialize just after any TVManagerV2 script
    public class TVManagerData : ATBehaviour
    {
        private TVManager tv;

        [UdonSynced] internal TVPlayState state = TVPlayState.STOPPED;
        [UdonSynced] internal TVErrorState errorState = TVErrorState.NONE;
        [UdonSynced] internal VRCUrl mainUrl = new VRCUrl("");
        [UdonSynced] internal VRCUrl alternateUrl = new VRCUrl("");
        [UdonSynced] internal string title = EMPTYSTR;
        [UdonSynced] internal string addedBy = EMPTYSTR;
        [UdonSynced] internal bool locked = false;
        [UdonSynced] internal bool loading = false;
        [UdonSynced] internal int urlRevision = 0;
        [UdonSynced] internal int videoPlayer = -1;
        [UdonSynced] internal float playbackSpeed = 1f;
        [UdonSynced] internal float volume = 0;
        [UdonSynced] internal bool audio3d = false;
        [UdonSynced] internal int loop = 0;
        [UdonSynced] internal float time = 0;
        [UdonSynced] internal string firstMaster = EMPTYSTR;

        public override void Start()
        {
            if (init) return;
            SetLogPrefixColor("#cccc44");
            base.Start();
            tv = GetComponentInParent<TVManager>();
            if (tv == null) SetLogPrefixLabel($"<Missing TV Ref>/{name}");
            else
            {
                SetLogPrefixLabel($"{tv.gameObject.name}/{name}");
                if (Logger == null) Logger = tv.Logger;
                if (tv.LogLevelOverride) LoggingLevel = tv.LoggingLevel;
            }
        }

        public override void OnPreSerialization()
        {
            // Extract data from TV for manual sync
            state = tv.state;
            mainUrl = tv.urlMain;
            alternateUrl = tv.urlAlt;
            title = tv.title;
            addedBy = tv.addedBy;
            // sanity check for rare case where unity serialization fucks up and nullifies the internal string of a supposedly empty VRCUrl
            if (mainUrl.Get() == null) mainUrl = VRCUrl.Empty;
            if (alternateUrl.Get() == null) alternateUrl = VRCUrl.Empty;
            // pull the owner's values
            locked = tv.locked;
            loading = tv.loading;
            urlRevision = tv.urlRevision;
            videoPlayer = tv.videoPlayer;
            errorState = tv.errorState;
            volume = tv.volume;
            audio3d = tv.audio3d;
            playbackSpeed = tv.playbackSpeed;
            loop = tv.loop;
            time = tv.currentTime;
            firstMaster = tv.firstMaster;
            if (IsTraceEnabled)
            {
                string log = "PreSerialization Data:";
                log += $"\nPlay State {state} | Error State {errorState} | Loading State {loading}";
                log += $"\nLocked {locked} | Url Revision {urlRevision} | Video Player {videoPlayer}";
                log += $"\nMain URL {mainUrl} | Alt URL {alternateUrl} | Title {title}";
                log += $"\nVolume {volume} | Time {time}";
                Trace(log);
            }
        }

        public override void OnPostSerialization(SerializationResult result)
        {
            if (result.success)
            {
                Debug("All good.");
                updateSyncedData();
                tv.stateOwner = state;
            }
            else
            {
                Warn("Failed to sync, retrying.");
                SendCustomEventDelayedSeconds(nameof(_RequestData), 5f);
            }
        }

        public override void OnDeserialization()
        {
            if (IsTraceEnabled)
            {
                string log = "Deserialization Data:";
                log += $"\nPlay State {state} | Error State {errorState} | Loading State {loading}";
                log += $"\nLocked {locked} | Url Revision {urlRevision} | Video Player {videoPlayer}";
                log += $"\nMain URL {mainUrl} | Alt URL {alternateUrl} | Title {title}";
                log += $"\nVolume {volume} | Time {time}";
                Trace(log);
            }

            updateSyncedData();
            tv._PostDeserialization();
        }

        private void updateSyncedData()
        {
            tv.syncState = state;
            tv.syncUrlMain = mainUrl;
            tv.syncUrlAlt = alternateUrl;
            tv.syncTitle = title;
            tv.syncLoading = loading;
            tv.syncAddedBy = addedBy;
            tv.syncLocked = locked;
            tv.syncUrlRevision = urlRevision;
            tv.syncVideoPlayer = videoPlayer;
            tv.syncErrorState = errorState;
            tv.syncVolume = volume;
            tv.syncAudio3d = audio3d;
            tv.syncLoop = loop;
            tv.syncPlaybackSpeed = playbackSpeed;
            tv.syncTime = time;
            if (tv.firstMaster == EMPTYSTR)
                tv.firstMaster = firstMaster;
        }

        public override bool OnOwnershipRequest(VRCPlayerApi requestingPlayer, VRCPlayerApi requestedOwner)
        {
            // allow transfer if unlocked or if the requesting player has enough privilege
            return (!tv.locked && !tv.disallowUnauthorizedUsers) || tv._IsAuthorized(requestingPlayer);
        }

        public void _RequestData()
        {
            Start();
            if (IsDebugEnabled) Debug("Requesting serialization");
            RequestSerialization();
        }
    }
}