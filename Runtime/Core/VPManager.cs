using System;
using System.Runtime.CompilerServices;
using ArchiTech.SDK;
using UdonSharp;
using UnityEngine;
using UnityEngine.Serialization;
using VRC.SDK3.Components.Video;
using VRC.SDK3.Video.Components.Base;
using VRC.SDKBase;
using VRC.Udon;

// ReSharper disable RedundantCast

// ReSharper disable UseObjectOrCollectionInitializer

namespace ArchiTech.ProTV
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    [RequireComponent(typeof(BaseVRCVideoPlayer), typeof(MeshRenderer))]
    [DefaultExecutionOrder(-9998)] // init immediately after the TV
    public partial class VPManager : ATBehaviour
    {
        [NonSerialized] public BaseVRCVideoPlayer videoPlayer;
        private TVManager tv;

        /// <summary>
        /// 
        /// </summary>
        [
            I18nInspectorName("Custom Label"), I18nTooltip("A custom name/label for the video manager that can be used by plugins. Typically shows up in any MediaControls dropdowns.")
        ]
        public string customLabel = "";

        // Speaker Management
        [HideInInspector, SerializeField,
         I18nInspectorName("Auto-Manage Volume"), I18nTooltip("Flag whether or not to have this video manager should automatically control the speakers' volume.")
        ]
        internal bool autoManageVolume = true;

        [HideInInspector, SerializeField,
         I18nInspectorName("Auto-Manage Mute"), I18nTooltip("Flag whether or not to have this video manager should automatically control the speakers' mute state.")
        ]
        internal bool autoManageMute = true;

        [HideInInspector, SerializeField,
         I18nInspectorName("Managed Screens")
        ]
        internal GameObject[] screens;

        [SerializeField, FormerlySerializedAs("speakers"),
         I18nInspectorName("Spatial (3D)")
        ]
        internal AudioSource[] spatialSpeakers;

        [SerializeField, FormerlySerializedAs("managedSpeakerVolume"),
         I18nInspectorName("Volume")
        ]
        internal bool[] managedSpatialVolume = new bool[0];

        [SerializeField, FormerlySerializedAs("managedSpeakerMute"),
         I18nInspectorName("Mute")
        ]
        internal bool[] managedSpatialMute = new bool[0];

        [SerializeField,
         I18nInspectorName("Stereo (2D)")
        ]
        internal AudioSource[] stereoSpeakers;

        [SerializeField,
         I18nInspectorName("Volume")
        ]
        internal bool[] managedStereoVolume = new bool[0];

        [SerializeField,
         I18nInspectorName("Mute")
        ]
        internal bool[] managedStereoMute = new bool[0];

        [HideInInspector, SerializeField] internal bool isAVPro;
        [HideInInspector, SerializeField] internal Renderer matRenderer;
        internal Animator mediaController;

        [NonSerialized] public bool isVisible;
        [NonSerialized] public bool mute = true;
        [NonSerialized] public float volume = 0.5f;
        [NonSerialized] public bool audio3d = true;

        private RenderTexture _internalTexture;
        private RenderTexture _bakedTexture;
        private Material[] _customMaterials = new Material[0];
        private Material _videoMat;
        private Material _blitMaterial;
        private bool[] _spatialMuteCache;
        private bool[] _stereoMuteCache;
        private bool hasInternalTexture = false;
        private bool hasBakedTexture = false;
        private bool hasCustomMaterials;
        private bool hasTV;
        private bool _customTextureIsGlobal = false;
        private int _sourceWidth;
        private int _sourceHeight;
        private bool _hasVideoMat;
        private bool _applyResize;
        private Texture2D _standby;
        private Texture2D _soundOnlyFallback;
        internal float playbackSpeed = 1f;
        private bool hasMediaController;
        private const string _playbackSpeedParameter = "PlaybackSpeed";

        // private bool extractionInProgress = false;
        internal Color32[] pixels = new Color32[0];
        internal Vector2Int pixelDims = Vector2Int.zero;

        // properties for the blit operation when using render texture
        private const string shaderName_MainTex = "_MainTex";
        private const string shaderName_MainTex_ST_Override = "_MainTex_ST_Override";
        private MaterialPropertyBlock matBlock;

        internal const string shaderNameGlobal_VideoTex = "_Udon_VideoTex";
        internal const string shaderNameGlobal_VideoData = "_Udon_VideoData";
        internal const string shaderName_VideoData = "_VideoData";

        internal const string shaderName_ForceAspect = "_ForceAspect";
        internal const string shaderName_SkipGamma = "_SkipGamma";
        internal const string shaderName_3D = "_3D";
        internal const string shaderName_AVPro = "_AVPro";

        private int shaderID_MainTex;
        private int shaderID_MainTex_ST;
        private int shaderID_MainTex_ST_Override;

        private int shaderIDGlobal_VideoTex;
        private int shaderIDGlobal_VideoData;
        private int[] shaderIDs_MaterialProperties = new int[0];
        private int shaderID_VideoData;

        private int shaderID_AVPro;
        private int shaderID_ForceAspect;
        private int shaderID_SkipGamma;
        private int shaderID_3D;

        // AVPro has a playback speed defect. Disable for AVPro until it's fixed.
        internal bool ValidMediaController => hasMediaController;

        public bool IsManagedSpeaker(AudioSource source)
        {
            var idx = Array.IndexOf(spatialSpeakers, source);
            if (idx == -1) idx = Array.IndexOf(stereoSpeakers, source);
            return idx > -1;
        }

        public override void Start()
        {
            if (init) return;
            SetLogPrefixColor("#00ccaa");
            base.Start();

            var stereoLen = stereoSpeakers.Length;
            var spatialLen = spatialSpeakers.Length;
            _stereoMuteCache = new bool[stereoLen];
            _spatialMuteCache = new bool[spatialLen];

            foreach (bool s in managedSpatialVolume)
            {
                autoManageVolume = s;
                if (s) break;
            }

            if (!autoManageVolume)
            {
                foreach (bool s in managedStereoVolume)
                {
                    autoManageVolume = s;
                    if (s) break;
                }
            }

            foreach (bool s in managedSpatialMute)
            {
                autoManageMute = s;
                if (s) break;
            }

            if (!autoManageMute)
            {
                foreach (bool s in managedStereoMute)
                {
                    autoManageMute = s;
                    if (s) break;
                }
            }

            videoPlayer = (BaseVRCVideoPlayer)GetComponent(typeof(BaseVRCVideoPlayer));
            videoPlayer.EnableAutomaticResync = false;

            mediaController = GetComponent<Animator>();
            hasMediaController = mediaController != null;

            matBlock = new MaterialPropertyBlock();
            if (matRenderer == null) matRenderer = GetComponent<MeshRenderer>();

            shaderID_MainTex = VRCShader.PropertyToID(shaderName_MainTex);
            shaderID_MainTex_ST = VRCShader.PropertyToID(shaderName_MainTex + "_ST");
            shaderID_MainTex_ST_Override = VRCShader.PropertyToID(shaderName_MainTex_ST_Override);
            shaderID_VideoData = VRCShader.PropertyToID(shaderName_VideoData);
            shaderIDGlobal_VideoTex = VRCShader.PropertyToID(shaderNameGlobal_VideoTex);
            shaderIDGlobal_VideoData = VRCShader.PropertyToID(shaderNameGlobal_VideoData);

            shaderID_ForceAspect = VRCShader.PropertyToID(shaderName_ForceAspect);
            shaderID_SkipGamma = VRCShader.PropertyToID(shaderName_SkipGamma);
            shaderID_3D = VRCShader.PropertyToID(shaderName_3D);
            shaderID_AVPro = VRCShader.PropertyToID(shaderName_AVPro);

#if UNITY_2022_3_OR_NEWER
            SetTV(GetComponentInParent<TVManager>(true));
#else
            SetTV(GetComponentInParent<TVManager>());
#endif
        }

        private void OnEnable()
        {
            Start();
            if (hasMediaController)
            {
                mediaController.enabled = true;
                mediaController.Rebind();
            }
        }

        #region Video Engine Proxy Methods

        public override void OnVideoEnd() => tv._OnVideoPlayerEnd();
        public override void OnVideoError(VideoError error) => tv._OnVideoPlayerError(error);

        public override void OnVideoReady()
        {
            if (tv.Buffering) return; // skip possible duplicate calls
            tv._OnVideoPlayerReady();
        }

        public override void OnVideoPlay() => tv._OnVideoPlayerPlay();

        #endregion

        // === Public events to control the video player parts ===

        public void Show()
        {
            Start();
            foreach (var screen in screens)
            {
                if (screen == null) continue;
                screen.SetActive(true);
            }

            if (autoManageMute) ChangeMute(false);
            isVisible = true;
            Debug("Activated");
        }

        [Obsolete("Use Hide() instead")]
        public void _Hide() => Hide();

        public void Hide()
        {
            Start();
            if (autoManageMute) ChangeMute(true);
            if (tv.videoManagers.Length > 1)
            {
                foreach (var screen in screens)
                {
                    if (screen == null) continue;
                    screen.SetActive(false);
                }
            }

            isVisible = false;
            Debug("Deactivated");
        }

        public void Stop()
        {
            Hide();
            videoPlayer.Stop();
        }

        public void UpdateState()
        {
            // audiomode is first to make sure the correct speakers are targeted.
            ChangeAudioMode(tv.audio3d);
            ChangeMute(tv.mute);
            ChangeVolume(tv.volume);
            ChangePlaybackSpeed(tv.playbackSpeed);
        }

        #region Speaker Control

        public void ChangeMute(bool muted)
        {
            Start();
            if (!autoManageMute) return;
            mute = muted;
            var targetSpeakers = audio3d ? spatialSpeakers : stereoSpeakers;
            var targetMute = audio3d ? managedSpatialMute : managedStereoMute;
            if (IsTraceEnabled) Trace($"Speakers count {targetSpeakers.Length} | setting mute {mute}");
            for (var index = 0; index < targetSpeakers.Length; index++)
            {
                if (targetMute[index])
                {
                    var speaker = targetSpeakers[index];
                    if (speaker == null) continue;
                    speaker.mute = mute;
                }
            }
        }

        public void ChangeVolume(float useVolume, bool suppressLog = false)
        {
            Start();
            if (!autoManageVolume) return;
            volume = useVolume;
            var targetSpeakers = audio3d ? spatialSpeakers : stereoSpeakers;
            var targetVolume = audio3d ? managedSpatialVolume : managedStereoVolume;
            if (!suppressLog && IsTraceEnabled) Trace($"Speakers count {targetSpeakers.Length} | setting volume {volume}");
            for (var index = 0; index < targetSpeakers.Length; index++)
            {
                if (targetVolume[index])
                {
                    var speaker = targetSpeakers[index];
                    if (speaker == null) continue;
                    speaker.volume = volume;
                }
            }
        }

        public void ChangeAudioMode(bool use3dAudio)
        {
            Start();
            audio3d = use3dAudio;
            var fromSpeakers = audio3d ? stereoSpeakers : spatialSpeakers;
            var fromMuteCache = audio3d ? _stereoMuteCache : _spatialMuteCache;
            var toSpeakers = audio3d ? spatialSpeakers : stereoSpeakers;
            var toMuteCache = audio3d ? _spatialMuteCache : _stereoMuteCache;
            var toVolume = audio3d ? managedSpatialVolume : managedStereoVolume;
            if (IsTraceEnabled) Trace($"Setting audio mode to {(audio3d ? "3D" : "2D")}");
            for (var index = 0; index < fromSpeakers.Length; index++)
            {
                var speaker = fromSpeakers[index];
                if (speaker == null) continue;
                fromMuteCache[index] = speaker.mute;
                speaker.mute = true;
            }

            for (var index = 0; index < toSpeakers.Length; index++)
            {
                var speaker = toSpeakers[index];
                if (speaker == null) continue;
                speaker.mute = toMuteCache[index];
                // ensure volume setting is caught up as well
                if (toVolume[index]) speaker.volume = volume;
            }
        }

        /// <summary>
        /// Modify the playback speed of the current video player if the animator for managing it is available.
        /// Only really works on the UnityVideo, AVPro doesn't correctly respect the playback speed.
        /// Hardcoded limits are slow 0.5f and fast 2f. Values beyond those extremes tend to be intolerably bad quality.
        /// </summary>
        /// <param name="speed">The relative speed adjustment desired (allows 0.5f to 2f)</param>
        public void ChangePlaybackSpeed(float speed)
        {
            Start();
            if (!ValidMediaController) return;
            speed = Mathf.Clamp(speed, 0.5f, 2f);
            playbackSpeed = speed; // cache
            if (IsTraceEnabled) Trace($"Updating PlaybackSpeed to {playbackSpeed}");
            mediaController.SetFloat(_playbackSpeedParameter, speed);
        }

        #endregion

        // ================= Helper Methods =================

        public void SetTV(TVManager manager)
        {
            if (manager == null)
            {
                Warn("No TV Reference provided");
                return;
            }

            tv = manager;
            hasTV = true;

            audio3d = tv.isReady ? tv.audio3d : !tv.startWith2DAudio;
            mute = tv.mute;
            volume = tv.isReady ? tv.volume : tv.defaultVolume;

            // The TV should hold the refrerence to the internal render texture to be used.
            // If there is none, this is the first VPManager to be active, it will create and assign implicitly.
            _internalTexture = tv.rawTexture;
            hasInternalTexture = _internalTexture != null;
            if (!hasInternalTexture)
            {
                _internalTexture = createInternalTexture();
                tv.rawTexture = _internalTexture;
                hasInternalTexture = true;
            }

            _bakedTexture = tv.customTexture;
            hasBakedTexture = _bakedTexture != null;
            _applyResize = hasBakedTexture && tv.autoResizeTexture;
            _customMaterials = tv.customMaterials;
            hasCustomMaterials = _customMaterials != null;
            _blitMaterial = tv.blitMaterial;
            _standby = tv.defaultStandbyTexture;
            _soundOnlyFallback = tv.soundOnlyTexture;

            if (_applyResize && _bakedTexture != null)
            {
                if (_bakedTexture.IsCreated()) _bakedTexture.Release();
                _bakedTexture.width = 16;
                _bakedTexture.height = 16;
            }

            if (Logger == null) Logger = tv.Logger;
            if (tv.LogLevelOverride) LoggingLevel = tv.LoggingLevel;
            SetLogPrefixLabel($"{tv.gameObject.name}/{name}");

            var customProperties = tv.customMaterialProperties;
            // derive the target property for the video texture for custom material from the TV settings.
            shaderIDs_MaterialProperties = new int[tv.customMaterials.Length];
            for (var index = 0; index < customProperties.Length; index++)
                shaderIDs_MaterialProperties[index] = VRCShader.PropertyToID(customProperties[index]);
        }

        #region Texture Handling

        private RenderTexture createInternalTexture()
        {
            RenderTextureFormat format = tv.enableHDR ? RenderTextureFormat.DefaultHDR : RenderTextureFormat.ARGB64;
            RenderTextureReadWrite rw = tv.enableHDR ? RenderTextureReadWrite.Linear : RenderTextureReadWrite.sRGB;
            var tex = VRCRenderTexture.GetTemporary(16, 16, 0, format, rw, 1);
            tex.wrapMode = TextureWrapMode.Clamp;
            tex.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
            tex.filterMode = FilterMode.Trilinear;
            // mobile phones don't seem to like ansio? Just disable it on that platform.
            if (isInVR || !isAndroid) tex.anisoLevel = 16;
            tex.useMipMap = true;
            tex.autoGenerateMips = true;
            return tex;
        }

        public void Blit()
        {
            // to enable swapping global tv target, always check for the most up to date value
            if (!init) Start();
            if (!hasTV) return; // no TV, no Blit.
            bool hasGSVEnabled = tv.enableGSV;
            if (!hasInternalTexture && !hasCustomMaterials && !hasGSVEnabled) return; // nothing to do without a texture, material or shader globals to copy to

            var blitTexture = GetVideoTexture(out var blitST);
            var blitAVPro = isAVPro;
            bool latestNull = blitTexture == null;

            bool applyResizeAspect = tv.applyAspectToResize;
            float aspect = tv.aspectRatio;
            var shaderVideoData = tv.shaderVideoData;

            if (tv.standbyOnMediaEnd && (tv.IsEnded || tv.IsSkipping) || tv.standbyOnMediaPause && tv.IsPaused || tv.disableVideo)
            {
                latestNull = true;
                blitTexture = null;
            }

            // handle fallback textures
            if (latestNull && !tv.disableStandby)
            {
                var seek = tv.SeekPercent;
                var standbyCheck = !tv.disableVideo;
                var showSound = standbyCheck && tv.state == TVPlayState.PLAYING && (tv.isLive || seek > 0f && seek < 1f);
                var _3d = (int)tv.standby3dMode;
                shaderVideoData.m30 = (float)_3d;
                if (showSound && _soundOnlyFallback != null)
                {
                    latestNull = false;
                    blitTexture = _soundOnlyFallback;
                    blitST = new Vector4(1, 1, 0, 0);
                    blitAVPro = false;
                }
                else if (standbyCheck && _standby != null)
                {
                    latestNull = false;
                    blitTexture = _standby;
                    blitST = new Vector4(1, 1, 0, 0);
                    blitAVPro = false;
                }
            }

            var latestTextureHeight = latestNull ? 16 : blitTexture.height;
            var latestTextureWidth = latestNull ? 16 : blitTexture.width;
            bool resizeTexture = latestTextureHeight != _sourceHeight || latestTextureWidth != _sourceWidth;
            _sourceWidth = latestTextureWidth;
            _sourceHeight = latestTextureHeight;


            // handle internal texture
            if (resizeTexture)
            {
                if (_internalTexture.IsCreated()) _internalTexture.Release();
                _internalTexture.width = _sourceWidth;
                _internalTexture.height = _sourceHeight;
                if (IsDebugEnabled) Debug($"Texture updating to {_sourceWidth}x{_sourceHeight} ({(float)_sourceWidth / _sourceHeight})");
            }

            if (latestNull)
            {
                if (_internalTexture.IsCreated()) _internalTexture.Release();
            }
            else if (blitTexture != _internalTexture)
            {
                if (!_internalTexture.IsCreated()) _internalTexture.Create();
                _blitMaterial.SetFloat(shaderID_SkipGamma, tv.skipGamma ? 1f : 0);
                _blitMaterial.SetFloat(shaderID_AVPro, blitAVPro ? 1f : 0);
                // cannot use _MainTex_ST due to unity shenanigans overriding the value during blit
                _blitMaterial.SetVector(shaderID_MainTex_ST_Override, blitST);
                // run the op
                VRCGraphics.Blit(blitTexture, _internalTexture, _blitMaterial, 0);
            }


            if (hasBakedTexture)
            {
                // Handle public texture
                if (_applyResize && resizeTexture)
                {
                    if (_bakedTexture.IsCreated()) _bakedTexture.Release();

                    var latestTextureAspect = latestNull ? aspect : (float)latestTextureWidth / latestTextureHeight;
                    if (applyResizeAspect && aspect > 0 && Mathf.Abs(latestTextureAspect - aspect) > 0.0001)
                    {
                        var normWidth = latestTextureWidth / aspect;
                        if (normWidth > latestTextureHeight)
                            latestTextureHeight = (int)(latestTextureHeight / (latestTextureHeight / normWidth));
                        else latestTextureWidth = (int)(latestTextureWidth / (normWidth / latestTextureHeight));
                    }

                    _bakedTexture.width = latestTextureWidth;
                    _bakedTexture.height = latestTextureHeight;
                }

                if (latestNull)
                {
                    if (_bakedTexture.IsCreated()) _bakedTexture.Release();
                }
                else if (blitTexture != _bakedTexture)
                {
                    if (!_bakedTexture.IsCreated()) _bakedTexture.Create();
                    // do not aspect the render if 3D mode is enabled, leave that up to a 3D shader downstream
                    _blitMaterial.SetFloat(shaderID_ForceAspect, tv.applyAspectToBlit ? aspect : 0);
                    _blitMaterial.SetFloat(shaderID_3D, shaderVideoData.m30);
                    // run the op
                    VRCGraphics.Blit(_internalTexture, _bakedTexture, _blitMaterial, 1);
                }
            }

            if (hasCustomMaterials)
            {
                for (var index = 0; index < _customMaterials.Length; index++)
                {
                    var customMaterial = _customMaterials[index];
                    var customProperty = shaderIDs_MaterialProperties[index];
                    customMaterial.SetTexture(customProperty, _internalTexture);
                    customMaterial.SetMatrix(shaderID_VideoData, shaderVideoData);
                }
            }

            if (hasGSVEnabled)
            {
                VRCShader.SetGlobalTexture(shaderIDGlobal_VideoTex, _internalTexture);
                VRCShader.SetGlobalMatrix(shaderIDGlobal_VideoData, shaderVideoData);
                _customTextureIsGlobal = true;
            }
            // if globals ever get disabled, unset the custom texture global flag as well
            else if (_customTextureIsGlobal)
            {
                _customTextureIsGlobal = false;
                VRCShader.SetGlobalTexture(shaderIDGlobal_VideoTex, null);
                VRCShader.SetGlobalMatrix(shaderIDGlobal_VideoData, Matrix4x4.zero);
            }

            // bool hasReadbackEnabled = tv.enablePixelExtraction;
            // if (hasReadbackEnabled && !extractionInProgress && internalTexture.IsCreated())
            // {
            //     // ReSharper disable once SuspiciousTypeConversion.Global
            //     VRCAsyncGPUReadback.Request(internalTexture, 0, (UdonBehaviour)(Component)this);
            //     extractionInProgress = true;
            // }
        }

        // public override void OnAsyncGpuReadbackComplete(VRCAsyncGPUReadbackRequest request)
        // {
        //     if (!request.done) return;
        //     if (request.hasError)
        //     {
        //         tv.enablePixelExtraction = false;
        //         Error("Pixel Readback had an error occur. Check logs.");
        //         return;
        //     }
        //     var pix = pixels;
        //     var pixelCount = textureWidth * textureHeight;
        //     if (pixelCount != pixels.Length)
        //     {
        //         pixelDims.x = textureWidth;
        //         pixelDims.y = textureHeight;
        //         Trace($"layer count {request.layerCount} layer size {request.layerDataSize}");
        //         pix = new Color32[request.layerDataSize];
        //     }
        //
        //     if (request.TryGetData(pix, 0)) pixels = pix;
        //     extractionInProgress = false;
        // }

        /// <summary>
        /// Extract the Texture from the respective video player target. This reference is not in the CPU so ReadPixels would be costly, just FYI.
        /// </summary>
        /// <returns>The Texture reference from the backing video player for the current frame.</returns>
        public Texture GetVideoTexture(out Vector4 textureST)
        {
            Texture texture = null;
            textureST = new Vector4(1, 1, 0, 0);
            if ((int)tv.state <= (int)TVPlayState.STOPPED) return null;

            if (!_hasVideoMat)
            {
                var mat = matRenderer.sharedMaterial;
                _videoMat = mat != null ? mat : matRenderer.material;
                _hasVideoMat = _videoMat != null;
            }


            if (_hasVideoMat)
            {
                Vector4 st;
                if (isAVPro)
                {
                    texture = _videoMat.GetTexture(shaderID_MainTex);
                    st = _videoMat.GetVector(shaderID_MainTex_ST);
                }
                else
                {
                    matRenderer.GetPropertyBlock(matBlock);
                    texture = matBlock.GetTexture(shaderID_MainTex);
                    st = matBlock.GetVector(shaderID_MainTex_ST);
                }

                if (st != Vector4.zero) textureST = st; // prevent empty vector 4 from being applied
            }

            return texture;
        }

        #endregion
    }
}