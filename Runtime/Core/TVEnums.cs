using System.Runtime.CompilerServices;
using ArchiTech.SDK;

namespace ArchiTech.ProTV
{
    public enum TVPlayState
    {
        WAITING,
        STOPPED,
        PLAYING,
        PAUSED
    }

    public enum TVErrorState
    {
        NONE,
        RETRY,
        BLOCKED,
        FAILED
    }

    public enum TVPlaylistSortMode
    {
        DEFAULT,
        RANDOM,
        TITLE_ASC,
        TITLE_DESC,
        TAG_ASC,
        TAG_DESC
    }

    public enum TV3DMode
    {
        [I18nInspectorName("Not 3D")] NONE,
        [I18nInspectorName("Side by Side")] SBS,
        [I18nInspectorName("Side by Side Swapped")] SBS_SWAP,
        [I18nInspectorName("Over Under")] OVUN,
        [I18nInspectorName("Over Under Swapped")] OVUN_SWAP
    }
}