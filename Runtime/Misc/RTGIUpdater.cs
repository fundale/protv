using UdonSharp;
using UnityEngine;

namespace ArchiTech.ProTV
{
    [DisallowMultipleComponent]
    [UdonBehaviourSyncMode(BehaviourSyncMode.NoVariableSync)]
    public class RTGIUpdater : UdonSharpBehaviour
    {
        private Renderer _renderer;
        private void Start() => _renderer = GetComponent<Renderer>();
        private void Update() => _renderer.UpdateGIMaterials();
    }
}