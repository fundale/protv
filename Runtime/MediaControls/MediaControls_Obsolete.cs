using System;

namespace ArchiTech.ProTV
{
    public partial class MediaControls
    {
        
        /// <summary>
        /// Use <see cref="UpdateLoading()"/> instead
        /// </summary>
        [Obsolete("Use UpdateLoading() instead")]
        public void _UpdateLoading() => UpdateLoading();
        
        /// <summary>
        /// Use <see cref="UpdateSeek()"/> instead
        /// </summary>
        [Obsolete("Use UpdateSeek() instead")]
        public void _UpdateSeek() => UpdateSeek();
        
        /// <summary>
        /// Use <see cref="UpdateClock()"/> instead
        /// </summary>
        [Obsolete("Use UpdateClock() instead")]
        public void _UpdateClock() => UpdateClock();
        
        /// <summary>
        /// Use <see cref="UpdateUrlInput()"/> instead
        /// </summary>
        [Obsolete("Use UpdateUrlInput() instead")]
        public void _UpdateUrlInput() => UpdateUrlInput();
        
        /// <summary>
        /// Use <see cref="EndEditUrlInput()"/> instead
        /// </summary>
        [Obsolete("Use EndEditUrlInput() instead")]
        public void _EndEditUrlInput() => EndEditUrlInput();
        
        /// <summary>
        /// Use <see cref="Play()"/> instead
        /// </summary>
        [Obsolete("Use Play() instead")]
        public void _Play() => Play();
        
        /// <summary>
        /// Use <see cref="Pause()"/> instead
        /// </summary>
        [Obsolete("Use Pause() instead")]
        public void _Pause() => Pause();
        
        /// <summary>
        /// Use <see cref="Stop()"/> instead
        /// </summary>
        [Obsolete("Use Stop() instead")]
        public void _Stop() => Stop();
        
        /// <summary>
        /// Use <see cref="Skip()"/> instead
        /// </summary>
        [Obsolete("Use Skip() instead")]
        public void _Skip() => Skip();
        
        /// <summary>
        /// Use <see cref="ReSync()"/> instead
        /// </summary>
        [Obsolete("Use ReSync() instead")]
        public void _ReSync() => ReSync();
        
        /// <summary>
        /// Use <see cref="ToggleSync()"/> instead
        /// </summary>
        [Obsolete("Use ToggleSync() instead")]
        public void _ToggleSync() => ToggleSync();
        
        /// <summary>
        /// Use <see cref="ToggleLoop()"/> instead
        /// </summary>
        [Obsolete("Use ToggleLoop() instead")]
        public void _ToggleLoop() => ToggleLoop();
        
        /// <summary>
        /// Use <see cref="RefreshMedia()"/> instead
        /// </summary>
        [Obsolete("Use RefreshMedia() instead")]
        public void _RefreshMedia() => RefreshMedia();
        
        /// <summary>
        /// Use <see cref="RefreshMedia()"/> instead
        /// </summary>
        [Obsolete("Use RefreshMedia() instead")]
        public void _Refresh() => RefreshMedia();
        
        /// <summary>
        /// Use <see cref="ToggleAudioMode()"/> instead
        /// </summary>
        [Obsolete("Use ToggleAudioMode() instead")]
        public void _ToggleAudioMode() => ToggleAudioMode();
        
        /// <summary>
        /// Use <see cref="ToggleColorCorrection()"/> instead
        /// </summary>
        [Obsolete("Use ToggleColorCorrection() instead")]
        public void _ToggleColorCorrection() => ToggleColorCorrection();
        
        /// <summary>
        /// Use <see cref="ToggleMute()"/> instead
        /// </summary>
        [Obsolete("Use ToggleMute() instead")]
        public void _ToggleMute() => ToggleMute();
        
        /// <summary>
        /// Use <see cref="ToggleLock()"/> instead
        /// </summary>
        [Obsolete("Use ToggleLock() instead")]
        public void _ToggleLock() => ToggleLock();
        
        /// <summary>
        /// Use <see cref="SeekForward()"/> instead
        /// </summary>
        [Obsolete("Use SeekForward() instead")]
        public void _SeekForward() => SeekForward();
        
        /// <summary>
        /// Use <see cref="SeekBackward()"/> instead
        /// </summary>
        [Obsolete("Use SeekBackward() instead")]
        public void _SeekBackward() => SeekBackward();
        
        /// <summary>
        /// Use <see cref="Seek()"/> instead
        /// </summary>
        [Obsolete("Use Seek() instead")]
        public void _Seek() => Seek();
        
        /// <summary>
        /// Use <see cref="ChangeSeekOffset()"/> instead
        /// </summary>
        [Obsolete("Use ChangeSeekOffset() instead")]
        public void _ChangeSeekOffset() => ChangeSeekOffset();
        
        /// <summary>
        /// Use <see cref="ChangeVolume()"/> instead
        /// </summary>
        [Obsolete("Use ChangeVolume() instead")]
        public void _ChangeVolume() => ChangeVolume();
        
        /// <summary>
        /// Use <see cref="ChangeVideoPlayer()"/> instead
        /// </summary>
        [Obsolete("Use ChangeVideoPlayer() instead")]
        public void _ChangeVideoPlayer() => ChangeVideoPlayer();
        
        /// <summary>
        /// Use <see cref="Change3DMode()"/> instead
        /// </summary>
        [Obsolete("Use Change3DMode() instead")]
        public void _Change3DMode() => Change3DMode();
        
        /// <summary>
        /// Use <see cref="Toggle3DWidth()"/> instead
        /// </summary>
        [Obsolete("Use Toggle3DWidth() instead")]
        public void _Toggle3DWidth() => Toggle3DWidth();
        
        /// <summary>
        /// Use <see cref="UpdateMedia()"/> instead
        /// </summary>
        [Obsolete("Use UpdateMedia() instead")]
        public void _UpdateMedia() => UpdateMedia();
        
        /// <summary>
        /// Use <see cref="ChangeMedia()"/> instead
        /// </summary>
        [Obsolete("Use ChangeMedia() instead")]
        public void _ChangeMedia() => ChangeMedia();
        
        /// <summary>
        /// Use <see cref="ToggleUrlMode()"/> instead
        /// </summary>
        [Obsolete("Use ToggleUrlMode() instead")]
        public void _ToggleUrlMode() => ToggleUrlMode();

        /// <summary>
        /// Use <see cref="ToggleUrlMode()"/> instead
        /// </summary>
        [Obsolete("Use ToggleUrlMode() instead")]
        public void _ToggleUrl() => ToggleUrlMode();
        
        /// <summary>
        /// Use <see cref="UseMainUrl()"/> instead
        /// </summary>
        [Obsolete("Use UseMainUrl() instead")]
        public void _UseMainUrl() => UseMainUrl();
        
        /// <summary>
        /// Use <see cref="UseAlternateUrl()"/> instead
        /// </summary>
        [Obsolete("Use UseAlternateUrl() instead")]
        public void _UseAlternateUrl() => UseAlternateUrl();
        
        /// <summary>
        /// Use <see cref="ToggleCurrentRemainingTime()"/> instead
        /// </summary>
        [Obsolete("Use ToggleCurrentRemainingTime() instead")]
        public void _ToggleCurrentRemainingTime() => ToggleCurrentRemainingTime();
        
        /// <summary>
        /// Use <see cref="ChangePlaybackSpeed()"/> instead
        /// </summary>
        [Obsolete("Use ChangePlaybackSpeed() instead")]
        public void _ChangePlaybackSpeed() => ChangePlaybackSpeed();
        
        /// <summary>
        /// Use <see cref="ResetPlaybackSpeed()"/> instead
        /// </summary>
        [Obsolete("Use ResetPlaybackSpeed() instead")]
        public void _ResetPlaybackSpeed() => ResetPlaybackSpeed();
        
        /// <summary>
        /// Use <see cref="UpdateInfo()"/> instead
        /// </summary>
        [Obsolete("Use UpdateInfo() instead")]
        public void _UpdateInfo() => UpdateInfo();
    }
}